import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BadrequestComponent } from './security/badrequest/badrequest.component';
import { LogoutComponent } from './security/logout/logout.component';
import { SessionexpiredComponent } from './security/sessionexpired/sessionexpired.component';
import { UnauthorizedaccessComponent } from './security/unauthorizedaccess/unauthorizedaccess.component';
import { ChecksessionService } from './thirdparty/checksession.service';


import { AdminLayoutComponent } from './Admin/Layout/admin-layout/admin-layout.component';
import { SacSurveyDashboardComponent } from './Admin/Modules/sac-survey-dashboard/sac-survey-dashboard.component';
import { SacSurveyReportComponent } from './Admin/Modules/sac-survey-report/sac-survey-report.component';

import { LoginComponent } from './Admin/login/login.component';
import { DashboardComponent } from './Admin/Modules/dashboard/dashboard.component';
import { ChangepasswordComponent } from './Admin/Modules/changepassword/changepassword.component';
import { GenderReportComponent } from './Admin/Modules/Deptreports/gender-report/gender-report.component';
import { CasteReportComponent } from './Admin/Modules/Deptreports/caste-report/caste-report.component';
import { AgeReportComponent } from './Admin/Modules/Deptreports/age-report/age-report.component';
import { HealthReportComponent } from './Admin/Modules/Deptreports/health-report/health-report.component';
import { EmploymentReportComponent } from './Admin/Modules/Deptreports/employment-report/employment-report.component';
import { EducationalReportComponent } from './Admin/Modules/Deptreports/educational-report/educational-report.component';
import { FemaleheadfamiliesComponent } from './Admin/Modules/Deptreports/femaleheadfamilies/femaleheadfamilies.component';
import { NewhomeComponent } from './Admin/Modules/newhome/newhome.component';
import { StateHomeComponent } from './Admin/Modules/state-home/state-home/state-home.component';
import { WorkActivityReportComponent } from './Admin/Modules/Deptreports/work-activity-report/work-activity-report/work-activity-report.component';
import { BarchartComponent } from './Admin/Modules/barchart/barchart.component';
import { SacsurveycheckerformComponent } from './Admin/Modules/sacsurveycheckerform/sacsurveycheckerform.component';
import { PermanentdisabilityComponent } from './Admin/Modules/permanentdisability/permanentdisability.component';
import { ApproverecordscountComponent } from './Admin/Modules/Deptreports/approverecordscount/approverecordscount.component';
import { SurveyedregcountsComponent } from './Admin/Modules/Deptreports/surveyedregcounts/surveyedregcounts.component';
import { AddvillageComponent } from './Admin/Modules/addvillage/addvillage.component';
import { LaunchComponent } from './Admin/Modules/launch/launch.component';
import { URRgisteredCountsComponent } from './Admin/Modules/Deptreports/urrgistered-counts/urrgistered-counts.component';
import { RegistrationsurveystatusreportComponent } from './Admin/Modules/Deptreports/registrationsurveystatusreport/registrationsurveystatusreport.component';
import { VillagedeletionComponent } from './Admin/Modules/Deptreports/villagedeletion/villagedeletion.component';
import { DistrictReportComponent } from './Admin/Modules/Deptreports/district-report/district-report.component';

const routes: Routes = [
  {
    path: 'test',
    loadChildren: () => import('./Test/test.module').then((s) => s.TestModule),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/NewHome',
    // redirectTo: '/NewHome',
    // redirectTo: '/boms/login',
  },
 /*  {
    path: 'launch',
    component: LaunchComponent,
  },  */
  {
    path: 'NewHome',
    component: NewhomeComponent,
  }, 
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'Dashboard',
    component: DashboardComponent,
  },
  { path: 'Barchart', component: BarchartComponent },
  {
    path: '',
   component: AdminLayoutComponent,

    children: [     
      { path: 'SACSurveyReport', component: SacSurveyReportComponent },
      { path: 'Home', component: StateHomeComponent },
      { path: 'Changepassword', component: ChangepasswordComponent },
      { path: 'GenderReport', component: GenderReportComponent },
      { path: 'CasteReport', component: CasteReportComponent },
      { path: 'AgeReport', component: AgeReportComponent },
      { path: 'EmploymentReport', component: EmploymentReportComponent },
      { path: 'EducationalReport', component: EducationalReportComponent },
      { path: 'HealthReport', component: HealthReportComponent },
      { path: 'FemaleheadReport', component: FemaleheadfamiliesComponent },
      { path: 'WorkActivityReport', component: WorkActivityReportComponent },
      { path: 'PermanentdisabilityReport', component: PermanentdisabilityComponent },
      { path: 'ApproveForm', component: SacsurveycheckerformComponent },
      { path: 'Surveyedregcounts', component: SurveyedregcountsComponent },
      { path: 'Approvecounts', component: ApproverecordscountComponent },
      { path: 'Addvillage', component: AddvillageComponent },
      { path: 'Details', component: URRgisteredCountsComponent },
      { path: 'Registrationstatusreport', component: RegistrationsurveystatusreportComponent },
      { path: 'Villagedeletion', component: VillagedeletionComponent },
      { path: 'District', component: DistrictReportComponent },
   ],
  },
  // {
  //    path: 'SACSurveyReport', 
  //    component: SacSurveyReportComponent 
  // },

  // {
  //    path: 'SACSurveyDashboard',
  //    component: SacSurveyDashboardComponent
  // },
  {
    path: 'Badrequest',
    component: BadrequestComponent,
  },
  {
    path: 'Logout',
    component: LogoutComponent,
    canActivate: [ChecksessionService],
  },
  {
    path: 'Sessionexpired',
    component: SessionexpiredComponent,
  },
  {
    path: 'Unauthorizedaccess',
    component: UnauthorizedaccessComponent,
  },
 
  { path: '**', redirectTo: 'NewHome'},
  

  {
    path: '**',
    pathMatch: 'full',
    component: BadrequestComponent,
  }


  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }