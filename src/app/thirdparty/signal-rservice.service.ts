import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { Subject } from 'rxjs';
import * as signalR from '@microsoft/signalr';

@Injectable({
  providedIn: 'root'
})
export class SignalRServiceService {
  private hubConnection: signalR.HubConnection;
  private messageReceived = new Subject<string>();

  constructor() { }

  startConnection() {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('http://localhost:22435/NewsHub')
      .build();

    this.hubConnection.start()
      .then(() => console.log('SignalR connection started'))
      .catch(err => console.error('Error while starting SignalR connection: ' + err));

    this.hubConnection.on('ReceiveMessage', (message: string) => {
      
      this.messageReceived.next(message);
    });
  }

  sendMessage(userId: string, message: string) {
    this.hubConnection.invoke('SendMessage', userId, message)
      .catch(err => console.error('Error while sending message: ' + err));
  }

  getMessageReceived() {
    
    return this.messageReceived.asObservable();
  }
}