import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
import * as Base64 from 'crypto-js/enc-base64';
import { CookieService } from 'ngx-cookie-service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { any } from 'cypress/types/bluebird';
@Injectable({
  providedIn: 'root'
})
export class MidlayerService {

  globalsetting = {
    production: 1,
    
    appclient:'Swachhandhra',
    //appclient:'ChefCode',
    //appclient:'boms',
    retry: 1,
    baseurl:'',
    //baseurl: 'http://localhost:22435/'
    base64Image:''
  };
  Deploystage:any="";
  hskkey: any = "";//"0123456789123456";
  encrypted: any = "";
  decrypted: any = "";
  hash: any = "";
  request: any = "";
  responce: any = "";
  cartvalue:any="";
  ipAddress:any="";
  pincode:any="";
  location:any="";
  deviceInfo:any;
  constructor(
    private cookieService: CookieService,
    private httpClient: HttpClient,
    private router: Router,
  ) {
     
    if(this.globalsetting.appclient=="Swachhandhra"){
      //Local
/*  this.globalsetting.baseurl="http://localhost:22435/";    */
      //UAT
     /* Auth book server */  
 //this.globalsetting.baseurl=" https://authbook.in/swachapi/";

 /* Stage server */
 //this.globalsetting.baseurl="http://10.96.44.65/SWAPI/";
 //this.Deploystage="UAT";
this.Deploystage="PROD"; 
 this.globalsetting.baseurl="https://apsanitationworkerssurvey.ap.gov.in/SWAPI/";  
 /* Prod server */
  // this.Deploystage="PROD";
 // this.globalsetting.baseurl="https://apsanitationworkerssurvey.ap.gov.in/SWAPI/"; 

   /* Audit server */
 //  this.globalsetting.baseurl="http://10.252.112.57/SWAPI/";
  //  this.globalsetting.baseurl="http://103.174.56.174/SWAPI/";
    //  this.globalsetting.baseurl="";

      //Production
   // this.globalsetting.baseurl="";
    }
   }
 

  getPostHttpOptionsplain(encobj: any, encsign: any): any {
    try {
      this.hash = Base64.stringify(CryptoJS.HmacSHA256("0123456789123456", "0123456789123456"));

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          accessus: '0123456789123456',
          accesske: '0123456789123456',
          accesstk: '0123456789123456',
          accessdt: encobj,
          accessdtsi: this.hash,
        }),
      };
      return httpOptions;
    }
    catch (error) {
      this.clearjunk();
      return this.router.navigate(['/Badrequest']);
    }
  }
  
  dataTableOptions(): any {
    return {
      pagingType: 'full_numbers',
      pageLength: 10,
      //info:false,
      destroy: true,
      //retrive:true,
      lengthMenu: [
        [10, 25, 50, -1],
        [10, 25, 50, 'ALL'],
      ],
      language: {
        emptyTable: 'No data available.',
        zeroRecords: 'No matching records found.'
      },
      //dom:'Bfrip',
      // buttons:[ 'excel']
      // autoWidth:true,
       //scrollX:true,
       //fixedColumns:true,
       //fixedHeader:true,
       //scrollCollapse:true 
    };
  }
  getPostHttpOption_withenc(encobj: any, encsign: any): any {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',

        }),
      };
      return httpOptions;
    }
    catch (error) {
      return this.Responseerror(error);
    }

  }

  HttpOptions(): any {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',


      }),
    };
    return httpOptions;
  }
  accessus1 :any='';_hsk1 :any='';_sltkn1:any='';
  getPostHttpOptionauth(encobj: any, encsign: any): any {
    this.accessus1=sessionStorage.getItem('_Uenc');
    this._hsk1=sessionStorage.getItem('_hsk');
    this._sltkn1=sessionStorage.getItem('_sltkn');
    try {
      if (this._hsk1 != "") {
     
        const httpOptions = {

          headers: new HttpHeaders({
            'Content-Type': 'application/json',
            accessus: this.accessus1,
            accesske: this._hsk1,
            accesstk: this._sltkn1,
            accessdt: encobj,
            accessdtsi: encsign,
          }),
        };
     
        return httpOptions;
      }
      else {
      //  alert(this.cookieService.get('_hsk'));
        this.clearjunk();
        return this.router.navigate(['/Badrequest']);
      }
    }
    catch (error) {
      return this.Responseerror(error);
    }

  }

  enccall(req: any): string {
  
    if (this.hskkey == null || this.hskkey == "" || this.hskkey == undefined) {
      this.hskkey == "0123456789123456";

    }
    if (this.hskkey != null && this.hskkey != "0123456789123456") {
      this.hskkey = sessionStorage.getItem('_hsk');
    }
    const keyVal = CryptoJS.enc.Utf8.parse(this.hskkey);
    const ivVal = CryptoJS.enc.Utf8.parse(this.hskkey);
    const encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(req), keyVal, { keySize: 128 / 8, iv: ivVal, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, }).toString();
    if (this.encrypted.length > 20) { return encrypted.toString(); }
    else { this.encrypted == "invalid"; }
    return encrypted;
  }
  deccall(req: any) {
 
     if (this.hskkey == null || this.hskkey == "" || this.hskkey == undefined) {
      this.hskkey = "0123456789123456";

    }
    if (this.hskkey != null && this.hskkey != "0123456789123456") {
      this.hskkey = sessionStorage.getItem('_hsk');
    }
    let _key = CryptoJS.enc.Utf8.parse(this.hskkey);
    let _iv = CryptoJS.enc.Utf8.parse(this.hskkey);
    let decrypted = CryptoJS.AES.decrypt(req, _key, { keySize: 16, iv: _iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }).toString(CryptoJS.enc.Utf8);

    if (decrypted.length > 20) {

      return JSON.parse(decrypted);
    }
    else {
      decrypted == "invalid";
      return decrypted;
    }
  }

  captchdeccall(req: any) {
 
      if (this.hskkey == null || this.hskkey == "" || this.hskkey == undefined) {
       this.hskkey = "0123456789123456";
 
     }
     if (this.hskkey != null && this.hskkey != "0123456789123456") {
       this.hskkey = sessionStorage.getItem('_hsk');
     }
     let _key = CryptoJS.enc.Utf8.parse(this.hskkey);
     let _iv = CryptoJS.enc.Utf8.parse(this.hskkey);
     let decrypted = CryptoJS.AES.decrypt(req, _key, { keySize: 16, iv: _iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 }).toString(CryptoJS.enc.Utf8);
 
     if (decrypted.length < 20) {
 
       return (decrypted);
     }
     else {
       decrypted == "invalid";
       return decrypted;
     }
   }
  Getuser() {
    let obj: any="";
    if (sessionStorage.getItem("_Uenc") !== "") {

      if (sessionStorage.getItem("_Uenc") == this.cookieService.get('_Uenc')) {
        obj= this.deccall(sessionStorage.getItem("Logdata"));
        if (obj[0].ROLE_ID != null || obj[0].ROLE_ID != undefined) {
          return obj;
        }
        else {

          Swal.fire({
            title: 'Sessionexpired....',
            text: 'Sessionexpired!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
          }).then((result) => {
            if (result.value) {
              sessionStorage.clear();
              this.cookieService.deleteAll();
              this.router.navigate(["/Sessionexpired"]);
            }
            else if (result.dismiss === Swal.DismissReason.cancel) {
              sessionStorage.clear();
              this.cookieService.deleteAll();
              this.router.navigate(["/Sessionexpired"]);
            }
          })


        }
      }
      else {
        Swal.fire({
          title: 'Sessionexpired....',
          text: 'Sessionexpired!',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
            sessionStorage.clear();
            this.cookieService.deleteAll();
            this.router.navigate(["/Sessionexpired"]);
          }
          else if (result.dismiss === Swal.DismissReason.cancel) {
            sessionStorage.clear();
            this.cookieService.deleteAll();
            this.router.navigate(["/Sessionexpired"]);
          }
        })
      }

    }
    else {

      Swal.fire({
        title: 'Sessionexpired....',
        text: 'Sessionexpired!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          sessionStorage.clear();
          this.cookieService.deleteAll();
          this.router.navigate(["/Sessionexpired"]);
        }
        else if (result.dismiss === Swal.DismissReason.cancel) {
          sessionStorage.clear();
          this.cookieService.deleteAll();
          this.router.navigate(["/Sessionexpired"]);
        }
      })


    }
  }

  Getmenu(){

    let objmenu: any="";
    if (sessionStorage.getItem("_Logmen") !== "") {

      objmenu= this.deccall(sessionStorage.getItem("_Logmen"));
        return objmenu;

    }
    else {

      Swal.fire({
        title: 'User menu not avilable....',
          text: 'User menu not avilable!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No'
      }).then((result) => {
        if (result.value) {
          sessionStorage.clear();
          this.cookieService.deleteAll();
          this.router.navigate(["/Sessionexpired"]);
        }
        else if (result.dismiss === Swal.DismissReason.cancel) {
          sessionStorage.clear();
          this.cookieService.deleteAll();
          this.router.navigate(["/Sessionexpired"]);
        }
      })


    }
  }

  JSONToCSVConvertorpdf(JSONData: any, ReportTitle: string, ShowLabel: any): void {
   
    const arrData =
      typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    let CSV = 'sep=,' + '\r\n\n';
    if (ShowLabel) {
      let row = '';
      for (let index in arrData[0]) {
        row += index + ',';
      }
      row = row.slice(0, -1);
      CSV += row + '\r\n';
    }
    for (let i = 0; i < arrData.length; i++) {
      let row = '';
      for (let index in arrData[i]) {
        row += '"' + arrData[i][index] + '",';
      }

      row.slice(0, row.length - 1);
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      alert('Invalid data');
      return;
    }

    let fileName = '';
    fileName += ReportTitle.replace(/ /g, '_');

    // Initialize file format you want csv or xls
    const uri = 'data:application/pdf;' + escape(CSV);
    const link = document.createElement('a');
    link.href = uri;

    // set the visibility hidden so it will not effect on your web-layout

    // link.style = 'visibility:hidden';
    link.download = fileName + '.pdf';

    // this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
  JSONToCSVConvertor(JSONData: any, ReportTitle: string, ShowLabel: any): void {
   
    const arrData =
      typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    let CSV = 'sep=,' + '\r\n\n';
    if (ShowLabel) {
      let row = '';
      for (let index in arrData[0]) {
        row += index + ',';
      }
      row = row.slice(0, -1);
      CSV += row + '\r\n';
    }
    for (let i = 0; i < arrData.length; i++) {
      let row = '';
      for (let index in arrData[i]) {
        row += '"' + arrData[i][index] + '",';
      }

      row.slice(0, row.length - 1);
      CSV += row + '\r\n';
    }

    if (CSV == '') {
      alert('Invalid data');
      return;
    }

    let fileName = '';
    fileName += ReportTitle.replace(/ /g, '_');

    // Initialize file format you want csv or xls
    const uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
    const link = document.createElement('a');
    link.href = uri;

    // set the visibility hidden so it will not effect on your web-layout

    // link.style = 'visibility:hidden';
    link.download = fileName + '.csv';

    // this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
  getnocontent(contenttype:any){
    if(contenttype=="IMG"){
     return "";

      }
      return "";
  }

  public hmackhash(req: any) {
    const hmacDigest = CryptoJS.HmacSHA1("0123456789123456", "0123456789123456");
  }
  clearjunk() { sessionStorage.clear(); this.cookieService.deleteAll(); }
  Responseerror(error: any): void {
   
    if (error.status === 401) {
      this.clearjunk();
      this.router.navigate(['/Unauthorizedaccess']);
    } else if (error.status === 403) {
      this.clearjunk();
      this.router.navigate(['/Unauthorizedaccess']);
    }
    else if (error.status >= 500 && error.status < 600) {
      this.clearjunk();
      this.router.navigate(['/Badrequest']);
    }
    else if (error.status === 400) {
      this.clearjunk();
      this.router.navigate(['/Badrequest']);
    }
    else if (error === "session") {
      this.clearjunk();
      this.router.navigate(['/Sessionexpired']);
    }
    else if (error === "access") {
      this.clearjunk();
      this.router.navigate(['/Unauthorizedaccess']);
    }
    else {
      this.router.navigate(['/Badrequest']);
    }
  }

}





