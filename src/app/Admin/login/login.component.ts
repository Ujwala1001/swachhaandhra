import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
 
  
  capachrefid :string='';
  username = '';
  password = '';
  isFocused=false;
  captchresult = '';
  captchvalue = '';
  loginarray: any[] = [];
  capenc = '';
  id = '';
  idref = '';
  imgurl = '';
  token = '';
   hsk = ''; 
   
   hash: any;
  
  constructor(

    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,
    
    private val: InputvalidaService,
    private cookieService: CookieService,

  ) {
  }
  ngOnInit(): void {
    this.mid.hskkey='';
   this.CapthaGenerate();
  }
  async CapthaGenerate(): Promise<void> {
   
    try {
      const req = {
        type: '102',
        source:"web"
      };
     debugger;
      let url= "api/swachaandhra/swcaptch";// swcaptchmob
      let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, url);
     
      if (rsdata.code) {
        this.capenc = rsdata.capenc;
        this.id = rsdata.id;
        this.idref = rsdata.idval;
   
      
        this.token = rsdata.token;
        this.hsk = rsdata._hsk;
      this.mid.hskkey= this.hsk ;
      const expiration = new Date();
      expiration.setHours(expiration.getHours() + 1);
      sessionStorage.setItem('_hsk', this.mid.hskkey ?? '');
let rsdata3 = (this.mid.captchdeccall(rsdata.imgurl));
        this.imgurl = rsdata3;
     //  this.imgurl = rsdata.capcthimg;
         
        this.capachrefid ='';
        this.capachrefid =rsdata.capachrefid ;
      
      }
      else {
       
      }
    } catch (error) { 
      this.spinner.hide();
     // this.mid.Responseerror(error);
    }
  }

   getCookie(cname:any) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length,c.length);
    }
    return "";
} 
today:any;
  async Login(): Promise<void> {
    this.today = new Date(); 
    if (this.val.isEmpty(this.username)) {
      this.alt.warning('Please enter username');
      this.isFocused=true;
      return;
    }
    else if (this.val.isEmpty(this.password)) {
      this.alt.warning('Please enter password');
      this.isFocused=true;
      return ;
    }
    else if (this.val.isEmpty(this.captchvalue)) {
      this.alt.warning('captch result');
      this.isFocused=true;
      return ;
    }
   
    debugger;
    this.spinner.show();
    const req = {
      type: '105',
      input01:this.username,
      input02:this.password,
      input03:this.capachrefid,
      input04:this.captchvalue,
      input05:this.today
    };
   let url= "api/swachaandhra/SwachhAndhra_Login"; 
  let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, url);
  if (rsdata.code && rsdata.Details[0].STATUS=="1") {
  const expiration = new Date();
  expiration.setHours(expiration.getHours() + 1);
  this.token = rsdata.token;
  this.hsk = rsdata._hsk;
  let _hsk=rsdata._hsk;  
  let _sltkn=rsdata.token;  
  this.mid.hskkey=rsdata._hsk;
  this.spinner.hide();
  debugger;
  console.log(rsdata.loginDetails)
  
  sessionStorage.setItem('_Uenc', this.username ?? '');
  sessionStorage.setItem('_hsk', _hsk ?? '');
  sessionStorage.setItem('_sltkn', _sltkn ?? '');
  sessionStorage.setItem('lastlogintime', rsdata.lastlogintime ?? '');
  sessionStorage.setItem('username', this.username);
  sessionStorage.setItem('userrole', rsdata.loginDetails[0].ROLE_ID);
 // sessionStorage.setItem('userrole', '1005');
  sessionStorage.setItem('userdistcode', rsdata.loginDetails[0].DISTRICT_CODE);
  sessionStorage.setItem('userurtype', rsdata.loginDetails[0].URBAN_RURAL_TYPE);
  sessionStorage.setItem('userulbcode', rsdata.loginDetails[0].ULB_CODE);
  sessionStorage.setItem('usermandalcode', rsdata.loginDetails[0].MANDAL_CODE);
  sessionStorage.setItem('usergpcode', rsdata.loginDetails[0].GP_CODE);
  sessionStorage.setItem('userwardcode', rsdata.loginDetails[0].ROLE_ID);
  sessionStorage.setItem('uservillagecode', rsdata.loginDetails[0].VILLAGE_CODE);

/*   alert(rsdata.loginDetails[0].MANDAL_CODE); */
debugger;
  if(rsdata.IS_PASSWORD_CHANGE == "0")
  {

    this.router.navigate(["/Changepassword"]);
  
  }
  else if(rsdata.IS_PASSWORD_CHANGE == "1")
  {

    if(sessionStorage.getItem('userrole') =='1003' || sessionStorage.getItem('userrole') =='1004')
{
  this.router.navigate(["/SACSurveyReport"]);
}
else{
  this.router.navigate(["/Home"]);
}

  
}
  }
  else 
  { 
    this.alt.warning(rsdata.message);
    this.isFocused=true;
      this.clearinput();
     this.CapthaGenerate();
    this.spinner.hide();
    return ;
 
   }
  }

  clearinput(){
    this.mid.hskkey='';
    this.CapthaGenerate();
    this.username = '';
    this.password = '';
    this.captchvalue = ''; 
    this.capenc = '';
    this.id = '';
    this.idref = '';
    this.imgurl = '';
    this.token = '';
    this.hsk = '';
    
  }


}
