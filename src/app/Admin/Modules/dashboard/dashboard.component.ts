import { Component } from '@angular/core';

import * as am5 from "@amcharts/amcharts5";
import * as am5xy from "@amcharts/amcharts5/xy";
import * as am5radar from "@amcharts/amcharts5/radar";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import * as am5percent from "@amcharts/amcharts5/percent";
import { Router } from '@angular/router';
import { ClientcallService } from 'src/app/service/clientcall.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormArray, AbstractControl, FormControl, FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  activeButton: any
  showPhase(event: any){
    this.activeButton = event;
  }

  constructor(
     
    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder,
 
  ) { 
  }
  Swatch_Ur:any="";
  Name:any=sessionStorage.getItem('username');
  ngAfterViewInit() {
   
   
  }
  isgenchecked:boolean=true;
  isgenchecked1:boolean=true;
  isgenchecked2:boolean=true;
  isgenchecked3:boolean=true;
  isgenchecked4:boolean=true;
  roleid:any='';
  loginur:any='';
  roledistid:any='';
  rolemanid:any='';
  roleulbid:any='';
  roleurid:any='';
  ischecked:boolean=false;
  ngOnInit(): void {
    this.roleid=sessionStorage.getItem("userrole");
    this.loginur=sessionStorage.getItem("userurtype");
    debugger;
    this.Swatch_Ur='total';
   /*  if(sessionStorage.getItem('username') != '')
    {

      this.router.navigate(['/Home']); return;
    } */
    if (this.roleid=='1004' || this.roleid=='1003')
    { 
      this.Swatch_Ur=sessionStorage.getItem('userurtype');
      this.roleurid= sessionStorage.getItem('userurtype');
      this.roledistid= sessionStorage.getItem('userdistcode');
      this.roleulbid='0';
      this.rolemanid='0';
      if(this.Swatch_Ur=="U")
      {
        this.roleulbid= sessionStorage.getItem('userulbcode');
      }
      else if(this.Swatch_Ur=="R")
      {
        if(this.roleid=='1004')
        {
        this.rolemanid= sessionStorage.getItem('usermandalcode');
        }
        else{
          this.rolemanid= "0";
        }
      }
    
      this.getGender("total","Gender");
   this.getAge("total","Age");
        this.getCaste("total","Caste");
        this.getEduStatus("total","edustationreport");
      this.getfamilystatus("total","family");  
    }
    else   if (this.roleid=='1001' || this.roleid=='1002'|| this.roleid=='1006')
    {
      this.roledistid='0';
      this.rolemanid='0';
      this.roleulbid='0';
      this.roleurid='0';
      this.Swatch_Ur="U";
      this.ischecked=true;
      this.getGender("total","Gender");
    this.getAge("total","Age");
        this.getCaste("total","Caste");
        this.getEduStatus("total","edustationreport");
      this.getfamilystatus("total","family");  
    }
  
  }

 maybeDisposeRoot(divId:any) {
  debugger;
    am5.array.each(am5.registry.rootElements, function (root) {
      if (root.dom.id == divId) {
        root.dispose();
      }
    });
  };

  async getpiechart(datarow:any,piechartid:any): Promise<void> {
    debugger;
let root=am5.Root.new(piechartid);
    
    /* Chart code */
// Create root element

// Set themes
root.setThemes([
am5themes_Animated.new(root)
]);
root.interfaceColors.set("fill", am5.color('#FF0000'));

// Create chart
let chart = root.container.children.push(am5percent.PieChart.new(root, {
layout: root.verticalLayout,
radius: am5.percent(70)
}));


// Create series
let series = chart.series.push(am5percent.PieSeries.new(root, {
valueField: "value",
categoryField: "category",

}));



if(piechartid =="chartdivpie")
{
series.set("colors", am5.ColorSet.new(root, {
colors: [
  am5.color(0x73556E),
  am5.color(0x9FA1A6),
  am5.color(0xF28F6B),
 // am5.color(0xFF6F91)
 am5.color(0xD65DB1)
]
}))
}

 else if(piechartid =="chartdivpie1")
{
series.set("colors", am5.ColorSet.new(root, {
colors: [
  am5.color(0x4B2B6D),
  am5.color(0xf778a1),
  am5.color(0x008099),
  am5.color(0x809ACC)
]

}))
}

else if(piechartid =="chartdivpie2")
{
series.set("colors", am5.ColorSet.new(root, {
  colors: [
    am5.color(0xF2AA6B),
    am5.color(0x737ca1),
    am5.color(0x7d0541), 
    am5.color(0x2e1a47)
  ]


}))
}

else if(piechartid =="chartdivpie3")
{
series.set("colors", am5.ColorSet.new(root, {
  colors: [
    am5.color(0xc48793),
   
    am5.color(0xF2AA6B),
    am5.color(0xA95A52),
    am5.color(0xE35B5D),
    am5.color(0x307d7e),
    am5.color(0x4B1154),
    am5.color(0xb4cfec)
  ]
}))
}

else if(piechartid =="chartdivpie4")
{
series.set("colors", am5.ColorSet.new(root, {
  colors: [
    am5.color(0x435488),
    am5.color(0x96596D),
    am5.color(0x609078),
    am5.color(0xFFC0CB),
    am5.color(0xC71585),
     am5.color(0xc2e5a3)
  ]
}))
}


// Set data
series.data.setAll(datarow);
//series.set("fill", am5.color("#00ff00"));


// Create legend
let legend = chart.children.push(am5.Legend.new(root, {
centerX: am5.percent(50),
x: am5.percent(50),
marginTop: 15, 

marginBottom: 15
}));

legend.data.setAll(series.dataItems);

series.labels.template.setAll({

fontSize: 10,


});
// Play initial series animation
//series.set("fill", am5.color("#00ff00")); 
series.appear(1000, 100);
}


  gendarray: any[] = [];  dataRow:any []=[];req:any={};
  async getGender(selectradiotype:any,reporttype:any): Promise<void> {
 debugger;
    try {
     
      this.maybeDisposeRoot("chartdivpie");

      
}
 catch (error) {

}
     
   this.spinner.hide();
try{
  debugger;

  if(selectradiotype=="total")
  {
    this.isgenchecked=true;
    this.req = {
      type: '201',
      input01:this.roleurid,
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="urban")
  {
    this.isgenchecked=false;
    this.req = {
      type: '201',
      input01:"U",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="rural")
  {
    this.isgenchecked=false;
    this.req = {
      type: '201',
      input01:"R",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }

  
  this.dataRow=[];
  this.gendarray =[];
  let urlservice = "api/swachaandhra/Dashboard";
   let rsdata = await this.apipost.clinetposturlauth_withoutenc(this.req, urlservice);
  // let responce = await this.apipost.clinetposturlauth(req, urlservice);
  // let rsdata = JSON.parse(this.mid.deccall(responce.data));

   this.spinner.hide();
   debugger;
  this.gendarray = rsdata.Details;

  let Data =this.gendarray;

    /* Chart code */
// Create root element
// https://www.amcharts.com/docs/v5/getting-started/#Root_element
let root = am5.Root.new("chartdivpie");

root.setThemes([
am5themes_Animated.new(root)
]);

let chart = root.container.children.push(am5xy.XYChart.new(root, {
panX: false,
panY: false,
wheelX: "panX",
layout: root.verticalLayout
}));

/* var data = [{ 
  category: "Research", 
  value:100,
  settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(4)
  }
}, { 
  category: "Marketing", 
  value: 200,
  settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(2)
  }
}, { 
  category: "Sales", 
  value:300,
  settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(1)
  }
}]; */

debugger;
for(var i in Data) {    

 var item = Data[i];   

 this.dataRow.push({ 
  
 value: Data[i].TOTAL_COUNT, category: "Total",
 settings: {
  fill: am5.ColorSet.new(root, {
    colors: [
      am5.color(0x73556E),
      am5.color(0x9FA1A6),
      am5.color(0xF28F6B),
     // am5.color(0xFF6F91)
     am5.color(0xD65DB1)
    ]
    }).getIndex(0)
}
 
     
 });
 this.dataRow.push({ 
   value: Data[i].MALE_COUNT, category: "Male",
   settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(1)
  }
     
 });
 this.dataRow.push({ 
   value: Data[i].FEMALE_COUNT, category: "Female",
   settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(2)
  }
     
 });
 this.dataRow.push({ 
   value: Data[i].TRANSGENDER_COUNT, category: "Transgender",
   settings: {
    fill: am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(3)
  }
 
     
 });
}


let yAxis = chart.yAxes.push(am5xy.CategoryAxis.new(root, {
  maxDeviation: 0.2,
categoryField: "category",
renderer: am5xy.AxisRendererY.new(root, {


})
}));


yAxis.data.setAll(this.dataRow);

let xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
renderer: am5xy.AxisRendererX.new(root, {
  
})
}));



let series = chart.series.push(am5xy.ColumnSeries.new(root, {
  name:  "Series",
  xAxis: xAxis,
  yAxis: yAxis,
  valueXField: "value",
  categoryYField: "category",
  tooltip: am5.Tooltip.new(root, {
    pointerOrientation: "horizontal",
    labelText: "J"
   })
}));




series.columns.template.setAll({
  templateField: "settings"
});

series.data.setAll(this.dataRow);


var legend = chart.children.push(am5.Legend.new(root, {
  nameField: "categoryY",
  centerX: am5.percent(50),
  x: am5.percent(50)
}));

legend.data.setAll(series.dataItems);




}
catch (error) {

}
    
  }

  agearray: any[] = [];  dataRow1:any []=[];req1:any={};
  async getAge(selectradiotype:any,reporttype:any): Promise<void> {
  debugger;
    try {
      this.maybeDisposeRoot("chartdivpie1");
    
}
 catch (error) {

}
     
   this.spinner.hide();
try{
 
  if(selectradiotype=="total")
  {
    this.isgenchecked=true;
    this.req1 = {
      type: '202',
      input01:this.roleurid,
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="urban")
  {
    this.isgenchecked=false;
    this.req1 = {
      type: '202',
      input01:"U",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="rural")
  {
    this.isgenchecked=false;
    this.req1 = {
      type: '202',
      input01:"R",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }

  
  this.dataRow1=[];
  this.agearray =[];
  let urlservice = "api/swachaandhra/Dashboard";
   let rsdata = await this.apipost.clinetposturlauth_withoutenc(this.req1, urlservice);
  // let responce = await this.apipost.clinetposturlauth(req, urlservice);
  // let rsdata = JSON.parse(this.mid.deccall(responce.data));

   this.spinner.hide();
   debugger;
  this.agearray = rsdata.Details;
 
  let Data =this.agearray;
 debugger;
 



  var root = am5.Root.new("chartdivpie1"); 

root.setThemes([
am5themes_Animated.new(root)
]);

var chart = root.container.children.push( 
am5xy.XYChart.new(root, {
  panY: false,
  wheelY: "zoomX",
  layout: root.verticalLayout
}) 
);

// Define data
/* var data = [{ 
category: "Research", 
value: 100,
settings: {
  fill: am5.ColorSet.new(root, {
    colors: [
      am5.color(0x73556E),
      am5.color(0x9FA1A6),
      am5.color(0xF28F6B),
     // am5.color(0xFF6F91)
     am5.color(0xD65DB1)
    ]
    }).getIndex(4)
}
}, { 
category: "Marketing", 
value: 200,
settings: {
  fill: am5.ColorSet.new(root, {
    colors: [
      am5.color(0x73556E),
      am5.color(0x9FA1A6),
      am5.color(0xF28F6B),
     // am5.color(0xFF6F91)
     am5.color(0xD65DB1)
    ]
    }).getIndex(2)
}
}, { 
category: "Sales", 
value:300,
settings: {
  fill: am5.ColorSet.new(root, {
    colors: [
      am5.color(0x73556E),
      am5.color(0x9FA1A6),
      am5.color(0xF28F6B),
     // am5.color(0xFF6F91)
     am5.color(0xD65DB1)
    ]
    }).getIndex(1)
}
}];  */

for(var i in Data) {    
 
  var item = Data[i];   

  this.dataRow1.push({ 
    value: Data[i].AGE_15_25, category: "Age 15-25",
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(0)
    }
      
  });
  this.dataRow1.push({ 
    value: Data[i].AGE_26_35, category: "Age 26-35",
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(1)
    }
      
  });
  this.dataRow1.push({ 
    value: Data[i].AGE_36_45, category: "Age 36-45",
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(2)
    }
      
  });
  this.dataRow1.push({ 
    value: Data[i].AGE_46_60 , category: "Age 46-60",
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(3)
    }
      
  });
  this.dataRow1.push({ 
    value: Data[i].ABOVE_60 , category: "Above 60",
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         am5.color(0xFF6F91),
         am5.color(0x29AB87)
        ]
        }).getIndex(4)
    }
      
  });
}
var data =this.dataRow1;
// Craete Y-axis
var yAxis = chart.yAxes.push(
am5xy.ValueAxis.new(root, {
  renderer: am5xy.AxisRendererY.new(root, {})
})
);

// Create X-Axis
var xAxis = chart.xAxes.push(
am5xy.CategoryAxis.new(root, {
  maxDeviation: 0.2,
  renderer: am5xy.AxisRendererX.new(root, {
  }),
  categoryField: "category"
})
);
xAxis.data.setAll(data);

// Create series
var series = chart.series.push( 
am5xy.ColumnSeries.new(root, { 
  name: "Series", 
  xAxis: xAxis, 
  yAxis: yAxis, 
  valueYField: "value", 
  categoryXField: "category",
  tooltip: am5.Tooltip.new(root, {
    labelText:'HI'
  })
}) 
);

series.columns.template.setAll({
templateField: "settings"
});

series.data.setAll(data);

// Add legend
var legend = chart.children.push(am5.Legend.new(root, {
nameField: "categoryX",
centerX: am5.percent(50),
x: am5.percent(50)
}));

legend.data.setAll(series.dataItems);

}
catch (error) {

}
    
  }



 


  castearray :any[] = []; dataRow2:any []=[];req2:any={};
  async getCaste(selectradiotype:any,reporttype:any): Promise<void>  {
   try{
    this.maybeDisposeRoot("chartdivpie2");
  }
   catch (error) {
  
  }
       
     this.spinner.hide();
     try{
    
    
  if(selectradiotype=="total")
  {
    this.isgenchecked=true;
    this.req2 = {
      type: '204',
      input01:this.roleurid,
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="urban")
  {
    this.isgenchecked=false;
    this.req2 = {
      type: '204',
      input01:"U",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="rural")
  {
    this.isgenchecked=false;
    this.req2 = {
      type: '204',
      input01:"R",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
      this.dataRow2=[];
      this.castearray=[];
   
      let urlservice="api/swachaandhra/Dashboard";
      let rsdata = await this.apipost.clinetposturlauth_withoutenc(this.req2, urlservice);
      this.spinner.hide();
      this.castearray=rsdata.Details;
      debugger;
     
      let Data= this.castearray;
      for(var i in Data){
        var item = Data[i]; 
        this.dataRow2.push({ 
          value: Data[i].OC, category: "OC"
            
        });
        this.dataRow2.push({ 
          value: Data[i].BC, category: "BC"
            
        });
        this.dataRow2.push({ 
          value: Data[i].SC, category: "SC"
            
        });
        this.dataRow2.push({ 
          value: Data[i].ST , category: "ST"
            
        });  
      }
     
      this.getpiechart(this.dataRow2,"chartdivpie2");
    }
  catch (error) {
  
  }
      
    }

  


    edstatusarray: any[] = [];  dataRow3:any []=[];req3:any={};
    async getEduStatus(selectradiotype:any,reporttype:any): Promise<void> {
   debugger;
      try {
        this.maybeDisposeRoot("chartdivpie3");
   
     
  }
   catch (error) {
  
  }
       
     this.spinner.hide();
  
     try {
   
    
  if(selectradiotype=="total")
  {
    this.isgenchecked=true;
    this.req3 = {
      type: '203',
      input01:this.roleurid,
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="urban")
  {
    this.isgenchecked=false;
    this.req3 = {
      type: '203',
      input01:"U",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="rural")
  {
    this.isgenchecked=false;
    this.req3 = {
      type: '203',
      input01:"R",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
      
      this.dataRow3=[];
      this.edstatusarray =[];
      let urlservice = "api/swachaandhra/Dashboard";
       let rsdata = await this.apipost.clinetposturlauth_withoutenc(this.req3, urlservice);
      // let responce = await this.apipost.clinetposturlauth(req, urlservice);
      // let rsdata = JSON.parse(this.mid.deccall(responce.data));
  
       this.spinner.hide();
       debugger;
      this.edstatusarray = rsdata.Details;
 
      let Data =this.edstatusarray;
     debugger;
     for(var i in Data) {    

      var item = Data[i];   
    
      this.dataRow3.push({ 
        value: Data[i].NOT_LITERATE, category: "Illiterate"
          
      });
      this.dataRow3.push({ 
        value: Data[i].UPTO_5TH_STD, category: "Upto 5th std"
          
      });
      this.dataRow3.push({ 
        value: Data[i].UPTO_8TH_STD, category: "Upto 8th std"
          
      });
      this.dataRow3.push({ 
        value: Data[i].UPTO_10TH_STD , category: "Upto 10th std"
          
      });
      this.dataRow3.push({ 
        value: Data[i].UPTO_12TH_STD , category: "Upto 12th std"
          
      });
      this.dataRow3.push({ 
        value: Data[i].DIPLOMA_VOCATIONAL , category: "Graduation"
          
      });
      this.dataRow3.push({ 
        value: Data[i].DIPLOMA_PROFESSIONAL , category: "Post Graduation"
          
      });
  }
 
this.getpiechart(this.dataRow3,"chartdivpie3");
}
      catch (error) {
  
      }
    }

  
  
    famarray: any[] = [];  dataRow4:any []=[];req4:any={};
    async getfamilystatus(selectradiotype:any,reporttype:any): Promise<void> {
   debugger;
      try {
        this.maybeDisposeRoot("chartdivpie4");
      
  }
   catch (error) {
  
  }
       
     this.spinner.hide();
  
     try {
   
    
  if(selectradiotype=="total")
  {
    this.isgenchecked=true;
    this.req4 = {
      type: '205',
      input01:this.roleurid,
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="urban")
  {
    this.isgenchecked=false;
    this.req4 = {
      type: '205',
      input01:"U",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }
  else if(selectradiotype=="rural")
  {
    this.isgenchecked=false;
    this.req4 = {
      type: '205',
      input01:"R",
      input02:this.roledistid,
      input03:this.roleulbid,
      input04:this.rolemanid,
    };
  }

      
      this.dataRow4=[];
      this.famarray =[];
      let urlservice = "api/swachaandhra/Dashboard";
       let rsdata = await this.apipost.clinetposturlauth_withoutenc(this.req4, urlservice);
      // let responce = await this.apipost.clinetposturlauth(req, urlservice);
      // let rsdata = JSON.parse(this.mid.deccall(responce.data));
  
       this.spinner.hide();
       debugger;
      this.famarray = rsdata.Details;
      console.log(this.famarray);
      let Data =this.famarray;
     debugger;
     for(var i in Data) {    

      var item = Data[i];   
    
      this.dataRow4.push({ 
        value: Data[i].NOT_LITERATE, category: "Illiterate"
          
      });
      this.dataRow4.push({ 
        value: Data[i].UPTO_5TH_STD, category: "Upto 5th std"
          
      });
      this.dataRow4.push({ 
        value: Data[i].UPTO_8TH_STD, category: "Upto 8th std"
          
      });
      this.dataRow4.push({ 
        value: Data[i].UPTO_10TH_STD , category: "Upto 10th std"
          
      });
      this.dataRow4.push({ 
        value: Data[i].UPTO_12TH_STD , category: "Upto 12th std"
          
      });
      this.dataRow4.push({ 
        value: Data[i].DIPLOMA_VOCATIONAL , category: "Graduation"
          
      });
      this.dataRow4.push({ 
        value: Data[i].DIPLOMA_PROFESSIONAL , category: "Post Graduation"
          
      });
  }

this.getpiechart(this.dataRow4,"chartdivpie4");
}
     catch (error) {
  
     }
      
    }
    root1:any='';xAxis1:any='';yAxis1:any='';chart1:any='';
    Barchartreport(datarow:any,piechartid:any)
    {
      
  /* Chart code */
  // Create root element
  // https://www.amcharts.com/docs/v5/getting-started/#Root_element
  this.root1 = am5.Root.new(piechartid);
  
  // Set themes
  // https://www.amcharts.com/docs/v5/concepts/themes/
  this.root1.setThemes([
    am5themes_Animated.new(this.root1)
  ]);
  
  // Create chart
  // https://www.amcharts.com/docs/v5/charts/xy-chart/
  this.chart1 = this.root1.container.children.push(am5xy.XYChart.new(this.root1, {
    panX: false,
    panY: false,
    wheelX: "panX",
    wheelY: "zoomX",
    pinchZoomX: false,
    paddingLeft:0
  }));
  
  // Add cursor
  // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
  let cursor = this.chart1.set("cursor", am5xy.XYCursor.new(this.root1, {}));
  cursor.lineY.set("visible", false);
  
  
  // Create axes
  // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
  let xRenderer = am5xy.AxisRendererX.new(this.root1, { 
    minGridDistance: 30, 
    
  });
  
 
  
  xRenderer.grid.template.setAll({
    location: 1
  })
  
  this.xAxis1 = this.chart1.xAxes.push(am5xy.CategoryAxis.new(this.root1, {
    maxDeviation: 0.1,
    categoryField: "country",
    renderer: xRenderer,
    tooltip: am5.Tooltip.new(this.root1, {})
  }));
  
  let yRenderer = am5xy.AxisRendererY.new(this.root1, {
    strokeOpacity: 0.1
  })
  
  this.yAxis1 = this.chart1.yAxes.push(am5xy.ValueAxis.new(this.root1, {
    maxDeviation: 0.1,
    renderer: yRenderer
  }));
  let legend = this.chart1.children.push(am5.Legend.new(this.root1, {
    centerY: am5.p0,
    y: am5.p0
  }));
  
  // Create series
  // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
  for (let i = 0; i < (datarow.length); i++) {
  let series = this.chart1.series.push(am5xy.ColumnSeries.new(this.root1, {
    name:datarow[i].country,
    xAxis: this.xAxis1,
    yAxis: this.yAxis1,
    valueYField: "value",
    sequencedInterpolation: true,
    categoryXField: "country",
    tooltip: am5.Tooltip.new(this.root1, {
      labelText: "{valueY}"
    })
  }));
 


  series.columns.template.setAll({ cornerRadiusTL: 5, cornerRadiusTR: 5, strokeOpacity: 0 });
 /*  series.columns.template.adapters.add("fill", function (fill, target) {
    return  am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(series.columns.indexOf(target));
  });
  
  series.columns.template.adapters.add("stroke", function (stroke, target) {
    return  am5.ColorSet.new(root, {
      colors: [
        am5.color(0x73556E),
        am5.color(0x9FA1A6),
        am5.color(0xF28F6B),
       // am5.color(0xFF6F91)
       am5.color(0xD65DB1)
      ]
      }).getIndex(series.columns.indexOf(target));
  });   */
  
  
  // Set data
  
  
  this.xAxis1.data.setAll(datarow);
  if(i==0)
  {
  series.data.setAll(datarow);
  series.appear(1000);
  }
  legend.data.push(series);
}
legend.data.setAll(this.chart1.series.values); 
  
  // Make stuff animate on load
  // https://www.amcharts.com/docs/v5/concepts/animations/
 
  this.chart1.appear(1000, 100);
  
    }

   

    HorBarchartreport1(datarow:any,piechartid:any)
    {
      debugger;
      /* Chart code */
// Create root element
// https://www.amcharts.com/docs/v5/getting-started/#Root_element
let root = am5.Root.new(piechartid);


// Set themes
// https://www.amcharts.com/docs/v5/concepts/themes/
root.setThemes([
  am5themes_Animated.new(root)
]);


// Create chart
// https://www.amcharts.com/docs/v5/charts/xy-chart/
let chart = root.container.children.push(am5xy.XYChart.new(root, {
  panX: false,
  panY: false,
  wheelX: "panX",
  wheelY: "zoomX",
  paddingLeft:0,
  layout: root.verticalLayout
}));


// Add legend
// https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/



// Data
let data1 = [{
  year: "2017",
  income: 23.5
}, {
  year: "2018",
  income: 26.2
}, {
  year: "2019",
  income: 30.1
}, {
  year: "2020",
  income: 29.5
}, {
  year: "2021",
  income: 24.6
}];
let data = datarow;
//datarow=data1;

// Create axes
// https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
let yAxis = chart.yAxes.push(am5xy.CategoryAxis.new(root, {
  categoryField: "year",
  renderer: am5xy.AxisRendererY.new(root, {
    inversed: true,
    cellStartLocation: 0.1,
    cellEndLocation: 0.9,
  
  })
}));

yAxis.data.setAll(datarow);

let xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
  renderer: am5xy.AxisRendererX.new(root, {
    strokeOpacity: 0.1,
    minGridDistance: 50
  }),
  min: 0
}));


// Add series
// https://www.amcharts.com/docs/v5/charts/xy-chart/series/


//createSeries("income", "Income");
//createSeries("expenses", "Expenses");
let legend = chart.children.push(am5.Legend.new(root, {
  centerX: am5.percent(50),
  x: am5.percent(50)
}));

for (let i = 0; i < (data.length); i++) {
  let series = chart.series.push(am5xy.ColumnSeries.new(root, {
    name:  data[i].year,
    xAxis: xAxis,
    yAxis: yAxis,
    valueXField: "income",
    categoryYField: "year",
    sequencedInterpolation: true,
    tooltip: am5.Tooltip.new(root, {
      pointerOrientation: "horizontal",
      labelText: "[bold][/]\n{categoryY}: {valueX}"
    })
  }));
  
  series.columns.template.setAll({
    height: am5.p100,
    strokeOpacity: 0
  });
  
  
  series.bullets.push(function () {
    return am5.Bullet.new(root, {
      locationX: 1,
      locationY: 0.5,
      sprite: am5.Label.new(root, {
        centerY: am5.p50,
        text: "{valueX}",
        populateText: true
      })
    });
  });
  
  series.bullets.push(function () {
    return am5.Bullet.new(root, {
      locationX: 1,
      locationY: 0.5,
      sprite: am5.Label.new(root, {
        centerX: am5.p100,
        centerY: am5.p50,
        text: "",
        fill: am5.color(0xffffff),
        populateText: true
      })
    });
  });
/* series.columns.template.adapters.add("fill", function (fill, target) {
  return  am5.ColorSet.new(root, {
    colors: [
      am5.color(0x73556E),
      am5.color(0x9FA1A6),
      am5.color(0xF28F6B),
     // am5.color(0xFF6F91)
     am5.color(0xD65DB1)
    ]
    }).getIndex(series.columns.indexOf(target));
});

series.columns.template.adapters.add("stroke", function (stroke, target) {
  return  am5.ColorSet.new(root, {
    colors: [
      am5.color(0x73556E),
      am5.color(0x9FA1A6),
      am5.color(0xF28F6B),
     // am5.color(0xFF6F91)
     am5.color(0xD65DB1)
    ]
    }).getIndex(series.columns.indexOf(target));
});   */
if(i==0)
{
  series.data.setAll(data);
}
 series.appear();
  
  legend.data.push(series);
}

// Add legend
// https://www.amcharts.com/docs/v5/charts/xy-chart/legend-xy-series/


legend.data.setAll(chart.series.values); 


// Add cursor
// https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
let cursor = chart.set("cursor", am5xy.XYCursor.new(root, {
  behavior: "zoomY"
}));
cursor.lineY.set("forceHidden", true);
cursor.lineX.set("forceHidden", true);


// Make stuff animate on load
// https://www.amcharts.com/docs/v5/concepts/animations/
chart.appear(1000, 100);

    }

    HorBarchartreport(datarow:any,piechartid:any)
    {
      debugger;
      /* Chart code */
  // Create root element
  // https://www.amcharts.com/docs/v5/getting-started/#Root_element
  let root = am5.Root.new(piechartid);
  
  root.setThemes([
  am5themes_Animated.new(root)
  ]);
  
  let chart = root.container.children.push(am5xy.XYChart.new(root, {
  panX: false,
  panY: false,
  wheelX: "panX",
  layout: root.verticalLayout
  }));
  
  var data = [{ 
    category: "Research", 
    value:100,
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(4)
    }
  }, { 
    category: "Marketing", 
    value: 200,
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(2)
    }
  }, { 
    category: "Sales", 
    value:300,
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(1)
    }
  }];
  
  
  let yAxis = chart.yAxes.push(am5xy.CategoryAxis.new(root, {
    maxDeviation: 0.2,
  categoryField: "category",
  renderer: am5xy.AxisRendererY.new(root, {
  
  
  })
  }));
  
  
  yAxis.data.setAll(data);
  
  let xAxis = chart.xAxes.push(am5xy.ValueAxis.new(root, {
  renderer: am5xy.AxisRendererX.new(root, {
    
  })
  }));
  
  
  
  let series = chart.series.push(am5xy.ColumnSeries.new(root, {
    name:  "Series",
    xAxis: xAxis,
    yAxis: yAxis,
    valueXField: "value",
    categoryYField: "category",
    tooltip: am5.Tooltip.new(root, {
      pointerOrientation: "horizontal",
      labelText: "[bold][/]\n{categoryY}: {valueX}"
     })
  }));
  
  
  
  
  series.columns.template.setAll({
    templateField: "settings"
  });
  
  series.data.setAll(data);
  
  
  var legend = chart.children.push(am5.Legend.new(root, {
    nameField: "categoryY",
    centerX: am5.percent(50),
    x: am5.percent(50)
  }));
  
  legend.data.setAll(series.dataItems);
  
  
  
  
    }
  
    vrtr()
    {
      var root = am5.Root.new("chartdiv"); 
  
  root.setThemes([
    am5themes_Animated.new(root)
  ]);
  
  var chart = root.container.children.push( 
    am5xy.XYChart.new(root, {
      panY: false,
      wheelY: "zoomX",
      layout: root.verticalLayout
    }) 
  );
  
  // Define data
  var data = [{ 
    category: "Research", 
    value: 0,
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(4)
    }
  }, { 
    category: "Marketing", 
    value: 0,
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(2)
    }
  }, { 
    category: "Sales", 
    value:0,
    settings: {
      fill: am5.ColorSet.new(root, {
        colors: [
          am5.color(0x73556E),
          am5.color(0x9FA1A6),
          am5.color(0xF28F6B),
         // am5.color(0xFF6F91)
         am5.color(0xD65DB1)
        ]
        }).getIndex(1)
    }
  }];
  
  // Craete Y-axis
  var yAxis = chart.yAxes.push(
    am5xy.ValueAxis.new(root, {
      renderer: am5xy.AxisRendererY.new(root, {})
    })
  );
  
  // Create X-Axis
  var xAxis = chart.xAxes.push(
    am5xy.CategoryAxis.new(root, {
      maxDeviation: 0.2,
      renderer: am5xy.AxisRendererX.new(root, {
      }),
      categoryField: "category"
    })
  );
  xAxis.data.setAll(data);
  
  // Create series
  var series = chart.series.push( 
    am5xy.ColumnSeries.new(root, { 
      name: "Series", 
      xAxis: xAxis, 
      yAxis: yAxis, 
      valueYField: "value", 
      categoryXField: "category",
      tooltip: am5.Tooltip.new(root, {})
    }) 
  );
  
  series.columns.template.setAll({
    templateField: "settings"
  });
  
  series.data.setAll(data);
  
  // Add legend
  var legend = chart.children.push(am5.Legend.new(root, {
    nameField: "categoryX",
    centerX: am5.percent(50),
    x: am5.percent(50)
  }));
  
  legend.data.setAll(series.dataItems);
    }
   

}
