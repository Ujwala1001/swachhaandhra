import { Component, ElementRef, Input, ViewChild } from '@angular/core';


import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';

import { Router } from '@angular/router';
 

import { NgxSpinnerService } from 'ngx-spinner';
import { any } from 'cypress/types/bluebird';

@Component({
  selector: 'app-state-home',
  templateUrl: './state-home.component.html',
  styleUrls: ['./state-home.component.css']
})
export class StateHomeComponent {
  Swatch_Ur:any="U";
  ischecked:boolean=false;
 

  constructor(

    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,


  ) {
  
  }
  roleid:any='';roledistid:any=''
  ngOnInit(): void {
    debugger;
    this.roleid=sessionStorage.getItem("userrole");
    if (
      (sessionStorage.getItem('username') == '')) {
    
        this.router.navigate(['/NewHome']); return;
      }
      if (this.roleid=='1004' || this.roleid=='1003')
      { 
        this.Swatch_Ur=sessionStorage.getItem('userurtype');
        this.roledistid= sessionStorage.getItem('userdistcode');
        if(this.Swatch_Ur =="R")
        {
          if (this.roleid=='1004')
          {
            this.CountLoad(sessionStorage.getItem('userdistcode'),"",sessionStorage.getItem('usermandalcode'));
          }
          else{
            this.CountLoad(sessionStorage.getItem('userdistcode'),"","");
          }

        }
        else{
             this.CountLoad(sessionStorage.getItem('userdistcode'),sessionStorage.getItem('userulbcode'),"");
        }
      }
      else   if (this.roleid=='1001' || this.roleid=='1002'|| this.roleid=='1006')
      {
        this.roledistid='';
        this.Swatch_Ur="U";
        this.ischecked=true;
        this.CountLoad("","","");
      }
   
  
  
  }

 


  URchange(URID:any)
  {
   this.Swatch_Ur=URID;
   if(this.Swatch_Ur!='')
   {
    this.CountLoad("","","");
   }
   



}

countdata: any[] = [];
TOTAL_NUMBER_OF_WARDS_Village_COUNTS_Name:any='';
TOTAL_NUMBER_OF_WARDS_Village_COUNTS:any='';
COMPLETED_COUNT_Name:any='';
PENDING_COUNT_Name:any='';
MALE_COUNT_Name:any='';
FEMALE_COUNT_Name:any='';
TRANSGENDER_COUNT_Name:any='';
COMPLETED_COUNT:any='';
PENDING_COUNT:any='';
MALE_COUNT:any='';
FEMALE_COUNT:any='';
TRANSGENDER_COUNT:any='';
async  CountLoad(distcode:any,ulbcode:any,mandalcode:any): Promise<void> 
{
 
  debugger;
    const req = {
      type: "138",
      input02:this.Swatch_Ur.toString(),
      input03:distcode.toString(),
      input04:ulbcode.toString(),
      input05:mandalcode.toString(),
    }
    this.countdata=[];   
    let urlservice = "api/swachaandhra/SwachhAndhra_MastersMob";
     let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, urlservice);
    // let responce = await this.apipost.clinetposturlauth(req, urlservice);
    // let rsdata = JSON.parse(this.mid.deccall(responce.data));
    console.log(rsdata.Details);
    this.countdata=rsdata.Details; 
    if(this.countdata.length > 0)
    {
      if(this.Swatch_Ur.toString() =='U')
      {
        
        this.TOTAL_NUMBER_OF_WARDS_Village_COUNTS=this.countdata[0].TOTAL_NUMBER_OF_WARDS_COUNTS == null ? 0 : this.countdata[0].TOTAL_NUMBER_OF_WARDS_COUNTS;
        this.TOTAL_NUMBER_OF_WARDS_Village_COUNTS_Name='Total No.of wards';
        this.COMPLETED_COUNT_Name='No.of wards in which the survey has completed';
        this.PENDING_COUNT_Name='No.of wards in which the survey is pending';
        
      }
      else if(this.Swatch_Ur.toString() =='R')
      {
        this.TOTAL_NUMBER_OF_WARDS_Village_COUNTS=this.countdata[0].TOTAL_NUMBER_OF_VILLAGES_COUNTS == null ? 0 : this.countdata[0].TOTAL_NUMBER_OF_VILLAGES_COUNTS;
        this.TOTAL_NUMBER_OF_WARDS_Village_COUNTS_Name='Total No.of Villages';
        this.COMPLETED_COUNT_Name='No.of Villages in which the survey has completed';
        this.PENDING_COUNT_Name='No.of Villages in which the survey is pending';
    
      }
      this.MALE_COUNT_Name='Total No.of male sanitation workers';
      this.FEMALE_COUNT_Name='Total No.of female sanitation workers';
      this.TRANSGENDER_COUNT_Name='Total No.of transgender workers';
      this.COMPLETED_COUNT=this.countdata[0].COMPLETED_COUNT == null ? 0 : this.countdata[0].COMPLETED_COUNT;
      this.PENDING_COUNT=this.countdata[0].PENDING_COUNT == null ? 0 : this.countdata[0].PENDING_COUNT;
      this.MALE_COUNT=this.countdata[0].MALE_COUNT == null ? 0 : this.countdata[0].MALE_COUNT;
      this.FEMALE_COUNT=this.countdata[0].FEMALE_COUNT == null ? 0 : this.countdata[0].FEMALE_COUNT;
      this.TRANSGENDER_COUNT=this.countdata[0].TRANSGENDER_COUNT == null ? 0 : this.countdata[0].TRANSGENDER_COUNT;
    }
  }
 


}
