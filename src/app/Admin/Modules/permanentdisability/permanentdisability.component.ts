import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-permanentdisability',
  templateUrl: './permanentdisability.component.html',
  styleUrls: ['./permanentdisability.component.css']
})
export class PermanentdisabilityComponent {

  constructor(

    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,


  ) {
  
  }
  roleid:any='';roledistid:any='';
  ngOnInit(): void {
    debugger;
    this.roleid=sessionStorage.getItem("userrole");
    if (
    (sessionStorage.getItem('username') == '')) {

     this.router.navigate(['/NewHome']); return;
    }
    if (this.roleid=='1004' || this.roleid=='1003')
    { 
      this.Swatch_Ur=sessionStorage.getItem('userurtype');
      this.roledistid= sessionStorage.getItem('userdistcode');
      this.GetSurveyReportdata();
    }
    else   if (this.roleid=='1001' || this.roleid=='1002'|| this.roleid=='1006')
    {
      this.roledistid='0';
      this.Swatch_Ur="U";
      this.ischecked=true;
      this.GetSurveyReportdata();
    }
    
  }

  URchange(URID:any)
  {
   this.Swatch_Ur=URID;
   if(this.Swatch_Ur!='')
   {
    this.GetSurveyReportdata();
   }
  }
  distarray: any[] = [];Swatch_DISTRICT:any='0';  dataRow:any []=[];
  Swatch_Ur:any="";
  ischecked:boolean=false;
  ischecked1:boolean=false;
  VillageSurveyReportdata: any[] = [];MandalSurveyReportdata: any[] = [];
  GpSurveyReportdata: any[] = [];Reportlevel:any=0;
  SurveyReportdata: any[] = [];typeid:any='';
  async  GetSurveyReportdata(): Promise<void> 
  {
    debugger;
    sessionStorage.setItem('dist', '');
    sessionStorage.setItem('distname', '');
    sessionStorage.setItem('mandal', '');
    sessionStorage.setItem('mandalname', '');
    this.Reportlevel=0;
      const req = {
        type: "149",
        input01:this.Swatch_Ur.toString(),
        input02:this.roledistid.toString(),
      }
      this.SurveyReportdata=[];   
      this.details=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.SurveyReportdata=rsdata1.Details; 
        this.sumtotalrows(rsdata.Details,"DISTRICT_NAME");
        this.SurveyReportdata.push(this.details);
    }
    details:any[]=[];
    async  sumtotalrows(details:any,column:any): Promise<void> 
    {
      const scores =details; 
   
      const sumScores = (arr:any) => {
        return arr.reduce((acc:any, val:any) => {
          acc[column] = 'Total';
           Object.keys(val).forEach(key => {
            /*   if((key!== 'DISTRICT_NAME1')){ */
                 acc[key] += val[key];
             /*  }; */
           });
         /*   if((acc['DISTRICT_NAME'] !== 'all')){ */
              acc[column] = 'Total';
           /* }; */
           return acc;
        });
       
     };
     
     debugger;
     let det=sumScores(details);
     if(details.length ==1)
     {
      det[column]='Total';
     }
    for(var k in det) {
      if(k != "0")
      {
        delete scores[k];
      }
      
   } 
   this.details=det;
    }
    District:any='';Mandal:any='';Gp:any='';
    async  GetMandalSurveyReportdata(Distrctcode:any,Distname:any): Promise<void> 
  {
    this.District=Distname;
    sessionStorage.setItem('dist', Distrctcode);
    sessionStorage.setItem('distname', Distname);
    this.Reportlevel++;
    let rolemandalcode:any='';
    if (this.roleid=='1004' || this.roleid=='1003')
    {
      if(this.Swatch_Ur.toString() =="U")
      {
        rolemandalcode=sessionStorage.getItem('userulbcode');
      }
      else  if(this.Swatch_Ur.toString() =="R")
      {
        if(this.roleid=='1003')
      {
        rolemandalcode="0";
      }
      else{
        rolemandalcode=sessionStorage.getItem('usermandalcode');
      }
       
      }
     
    }
    else{
      rolemandalcode='0';
    }
    debugger;
      const req = {
        type:  this.Swatch_Ur.toString() =="U" ?"153":"150",
        input01:Distrctcode.toString(),
        input02:this.Swatch_Ur.toString(),
        input03:rolemandalcode.toString(),
      }
      this.MandalSurveyReportdata=[];  
      this.details=[]; 
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      if(rsdata.code)
        {
          this.MandalSurveyReportdata=rsdata.Details; 
          this.sumtotalrows(rsdata1.Details,"MANDAL_NAME");
          this.MandalSurveyReportdata.push(this.details);
        }
        else{
          this.alt.toasterror('No data found')
        }
      
    }

    async  GetGpSurveyReportdata(mandalname:any,Mandalcode:any): Promise<void> 
  {
    this.District=sessionStorage.getItem('distname');
    this.Mandal=mandalname;
    sessionStorage.setItem('mandal', Mandalcode);
    sessionStorage.setItem('mandalname', mandalname);
    this.Reportlevel++;
    let rolegpcode:any='';
    if (this.roleid=='1004' || this.roleid=='1003')
    {
      if(this.Swatch_Ur.toString() =="U")
      {
        rolegpcode='0';
      }
      else  if(this.Swatch_Ur.toString() =="R")
      {
       
        rolegpcode="0";
        
      }
     
    }
    else{
      rolegpcode='0';
    }
    debugger;
    const req = {
      type: this.Swatch_Ur.toString() =="U" ?"154":"151",
      input01:sessionStorage.getItem('dist'),
      input02:Mandalcode.toString(),
      input03:this.Swatch_Ur.toString(),
      input04:rolegpcode.toString(),
    }
      this.GpSurveyReportdata=[];   
      this.details=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.GpSurveyReportdata=rsdata.Details; 
      this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
      this.GpSurveyReportdata.push(this.details);
    }

    async  GetVillageSurveyReportdata(Gpname:any,gpcode:any): Promise<void> 
  {
    this.Gp=Gpname;
    this.Reportlevel++;
    debugger;
      const req = {
        type: "152",
        input01:sessionStorage.getItem('dist'),
        input02:sessionStorage.getItem('mandal'),
        input03:gpcode.toString(),
        input04:this.Swatch_Ur.toString(),
      }
      this.VillageSurveyReportdata=[];
      this.details=[];   
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.VillageSurveyReportdata=rsdata.Details; 
      this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
      this.VillageSurveyReportdata.push(this.details);
    }
  
    Back()
    {
      this.Reportlevel--;
    }
  


    Data1:any[]=[]; Ur:any=''
    mandalname='';
    villagename='';
exportSlipXl() {
 
  if (this.Swatch_Ur == 'U') {
    this.Ur = 'Urban';
    this.mandalname='Urban Local Body';
    this.villagename='Ward';
  }
  else if (this.Swatch_Ur == 'R') {
    this.Ur = 'Rural';
    this.mandalname='Mandal';
    this.villagename='Gram panchayat';
  }
   
    if(this.Reportlevel =='0')
    {
      this.Data1= this.SurveyReportdata;
    }
    if(this.Reportlevel =='1')
    {
      this.Data1= this.MandalSurveyReportdata;
    }
    if(this.Reportlevel =='2')
    {
      this.Data1= this.GpSurveyReportdata;
    }
    if(this.Reportlevel =='3')
    {
      this.Data1= this.VillageSurveyReportdata;
    }
    const Data=this.Data1;
if(this.SurveyReportdata.length > 0)
{
    const checkdata = Math.max(Data.length);
    if(checkdata>0){
      debugger;
      //firstreport

      var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','','','','','SAFAIMITRA SURVEY - HEALTH DATA REPORTS','','','','','','','','',''],
      );
      body.push(dataRow);
      if(this.Reportlevel =='1')
      {
        body.push(['','','District :',this.District,'','','','','','','','','','','','','',''],
        );
      }
      else if(this.Reportlevel =='2')
      {
        body.push(['','','District :',this.District,'','','Mandal :',this.Mandal,'','','','','','','','','',''],
        );
      }
      else if(this.Reportlevel =='3')
      {
        body.push(['','','District :',this.District,'','','Mandal :',this.Mandal,'','','Grampanchayat/Ward :',this.Gp,'','','','','',''],
        );
      }
      body.push(['','','','','Health Issues','','','','','','','','','','','','','','Disease','','','','','','','','','','','','Disability',''],
      );
    
      body.push(['','','','Previously suffered Health Issues','','','Currently suffered Health Issues','','','Skin Diseases','','','Respiratory','','','Liver Diseases','','','Cancer','','','Cardiac/Heart problems','','','Hypertension/BP','','','Mental illness','','','Others','','','Permanent disability',''],
      );
      if(this.Reportlevel =='0')
      {
      body.push(['S.no','District',
      'Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender',
      'Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender',],);
      //body
      for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data[i].DISTRICT_NAME,
        Data[i].PREVIOUS_HEALTH_ISSUE_MALE,Data[i].PREVIOUS_HEALTH_ISSUE_FEMALE,Data[i].PREVIOUS_HEALTH_ISSUE_TRANSGENDER,Data[i].CURRENT_HEALTH_ISSUE_MALE,Data[i].CURRENT_HEALTH_ISSUE_FEMALE,
        Data[i].CURRENT_HEALTH_ISSUE_TRANSGENDER,Data[i].MALE_SKIN_DISEASES,Data[i].FEMALE_SKIN_DISEASES,Data[i].TRANSGENDER_SKIN_DISEASES,Data[i].MALE_RESPIRATORY,
        Data[i].FEMALE_RESPIRATORY,Data[i].TRANSGENDER_RESPIRATORY,Data[i].MALE_LIVER_DISEASES,Data[i].FEMALE_LIVER_DISEASES,Data[i].TRANSGENDER_LIVER_DISEASES,
        Data[i].MALE_CANCER,Data[i].FEMALE_CANCER,Data[i].TRANSGENDER_CANCER,Data[i].MALE_CARDIAC_HEART_PROBLLEMS,Data[i].FEMALE_CARDIAC_HEART_PROBLLEMS,Data[i].TRANSGENDER_CARDIAC_HEART_PROBLLEMS,Data[i].MALE_HYPERTENSION_BP,Data[i].FEMALE_HYPERTENSION_BP,
        Data[i].TRANSGENDER_HYPERTENSION_BP,Data[i].MALE_MENTAL_ILLNESS,Data[i].FEMALE_MENTAL_ILLNESS,Data[i].TRANSGENDER_MENTAL_ILLNESS,Data[i].MALE_OTHERS,Data[i].FEMALE_OTHERS,Data[i].TRANSGENDER_OTHERS,Data[i].PERMANENT_DISABLED_MALE,Data[i].PERMANENT_DISABLED_FEMALE,Data[i].PERMANENT_DISABLED_TRANSGENDER,
      ];
        body.push(dataRow);
      }
    }
    else  if(this.Reportlevel =='1')
    {
    body.push(['S.no',this.mandalname,
    'Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender',
    'Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender',],);
    //body
    for (let i = 0; i < Data.length; i++) {
      var dataRow = [];
      dataRow = [i+1,Data[i].MANDAL_NAME,
      Data[i].PREVIOUS_HEALTH_ISSUE_MALE,Data[i].PREVIOUS_HEALTH_ISSUE_FEMALE,Data[i].PREVIOUS_HEALTH_ISSUE_TRANSGENDER,Data[i].CURRENT_HEALTH_ISSUE_MALE,Data[i].CURRENT_HEALTH_ISSUE_FEMALE,
      Data[i].CURRENT_HEALTH_ISSUE_TRANSGENDER,Data[i].MALE_SKIN_DISEASES,Data[i].FEMALE_SKIN_DISEASES,Data[i].TRANSGENDER_SKIN_DISEASES,Data[i].MALE_RESPIRATORY,
      Data[i].FEMALE_RESPIRATORY,Data[i].TRANSGENDER_RESPIRATORY,Data[i].MALE_LIVER_DISEASES,Data[i].FEMALE_LIVER_DISEASES,Data[i].TRANSGENDER_LIVER_DISEASES,
      Data[i].MALE_CANCER,Data[i].FEMALE_CANCER,Data[i].TRANSGENDER_CANCER,Data[i].MALE_CARDIAC_HEART_PROBLLEMS,Data[i].FEMALE_CARDIAC_HEART_PROBLLEMS,Data[i].TRANSGENDER_CARDIAC_HEART_PROBLLEMS,Data[i].MALE_HYPERTENSION_BP,Data[i].FEMALE_HYPERTENSION_BP,
      Data[i].TRANSGENDER_HYPERTENSION_BP,Data[i].MALE_MENTAL_ILLNESS,Data[i].FEMALE_MENTAL_ILLNESS,Data[i].TRANSGENDER_MENTAL_ILLNESS,Data[i].MALE_OTHERS,Data[i].FEMALE_OTHERS,Data[i].TRANSGENDER_OTHERS,Data[i].PERMANENT_DISABLED_MALE,Data[i].PERMANENT_DISABLED_FEMALE,Data[i].PERMANENT_DISABLED_TRANSGENDER,
    ];
      body.push(dataRow);
    }
  }
  else  if(this.Reportlevel =='2')
  {
  body.push(['S.no',this.villagename,
  'Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender',
  'Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender',],);
  //body
  for (let i = 0; i < Data.length; i++) {
    var dataRow = [];
    if(this.Mandal !='Penamaluru' && this.Swatch_Ur =='U')
    {
    dataRow = [i+1,"Ward No"+Data[i].WARD_OR_VILLAGE_NAME,
    Data[i].PREVIOUS_HEALTH_ISSUE_MALE,Data[i].PREVIOUS_HEALTH_ISSUE_FEMALE,Data[i].PREVIOUS_HEALTH_ISSUE_TRANSGENDER,Data[i].CURRENT_HEALTH_ISSUE_MALE,Data[i].CURRENT_HEALTH_ISSUE_FEMALE,
    Data[i].CURRENT_HEALTH_ISSUE_TRANSGENDER,Data[i].MALE_SKIN_DISEASES,Data[i].FEMALE_SKIN_DISEASES,Data[i].TRANSGENDER_SKIN_DISEASES,Data[i].MALE_RESPIRATORY,
    Data[i].FEMALE_RESPIRATORY,Data[i].TRANSGENDER_RESPIRATORY,Data[i].MALE_LIVER_DISEASES,Data[i].FEMALE_LIVER_DISEASES,Data[i].TRANSGENDER_LIVER_DISEASES,
    Data[i].MALE_CANCER,Data[i].FEMALE_CANCER,Data[i].TRANSGENDER_CANCER,Data[i].MALE_CARDIAC_HEART_PROBLLEMS,Data[i].FEMALE_CARDIAC_HEART_PROBLLEMS,Data[i].TRANSGENDER_CARDIAC_HEART_PROBLLEMS,Data[i].MALE_HYPERTENSION_BP,Data[i].FEMALE_HYPERTENSION_BP,
    Data[i].TRANSGENDER_HYPERTENSION_BP,Data[i].MALE_MENTAL_ILLNESS,Data[i].FEMALE_MENTAL_ILLNESS,Data[i].TRANSGENDER_MENTAL_ILLNESS,Data[i].MALE_OTHERS,Data[i].FEMALE_OTHERS,Data[i].TRANSGENDER_OTHERS,Data[i].PERMANENT_DISABLED_MALE,Data[i].PERMANENT_DISABLED_FEMALE,Data[i].PERMANENT_DISABLED_TRANSGENDER,
  ];
}
else{
  dataRow = [i+1,Data[i].WARD_OR_VILLAGE_NAME,
  Data[i].PREVIOUS_HEALTH_ISSUE_MALE,Data[i].PREVIOUS_HEALTH_ISSUE_FEMALE,Data[i].PREVIOUS_HEALTH_ISSUE_TRANSGENDER,Data[i].CURRENT_HEALTH_ISSUE_MALE,Data[i].CURRENT_HEALTH_ISSUE_FEMALE,
  Data[i].CURRENT_HEALTH_ISSUE_TRANSGENDER,Data[i].MALE_SKIN_DISEASES,Data[i].FEMALE_SKIN_DISEASES,Data[i].TRANSGENDER_SKIN_DISEASES,Data[i].MALE_RESPIRATORY,
  Data[i].FEMALE_RESPIRATORY,Data[i].TRANSGENDER_RESPIRATORY,Data[i].MALE_LIVER_DISEASES,Data[i].FEMALE_LIVER_DISEASES,Data[i].TRANSGENDER_LIVER_DISEASES,
  Data[i].MALE_CANCER,Data[i].FEMALE_CANCER,Data[i].TRANSGENDER_CANCER,Data[i].MALE_CARDIAC_HEART_PROBLLEMS,Data[i].FEMALE_CARDIAC_HEART_PROBLLEMS,Data[i].TRANSGENDER_CARDIAC_HEART_PROBLLEMS,Data[i].MALE_HYPERTENSION_BP,Data[i].FEMALE_HYPERTENSION_BP,
  Data[i].TRANSGENDER_HYPERTENSION_BP,Data[i].MALE_MENTAL_ILLNESS,Data[i].FEMALE_MENTAL_ILLNESS,Data[i].TRANSGENDER_MENTAL_ILLNESS,Data[i].MALE_OTHERS,Data[i].FEMALE_OTHERS,Data[i].TRANSGENDER_OTHERS,Data[i].PERMANENT_DISABLED_MALE,Data[i].PERMANENT_DISABLED_FEMALE,Data[i].PERMANENT_DISABLED_TRANSGENDER,
];
}
    body.push(dataRow);
  }
}
else  if(this.Reportlevel =='3')
{
body.push(['S.no','Village',
'Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender',
'Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender','Male','Female','Third gender',],);
//body
for (let i = 0; i < Data.length; i++) {
  var dataRow = [];
  dataRow = [i+1,Data[i].WARD_OR_VILLAGE_NAME,
  Data[i].PREVIOUS_HEALTH_ISSUE_MALE,Data[i].PREVIOUS_HEALTH_ISSUE_FEMALE,Data[i].PREVIOUS_HEALTH_ISSUE_TRANSGENDER,Data[i].CURRENT_HEALTH_ISSUE_MALE,Data[i].CURRENT_HEALTH_ISSUE_FEMALE,
  Data[i].CURRENT_HEALTH_ISSUE_TRANSGENDER,Data[i].MALE_SKIN_DISEASES,Data[i].FEMALE_SKIN_DISEASES,Data[i].TRANSGENDER_SKIN_DISEASES,Data[i].MALE_RESPIRATORY,
  Data[i].FEMALE_RESPIRATORY,Data[i].TRANSGENDER_RESPIRATORY,Data[i].MALE_LIVER_DISEASES,Data[i].FEMALE_LIVER_DISEASES,Data[i].TRANSGENDER_LIVER_DISEASES,
  Data[i].MALE_CANCER,Data[i].FEMALE_CANCER,Data[i].TRANSGENDER_CANCER,Data[i].MALE_CARDIAC_HEART_PROBLLEMS,Data[i].FEMALE_CARDIAC_HEART_PROBLLEMS,Data[i].TRANSGENDER_CARDIAC_HEART_PROBLLEMS,Data[i].MALE_HYPERTENSION_BP,Data[i].FEMALE_HYPERTENSION_BP,
  Data[i].TRANSGENDER_HYPERTENSION_BP,Data[i].MALE_MENTAL_ILLNESS,Data[i].FEMALE_MENTAL_ILLNESS,Data[i].TRANSGENDER_MENTAL_ILLNESS,Data[i].MALE_OTHERS,Data[i].FEMALE_OTHERS,Data[i].TRANSGENDER_OTHERS,Data[i].PERMANENT_DISABLED_MALE,Data[i].PERMANENT_DISABLED_FEMALE,Data[i].PERMANENT_DISABLED_TRANSGENDER,
];
  body.push(dataRow);
}
}
      //footer
      body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length],
      );


      if(this.Reportlevel =='0')
      {
        this.mid.JSONToCSVConvertor(
          body,
          'District Level'+'_'+this.Ur+'_'+'Health Report Data',
          true
         );
      }
      if(this.Reportlevel =='1')
      {
        this.mid.JSONToCSVConvertor(
          body,
         this.District +'_'+this.Ur+'_'+ 'Health Report Data',
          true
         );
      }
      if(this.Reportlevel =='2')
      {
        this.mid.JSONToCSVConvertor(
          body,
          this.Mandal +'_'+this.Ur+'_'+'Health Report Data',
          true
         );
      }
      if(this.Reportlevel =='3')
      {
        this.mid.JSONToCSVConvertor(
          body,
          this.Gp +'_'+this.Ur+'_'+'Health Report Data',
          true
         );
      }

    }
  }
  }
 









  
  addModaldisplay:any='none';
  modalclose()
  {
    this.addModaldisplay = 'none';
    this.modaldata=[];
    this.educationdata=[];
    this.SANITATION_WORK_NAME='';
this.SURVEY_ID='';
        this.SANITATION_WORK_EDUCATIONAL_STATUS='';
  }




modaldata: any[] = [];
  async  GetModaldata(villagecode:any , urbancode:any): Promise<void> 
  {
   
    debugger;
      const req = {
        type: urbancode.toString() =="U" ?"100":"105",
      
        Input01: villagecode.toString()
        
       
      }
      this.modaldata=[];   
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
     
      this.modaldata=rsdata.Details; 


      this.addModaldisplay='block';
    }



    
    educationdata: any[] = [];SANITATION_WORK_NAME:any='';SANITATION_WORK_EDUCATIONAL_STATUS:any='';SURVEY_ID:any='';
    async  Vieweducation(surveyid:any,surveyid1:any ): Promise<void> 
    {
      this.addModaldisplay='block';
      debugger;
        const req = {
          
          type: this.Swatch_Ur.toString() =="U" ?"1544":"1522",
          Input06: surveyid.toString()

         
        }
        this.SANITATION_WORK_NAME='';
this.SURVEY_ID='';
        this.SANITATION_WORK_EDUCATIONAL_STATUS='';
        this.educationdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        //console.log(rsdata.Details);
        this.SURVEY_ID=rsdata.Details[0].SURVEY_ID;;
        this.SANITATION_WORK_NAME=rsdata.Details[0].SANITATION_WORK_NAME;this.SANITATION_WORK_EDUCATIONAL_STATUS=rsdata.Details[0].SANITATION_WORK_EDUCATIONAL_STATUS;
        this.educationdata=rsdata.Details; 

        
      }

}
