import { Component, ElementRef, Input, ViewChild } from '@angular/core';


import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';

import { Router } from '@angular/router';
 
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent {

  username = '';
  password = '';
  newpassword ='';
  isFocused=false;


  constructor(

    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,
    
    private val: InputvalidaService,
    private cookieService: CookieService,

  ) {
  }
  ngOnInit(): void {
  
    if ((sessionStorage.getItem('username') == '')) {

      this.router.navigate(['/Login']); return;
    }
  }
  logout()
  {
   
    sessionStorage.setItem('username', '');
    sessionStorage.setItem('_Uenc', '');
    sessionStorage.setItem('_hsk',  '');
  }
  async Login(): Promise<void> {
    debugger;
   
    if (this.val.isEmpty(this.username)) {
      this.alt.warning('Please enter username');
      this.isFocused=true;
      return;
    }
    else if (this.val.isEmpty(this.password)) {
      this.alt.warning('Please enter old password');
      this.isFocused=true;
      return ;
    }
    else if (this.val.isEmpty(this.newpassword)) {
      this.alt.warning('Please enter new password');
      this.isFocused=true;
      return ;
    }
    this.checkPasswordStrength();
    if(this.passwordStrength =='Strong')
    {
  
  
    const req = {
      type: '104',
      input01:this.username,
      input02:this.password,
      input03:this.newpassword
    }
    let url= "api/swachaandhra/SwachhAndhra_Changepassword";
    let responce = await this.apipost.clinetposturlauth(req, url); 
    let rsdata = JSON.parse(this.mid.deccall(responce.data));

   
    if (rsdata.code && rsdata.Details[0].STATUS=="1") {
      this.alt.success('Password updated successfully.');
      this.router.navigate(["/Home"]);
    }
    else 
    {
    
      this.alt.warning(rsdata.message);
    }
    
debugger;
  

}
else{
  this.newpassword ='';
  this.alt.warning('password should contain atleast one number and one special character');
}



   
  
  }
  passwordStrength:any='';
  checkPasswordStrength() {
    const password = this.newpassword;

    // Check password strength based on criteria
    if (password.length < 8) {
      this.passwordStrength = 'Weak';
    } else if (!/[A-Z]/.test(password) || !/[a-z]/.test(password) || !/[0-9]/.test(password)) {
      this.passwordStrength = 'Moderate';
    } else {
      this.passwordStrength = 'Strong';
    }
 
  }
  clearinput(){
    this.username = '';
    this.password = '';
    this.newpassword = ''; 
  }

}
