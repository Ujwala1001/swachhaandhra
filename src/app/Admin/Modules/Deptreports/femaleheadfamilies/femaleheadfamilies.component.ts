import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';

import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-femaleheadfamilies',
  templateUrl: './femaleheadfamilies.component.html',
  styleUrls: ['./femaleheadfamilies.component.css']
})
export class FemaleheadfamiliesComponent {

  constructor(

    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,


  ) {

  }
  roleid: any = ''; roledistid: any = '';
  ngOnInit(): void {
    debugger;
    this.roleid = sessionStorage.getItem("userrole");
    if (
      (sessionStorage.getItem('username') == '')) {

      this.router.navigate(['/NewHome']); return;
    }
    if (this.roleid == '1004' || this.roleid == '1003') {
      this.Swatch_Ur = sessionStorage.getItem('userurtype');
      this.roledistid = sessionStorage.getItem('userdistcode');
      this.GetSurveyReportdata();
    }
    else if (this.roleid == '1001' || this.roleid == '1002' || this.roleid == '1006') {
      this.roledistid = '0';
      this.Swatch_Ur = "U";
      this.ischecked = true;
      this.GetSurveyReportdata();
    }

  }

  URchange(URID: any) {
    this.Swatch_Ur = URID;
    if (this.Swatch_Ur != '') {
      this.GetSurveyReportdata();
    }
  }
  distarray: any[] = []; Swatch_DISTRICT: any = '0'; dataRow: any[] = [];
  Swatch_Ur: any = "";
  ischecked: boolean = false;
  ischecked1: boolean = false;
  VillageSurveyReportdata: any[] = []; MandalSurveyReportdata: any[] = [];
  GpSurveyReportdata: any[] = []; Reportlevel: any = 0;
  SurveyReportdata: any[] = []; typeid: any = '';
  async GetSurveyReportdata(): Promise<void> {
    debugger;
    sessionStorage.setItem('dist', '');
    sessionStorage.setItem('distname', '');
    sessionStorage.setItem('mandal', '');
    sessionStorage.setItem('mandalname', '');
    this.Reportlevel = 0;
    const req = {
      type: "137",
      input01: this.Swatch_Ur.toString(),
      input02: this.roledistid.toString(),
    }
    this.SurveyReportdata = [];
    this.details = [];
    let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
    // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
    let responce = await this.apipost.clinetposturlauth(req, urlservice);
    let rsdata = JSON.parse(this.mid.deccall(responce.data));
    let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
    console.log(rsdata.Details);
    this.SurveyReportdata = rsdata1.Details;
    this.sumtotalrows(rsdata.Details, "DISTRICT_NAME");
    this.SurveyReportdata.push(this.details);
  }
  details: any[] = [];
  async sumtotalrows(details: any, column: any): Promise<void> {
    const scores = details;

    const sumScores = (arr: any) => {
      return arr.reduce((acc: any, val: any) => {
        acc[column] = 'Total';
        Object.keys(val).forEach(key => {
          /*   if((key!== 'DISTRICT_NAME1')){ */
          acc[key] += val[key];
          /*  }; */
        });
        /*   if((acc['DISTRICT_NAME'] !== 'all')){ */
        acc[column] = 'Total';
        /* }; */
        return acc;
      });

    };

    debugger;
    let det = sumScores(details);
    if (details.length == 1) {
      det[column] = 'Total';
    }
    for (var k in det) {
      if (k != "0") {
        delete scores[k];
      }

    }
    this.details = det;
  }
  District: any = ''; Mandal: any = ''; Gp: any = '';
  async GetMandalSurveyReportdata(Distrctcode: any, Distname: any): Promise<void> {
    this.District = Distname;
    sessionStorage.setItem('dist', Distrctcode);
    sessionStorage.setItem('distname', Distname);
    this.Reportlevel++;
    let rolemandalcode: any = '';
    if (this.roleid == '1004' || this.roleid == '1003') {
      if (this.Swatch_Ur.toString() == "U") {
        rolemandalcode = sessionStorage.getItem('userulbcode');
      }
      else if (this.Swatch_Ur.toString() == "R") {
        if (this.roleid == '1003') {
          rolemandalcode = "0";
        }
        else {
          rolemandalcode = sessionStorage.getItem('usermandalcode');
        }

      }

    }
    else {
      rolemandalcode = '0';
    }
    debugger;
    const req = {
      type: this.Swatch_Ur.toString() == "U" ? "141" : "138",
      input01: Distrctcode.toString(),
      input02: this.Swatch_Ur.toString(),
      input03: rolemandalcode.toString(),
    }
    this.MandalSurveyReportdata = [];
    this.details = [];
    let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
    // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
    let responce = await this.apipost.clinetposturlauth(req, urlservice);
    let rsdata = JSON.parse(this.mid.deccall(responce.data));
    let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
    if (rsdata.code) {
      this.MandalSurveyReportdata = rsdata.Details;
      this.sumtotalrows(rsdata1.Details, "MANDAL_NAME");
      this.MandalSurveyReportdata.push(this.details);
    }
    else {
      this.alt.toasterror('No data found')
    }

  }

  async GetGpSurveyReportdata(mandalname: any, Mandalcode: any): Promise<void> {
    this.District = sessionStorage.getItem('distname');
    this.Mandal = mandalname;
    sessionStorage.setItem('mandal', Mandalcode);
    sessionStorage.setItem('mandalname', mandalname);
    this.Reportlevel++;
    let rolegpcode: any = '';
    if (this.roleid == '1004' || this.roleid == '1003') {
      if (this.Swatch_Ur.toString() == "U") {
        rolegpcode = '0';
      }
      else if (this.Swatch_Ur.toString() == "R") {

        rolegpcode = "0";

      }

    }
    else {
      rolegpcode = '0';
    }
    debugger;
    const req = {
      type: this.Swatch_Ur.toString() == "U" ? "142" : "139",
      input01: sessionStorage.getItem('dist'),
      input02: Mandalcode.toString(),
      input03: this.Swatch_Ur.toString(),
      input04: rolegpcode.toString(),
    }
    this.GpSurveyReportdata = [];
    this.details = [];
    let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
    // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
    let responce = await this.apipost.clinetposturlauth(req, urlservice);
    let rsdata = JSON.parse(this.mid.deccall(responce.data));
    let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
    console.log(rsdata.Details);
    this.GpSurveyReportdata = rsdata.Details;
    this.sumtotalrows(rsdata1.Details, "WARD_OR_VILLAGE_NAME");
    this.GpSurveyReportdata.push(this.details);
  }

  async GetVillageSurveyReportdata(Gpname: any, gpcode: any): Promise<void> {
    this.Gp = Gpname;
    this.Reportlevel++;
    debugger;
    const req = {
      type: "140",
      input01: sessionStorage.getItem('dist'),
      input02: sessionStorage.getItem('mandal'),
      input03: gpcode.toString(),
      input04: this.Swatch_Ur.toString(),
    }
    this.VillageSurveyReportdata = [];
    this.details = [];
    let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
    // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
    let responce = await this.apipost.clinetposturlauth(req, urlservice);
    let rsdata = JSON.parse(this.mid.deccall(responce.data));
    let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
    console.log(rsdata.Details);
    this.VillageSurveyReportdata = rsdata.Details;
    this.sumtotalrows(rsdata1.Details, "WARD_OR_VILLAGE_NAME");
    this.VillageSurveyReportdata.push(this.details);
  }

  Back() {
    this.Reportlevel--;
  }

  Data1: any[] = []; Ur: any = ''
  mandalname = '';
  villagename = '';
  exportSlipXl() {

    if (this.Swatch_Ur == 'U') {
      this.Ur = 'Urban';
      this.mandalname = 'Urban Local Body';
      this.villagename = 'Ward';
    }
    else if (this.Swatch_Ur == 'R') {
      this.Ur = 'Rural';
      this.mandalname = 'Mandal';
      this.villagename = 'Gram panchayat';
    }

    if (this.Reportlevel == '0') {
      this.Data1 = this.SurveyReportdata;
    }
    if (this.Reportlevel == '1') {
      this.Data1 = this.MandalSurveyReportdata;
    }
    if (this.Reportlevel == '2') {
      this.Data1 = this.GpSurveyReportdata;
    }
    if (this.Reportlevel == '3') {
      this.Data1 = this.VillageSurveyReportdata;
    }
    const Data = this.Data1;
    if (this.SurveyReportdata.length > 0) {
      const checkdata = Math.max(Data.length);
      if (checkdata > 0) {
        debugger;
        //firstreport

        var body = [];
        var dataRow: any[] = [];
        //Headers; 
        body.push(['', '', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '','', '', 'SAFAIMITRA SURVEY - FEMALE HEADED REPORTS', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        );
        
        body.push(dataRow);
        if (this.Reportlevel == '1') {
          body.push(['', '', 'District :', this.District, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
          );
        }
        else if (this.Reportlevel == '2') {
          body.push(['', '', 'District :', this.District, '', '', 'Mandal :', this.Mandal, '', '', '', '', '', '', '', '', '', ''],
          );
        }
        else if (this.Reportlevel == '3') {
          body.push(['', '', 'District :', this.District, '', '', 'Mandal :', this.Mandal, '', '', 'Grampanchayat/Ward :', this.Gp, '', '', '', '', '', ''],
          );
        }

        body.push(['', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Caste', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Family Members', '', '', '', '', ''],
        );
        body.push(['', '', '', '', '', '', '', 'OC', '', '', '', 'BC', '', '', '', 'SC', '', '', '', 'ST', '', '', '', '', '', '', '', '', 'Educational Details', '', '', '', '', '', '', '', '', 'Type of Employment', '', '', '', '', '', '', '', '', 'Monthly Income', '', '', '', '', '', '', 'Insurance	', '', '', '', ' ', '', 'Female', '', '', '', 'House Type', ''],
        );

        if (this.Reportlevel == '0') {
          body.push(['S.no', this.District,
            'Total Female Surveyed',
            'Divorced',
            'Widow',
            'Single Woman',
            'Divorced',
            'Widow',
            'Single Woman',
            'Total',
            'Divorced',
            'Widow',
            'Single Woman',
            'Total',
            'Divorced',
            'Widow',
            'Single Woman',
            'Total',
            'Divorced',
            'Widow',
            'Single Woman',
            'Total',
            'Illiterate',
            'Functional Literacy',
            'Till 5th std.',
            'Till 8th std.',
            'Till 10th std.',
            'Till 12th std.',
            'Diploma-Vocational',
            'Diploma-Professional',
            'Graduation',
            'Post Graduation',
            'PhD',
            'Total',
            'Contract Worker',
            'Outsourcing Worker',
            'Private Sector',
            'Unauthorized Sector',
            'Government Employee',
            'Reserve Worker',
            'Badili Worker',
            'Clap Mitras',
            'Total',
            'Less than Rs.5000',
            'Rs.5000- Rs.10000',
            'Rs.10001-Rs.15000',
            'Rs.15001-Rs.20000',
            'Rs.20001-Rs.25000',
            'Rs.25001-Rs.30000',
            'Above Rs.30000',
            'Total',
            'Health insurance',
            'Life insurance',
            'Accidental insurance',
            'Employees State Insurance Corporation (ESIC)',
            'Govt Insurance',
            'Any Other',
            'Total',
            'Children',
            'Adults',
            'Senior Citizens',
            'Total',
            'Kutcha House',
            'Semi-Pucca House',
            'Pucca House',
            'Total',],);
          //body
          for (let i = 0; i < Data.length; i++) {
            var dataRow = [];
            dataRow = [Data[i].DISTRICT_NAME !="Total" ? i+1:"", Data[i].DISTRICT_NAME,
            Data[i].TOTAL_FEMALE,
            Data[i].TOTAL_DIVORCED,
            Data[i].ASTOTAL_WIDOW,
            Data[i].TOTAL_SINGLE_WOMEN,
            Data[i].OC_TOTAL_DIVORCED,
            Data[i].OC_TOTAL_WIDOW,
            Data[i].OC_TOTAL_SINGLE_WOMEN,
            Data[i].OC_TOTAL,
            Data[i].BC_TOTAL_DIVORCED,
            Data[i].BC_TOTAL_WIDOW,
            Data[i].BC_TOTAL_SINGLE_WOMEN,
            Data[i].BC_TOTAL,
            Data[i].SC_TOTAL_DIVORCED,
            Data[i].SC_TOTAL_WIDOW,
            Data[i].SC_TOTAL_SINGLE_WOMEN,
            Data[i].SC_TOTAL,
            Data[i].ST_TOTAL_DIVORCED,
            Data[i].ST_TOTAL_WIDOW,
            Data[i].ST_TOTAL_SINGLE_WOMEN,
            Data[i].ST_TOTAL,
            Data[i].EDU_ILLITERATE,
            Data[i].EDU_FUN_LITERACY,
            Data[i].EDU_5TH,
            Data[i].EDU_8TH,
            Data[i].EDU_10TH,
            Data[i].EDU_12TH,
            Data[i].EDU_DIP_VOC,
            Data[i].EDU_DIP_PROFF,
            Data[i].EDU_GRADUATION,
            Data[i].EDU_PG,
            Data[i].EDU_PHD,
            Data[i].EDU_TOTAL,
            Data[i].EMP_CONTRACT,
            Data[i].EMP_OUTSOURCING,
            Data[i].EMP_PRIVATE,
            Data[i].EMP_UNAUTHORIZED,
            Data[i].EMP_GOVT,
            Data[i].EMP_RESERVE,
            Data[i].EMP_BADILI,
            Data[i].EMP_CLAP,
            Data[i].EMP_TOTAL,
            Data[i].INC_LESS_THAN_5000,
            Data[i].INC_5000_10000,
            Data[i].INC_10001_15000,
            Data[i].INC_15001_20000,
            Data[i].INC_20001_25000,
            Data[i].INC_25001_30000,
            Data[i].INC_30000,
            Data[i].INC_TOTAL,
            Data[i].HEALTH_INSURANCE,
            Data[i].LIFE_INSURANCE,
            Data[i].ACCIDENTAL_INSURANCE,
            Data[i].ESIC_INSURANCE,
            Data[i].GOVT_INSURANCE,
            Data[i].ANYOTHER_INSURANCE,
            Data[i].INSURANCE_TOTAL,
            Data[i].pending,
            Data[i].pending,
            Data[i].pending,
            Data[i].pending,
            Data[i].KUTCHA_HOUSE,
            Data[i].SEMI_PUCCA_HOUSE,
            Data[i].PUCCA_HOUSE,
            Data[i].HOUSE_TOTAL,
            ];
            body.push(dataRow);
          }
          //footer
        body.push(['No Of Records:', '', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', Data.length-1],
        );
        }
        else if (this.Reportlevel == '1') {
          if(this.Swatch_Ur="U")
          { body.push(['S.no', this.mandalname,'Urban Local Body (ULB)Code',
          'Total Female Surveyed',
          'Divorced',
          'Widow',
          'Single Woman',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Illiterate',
          'Functional Literacy',
          'Till 5th std.',
          'Till 8th std.',
          'Till 10th std.',
          'Till 12th std.',
          'Diploma-Vocational',
          'Diploma-Professional',
          'Graduation',
          'Post Graduation',
          'PhD',
          'Total',
          'Contract Worker',
          'Outsourcing Worker',
          'Private Sector',
          'Unauthorized Sector',
          'Government Employee',
          'Reserve Worker',
          'Badili Worker',
          'Clap Mitras',
          'Total',
          'Less than Rs.5000',
          'Rs.5000- Rs.10000',
          'Rs.10001-Rs.15000',
          'Rs.15001-Rs.20000',
          'Rs.20001-Rs.25000',
          'Rs.25001-Rs.30000',
          'Above Rs.30000',
          'Total',
          'Health insurance',
          'Life insurance',
          'Accidental insurance',
          'Employees State Insurance Corporation (ESIC)',
          'Govt Insurance',
          'Any Other',
          'Total',
          'Children',
          'Adults',
          'Senior Citizens',
          'Total',
          'Kutcha House',
          'Semi-Pucca House',
          'Pucca House',
          'Total',],);}
          else
          { body.push(['S.no', this.mandalname,
          'Total Female Surveyed',
          'Divorced',
          'Widow',
          'Single Woman',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Illiterate',
          'Functional Literacy',
          'Till 5th std.',
          'Till 8th std.',
          'Till 10th std.',
          'Till 12th std.',
          'Diploma-Vocational',
          'Diploma-Professional',
          'Graduation',
          'Post Graduation',
          'PhD',
          'Total',
          'Contract Worker',
          'Outsourcing Worker',
          'Private Sector',
          'Unauthorized Sector',
          'Government Employee',
          'Reserve Worker',
          'Badili Worker',
          'Clap Mitras',
          'Total',
          'Less than Rs.5000',
          'Rs.5000- Rs.10000',
          'Rs.10001-Rs.15000',
          'Rs.15001-Rs.20000',
          'Rs.20001-Rs.25000',
          'Rs.25001-Rs.30000',
          'Above Rs.30000',
          'Total',
          'Health insurance',
          'Life insurance',
          'Accidental insurance',
          'Employees State Insurance Corporation (ESIC)',
          'Govt Insurance',
          'Any Other',
          'Total',
          'Children',
          'Adults',
          'Senior Citizens',
          'Total',
          'Kutcha House',
          'Semi-Pucca House',
          'Pucca House',
          'Total',],);}
         
          //body
          for (let i = 0; i < Data.length; i++) {
            var dataRow = [];
            if(this.Swatch_Ur="U")
            { dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"",Data[i].MANDAL_NAME,  Data[i].MANDAL_NAME !="Total" ?  Data[i].MANDAL_ID:"",  
            Data[i].TOTAL_FEMALE,
            Data[i].TOTAL_DIVORCED,
            Data[i].ASTOTAL_WIDOW,
            Data[i].TOTAL_SINGLE_WOMEN,
            Data[i].OC_TOTAL_DIVORCED,
            Data[i].OC_TOTAL_WIDOW,
            Data[i].OC_TOTAL_SINGLE_WOMEN,
            Data[i].OC_TOTAL,
            Data[i].BC_TOTAL_DIVORCED,
            Data[i].BC_TOTAL_WIDOW,
            Data[i].BC_TOTAL_SINGLE_WOMEN,
            Data[i].BC_TOTAL,
            Data[i].SC_TOTAL_DIVORCED,
            Data[i].SC_TOTAL_WIDOW,
            Data[i].SC_TOTAL_SINGLE_WOMEN,
            Data[i].SC_TOTAL,
            Data[i].ST_TOTAL_DIVORCED,
            Data[i].ST_TOTAL_WIDOW,
            Data[i].ST_TOTAL_SINGLE_WOMEN,
            Data[i].ST_TOTAL,
            Data[i].EDU_ILLITERATE,
            Data[i].EDU_FUN_LITERACY,
            Data[i].EDU_5TH,
            Data[i].EDU_8TH,
            Data[i].EDU_10TH,
            Data[i].EDU_12TH,
            Data[i].EDU_DIP_VOC,
            Data[i].EDU_DIP_PROFF,
            Data[i].EDU_GRADUATION,
            Data[i].EDU_PG,
            Data[i].EDU_PHD,
            Data[i].EDU_TOTAL,
            Data[i].EMP_CONTRACT,
            Data[i].EMP_OUTSOURCING,
            Data[i].EMP_PRIVATE,
            Data[i].EMP_UNAUTHORIZED,
            Data[i].EMP_GOVT,
            Data[i].EMP_RESERVE,
            Data[i].EMP_BADILI,
            Data[i].EMP_CLAP,
            Data[i].EMP_TOTAL,
            Data[i].INC_LESS_THAN_5000,
            Data[i].INC_5000_10000,
            Data[i].INC_10001_15000,
            Data[i].INC_15001_20000,
            Data[i].INC_20001_25000,
            Data[i].INC_25001_30000,
            Data[i].INC_30000,
            Data[i].INC_TOTAL,
            Data[i].HEALTH_INSURANCE,
            Data[i].LIFE_INSURANCE,
            Data[i].ACCIDENTAL_INSURANCE,
            Data[i].ESIC_INSURANCE,
            Data[i].GOVT_INSURANCE,
            Data[i].ANYOTHER_INSURANCE,
            Data[i].INSURANCE_TOTAL,
            Data[i].pending,
            Data[i].pending,
            Data[i].pending,
            Data[i].pending,
            Data[i].KUTCHA_HOUSE,
            Data[i].SEMI_PUCCA_HOUSE,
            Data[i].PUCCA_HOUSE,
            Data[i].HOUSE_TOTAL,
            ];

             
            }
            else
             { dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"",Data[i].MANDAL_NAME, Data[i].DISTRICT_NAME,
             Data[i].TOTAL_FEMALE,
             Data[i].TOTAL_DIVORCED,
             Data[i].ASTOTAL_WIDOW,
             Data[i].TOTAL_SINGLE_WOMEN,
             Data[i].OC_TOTAL_DIVORCED,
             Data[i].OC_TOTAL_WIDOW,
             Data[i].OC_TOTAL_SINGLE_WOMEN,
             Data[i].OC_TOTAL,
             Data[i].BC_TOTAL_DIVORCED,
             Data[i].BC_TOTAL_WIDOW,
             Data[i].BC_TOTAL_SINGLE_WOMEN,
             Data[i].BC_TOTAL,
             Data[i].SC_TOTAL_DIVORCED,
             Data[i].SC_TOTAL_WIDOW,
             Data[i].SC_TOTAL_SINGLE_WOMEN,
             Data[i].SC_TOTAL,
             Data[i].ST_TOTAL_DIVORCED,
             Data[i].ST_TOTAL_WIDOW,
             Data[i].ST_TOTAL_SINGLE_WOMEN,
             Data[i].ST_TOTAL,
             Data[i].EDU_ILLITERATE,
             Data[i].EDU_FUN_LITERACY,
             Data[i].EDU_5TH,
             Data[i].EDU_8TH,
             Data[i].EDU_10TH,
             Data[i].EDU_12TH,
             Data[i].EDU_DIP_VOC,
             Data[i].EDU_DIP_PROFF,
             Data[i].EDU_GRADUATION,
             Data[i].EDU_PG,
             Data[i].EDU_PHD,
             Data[i].EDU_TOTAL,
             Data[i].EMP_CONTRACT,
             Data[i].EMP_OUTSOURCING,
             Data[i].EMP_PRIVATE,
             Data[i].EMP_UNAUTHORIZED,
             Data[i].EMP_GOVT,
             Data[i].EMP_RESERVE,
             Data[i].EMP_BADILI,
             Data[i].EMP_CLAP,
             Data[i].EMP_TOTAL,
             Data[i].INC_LESS_THAN_5000,
             Data[i].INC_5000_10000,
             Data[i].INC_10001_15000,
             Data[i].INC_15001_20000,
             Data[i].INC_20001_25000,
             Data[i].INC_25001_30000,
             Data[i].INC_30000,
             Data[i].INC_TOTAL,
             Data[i].HEALTH_INSURANCE,
             Data[i].LIFE_INSURANCE,
             Data[i].ACCIDENTAL_INSURANCE,
             Data[i].ESIC_INSURANCE,
             Data[i].GOVT_INSURANCE,
             Data[i].ANYOTHER_INSURANCE,
             Data[i].INSURANCE_TOTAL,
             Data[i].pending,
             Data[i].pending,
             Data[i].pending,
             Data[i].pending,
             Data[i].KUTCHA_HOUSE,
             Data[i].SEMI_PUCCA_HOUSE,
             Data[i].PUCCA_HOUSE,
             Data[i].HOUSE_TOTAL,
             ];

             
            
          }
          body.push(dataRow);
          }

          if(this.Swatch_Ur="U"){
          body.push(['No Of Records:', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', Data.length-1],
          );
        }
        else{body.push(['No Of Records:', '', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '',Data.length-1],
        );}
        }




        else if (this.Reportlevel == '2') {
          if (this.Swatch_Ur = "U") {
            body.push(['S.no', this.villagename,
            'Total Female Surveyed',
            'Divorced',
            'Widow',
            'Single Woman',
            'Divorced',
            'Widow',
            'Single Woman',
            'Total',
            'Divorced',
            'Widow',
            'Single Woman',
            'Total',
            'Divorced',
            'Widow',
            'Single Woman',
            'Total',
            'Divorced',
            'Widow',
            'Single Woman',
            'Total',
            'Illiterate',
            'Functional Literacy',
            'Till 5th std.',
            'Till 8th std.',
            'Till 10th std.',
            'Till 12th std.',
            'Diploma-Vocational',
            'Diploma-Professional',
            'Graduation',
            'Post Graduation',
            'PhD',
            'Total',
            'Contract Worker',
            'Outsourcing Worker',
            'Private Sector',
            'Unauthorized Sector',
            'Government Employee',
            'Reserve Worker',
            'Badili Worker',
            'Clap Mitras',
            'Total',
            'Less than Rs.5000',
            'Rs.5000- Rs.10000',
            'Rs.10001-Rs.15000',
            'Rs.15001-Rs.20000',
            'Rs.20001-Rs.25000',
            'Rs.25001-Rs.30000',
            'Above Rs.30000',
            'Total',
            'Health insurance',
            'Life insurance',
            'Accidental insurance',
            'Employees State Insurance Corporation (ESIC)',
            'Govt Insurance',
            'Any Other',
            'Total',
            'Children',
            'Adults',
            'Senior Citizens',
            'Total',
            'Kutcha House',
            'Semi-Pucca House',
            'Pucca House',
            'Total',],);      
          }
          else {
           body.push(['S.no', this.villagename,'GP(LGD) Code',
          'Total Female Surveyed',
          'Divorced',
          'Widow',
          'Single Woman',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Illiterate',
          'Functional Literacy',
          'Till 5th std.',
          'Till 8th std.',
          'Till 10th std.',
          'Till 12th std.',
          'Diploma-Vocational',
          'Diploma-Professional',
          'Graduation',
          'Post Graduation',
          'PhD',
          'Total',
          'Contract Worker',
          'Outsourcing Worker',
          'Private Sector',
          'Unauthorized Sector',
          'Government Employee',
          'Reserve Worker',
          'Badili Worker',
          'Clap Mitras',
          'Total',
          'Less than Rs.5000',
          'Rs.5000- Rs.10000',
          'Rs.10001-Rs.15000',
          'Rs.15001-Rs.20000',
          'Rs.20001-Rs.25000',
          'Rs.25001-Rs.30000',
          'Above Rs.30000',
          'Total',
          'Health insurance',
          'Life insurance',
          'Accidental insurance',
          'Employees State Insurance Corporation (ESIC)',
          'Govt Insurance',
          'Any Other',
          'Total',
          'Children',
          'Adults',
          'Senior Citizens',
          'Total',
          'Kutcha House',
          'Semi-Pucca House',
          'Pucca House',
          'Total',],);      
        }



          for (let i = 0; i < Data.length; i++) {
            var dataRow = [];
            if (this.Mandal != 'Penamaluru' && this.Swatch_Ur == 'U') {
              dataRow =  [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME,
              Data[i].TOTAL_FEMALE,
             Data[i].TOTAL_DIVORCED,
             Data[i].ASTOTAL_WIDOW,
             Data[i].TOTAL_SINGLE_WOMEN,
             Data[i].OC_TOTAL_DIVORCED,
             Data[i].OC_TOTAL_WIDOW,
             Data[i].OC_TOTAL_SINGLE_WOMEN,
             Data[i].OC_TOTAL,
             Data[i].BC_TOTAL_DIVORCED,
             Data[i].BC_TOTAL_WIDOW,
             Data[i].BC_TOTAL_SINGLE_WOMEN,
             Data[i].BC_TOTAL,
             Data[i].SC_TOTAL_DIVORCED,
             Data[i].SC_TOTAL_WIDOW,
             Data[i].SC_TOTAL_SINGLE_WOMEN,
             Data[i].SC_TOTAL,
             Data[i].ST_TOTAL_DIVORCED,
             Data[i].ST_TOTAL_WIDOW,
             Data[i].ST_TOTAL_SINGLE_WOMEN,
             Data[i].ST_TOTAL,
             Data[i].EDU_ILLITERATE,
             Data[i].EDU_FUN_LITERACY,
             Data[i].EDU_5TH,
             Data[i].EDU_8TH,
             Data[i].EDU_10TH,
             Data[i].EDU_12TH,
             Data[i].EDU_DIP_VOC,
             Data[i].EDU_DIP_PROFF,
             Data[i].EDU_GRADUATION,
             Data[i].EDU_PG,
             Data[i].EDU_PHD,
             Data[i].EDU_TOTAL,
             Data[i].EMP_CONTRACT,
             Data[i].EMP_OUTSOURCING,
             Data[i].EMP_PRIVATE,
             Data[i].EMP_UNAUTHORIZED,
             Data[i].EMP_GOVT,
             Data[i].EMP_RESERVE,
             Data[i].EMP_BADILI,
             Data[i].EMP_CLAP,
             Data[i].EMP_TOTAL,
             Data[i].INC_LESS_THAN_5000,
             Data[i].INC_5000_10000,
             Data[i].INC_10001_15000,
             Data[i].INC_15001_20000,
             Data[i].INC_20001_25000,
             Data[i].INC_25001_30000,
             Data[i].INC_30000,
             Data[i].INC_TOTAL,
             Data[i].HEALTH_INSURANCE,
             Data[i].LIFE_INSURANCE,
             Data[i].ACCIDENTAL_INSURANCE,
             Data[i].ESIC_INSURANCE,
             Data[i].GOVT_INSURANCE,
             Data[i].ANYOTHER_INSURANCE,
             Data[i].INSURANCE_TOTAL,
             Data[i].pending,
             Data[i].pending,
             Data[i].pending,
             Data[i].pending,
             Data[i].KUTCHA_HOUSE,
             Data[i].SEMI_PUCCA_HOUSE,
             Data[i].PUCCA_HOUSE,
             Data[i].HOUSE_TOTAL,
             ];
            }
            else {
              dataRow =  [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME, Data[i].WARD_OR_VILLAGE_NAME !="Total" ?  Data[i].WARD_OR_VILLAGE_ID:"",
              Data[i].TOTAL_FEMALE,
             Data[i].TOTAL_DIVORCED,
             Data[i].ASTOTAL_WIDOW,
             Data[i].TOTAL_SINGLE_WOMEN,
             Data[i].OC_TOTAL_DIVORCED,
             Data[i].OC_TOTAL_WIDOW,
             Data[i].OC_TOTAL_SINGLE_WOMEN,
             Data[i].OC_TOTAL,
             Data[i].BC_TOTAL_DIVORCED,
             Data[i].BC_TOTAL_WIDOW,
             Data[i].BC_TOTAL_SINGLE_WOMEN,
             Data[i].BC_TOTAL,
             Data[i].SC_TOTAL_DIVORCED,
             Data[i].SC_TOTAL_WIDOW,
             Data[i].SC_TOTAL_SINGLE_WOMEN,
             Data[i].SC_TOTAL,
             Data[i].ST_TOTAL_DIVORCED,
             Data[i].ST_TOTAL_WIDOW,
             Data[i].ST_TOTAL_SINGLE_WOMEN,
             Data[i].ST_TOTAL,
             Data[i].EDU_ILLITERATE,
             Data[i].EDU_FUN_LITERACY,
             Data[i].EDU_5TH,
             Data[i].EDU_8TH,
             Data[i].EDU_10TH,
             Data[i].EDU_12TH,
             Data[i].EDU_DIP_VOC,
             Data[i].EDU_DIP_PROFF,
             Data[i].EDU_GRADUATION,
             Data[i].EDU_PG,
             Data[i].EDU_PHD,
             Data[i].EDU_TOTAL,
             Data[i].EMP_CONTRACT,
             Data[i].EMP_OUTSOURCING,
             Data[i].EMP_PRIVATE,
             Data[i].EMP_UNAUTHORIZED,
             Data[i].EMP_GOVT,
             Data[i].EMP_RESERVE,
             Data[i].EMP_BADILI,
             Data[i].EMP_CLAP,
             Data[i].EMP_TOTAL,
             Data[i].INC_LESS_THAN_5000,
             Data[i].INC_5000_10000,
             Data[i].INC_10001_15000,
             Data[i].INC_15001_20000,
             Data[i].INC_20001_25000,
             Data[i].INC_25001_30000,
             Data[i].INC_30000,
             Data[i].INC_TOTAL,
             Data[i].HEALTH_INSURANCE,
             Data[i].LIFE_INSURANCE,
             Data[i].ACCIDENTAL_INSURANCE,
             Data[i].ESIC_INSURANCE,
             Data[i].GOVT_INSURANCE,
             Data[i].ANYOTHER_INSURANCE,
             Data[i].INSURANCE_TOTAL,
             Data[i].pending,
             Data[i].pending,
             Data[i].pending,
             Data[i].pending,
             Data[i].KUTCHA_HOUSE,
             Data[i].SEMI_PUCCA_HOUSE,
             Data[i].PUCCA_HOUSE,
             Data[i].HOUSE_TOTAL,
             ];
            }
            body.push(dataRow);
          }

          if(this.Swatch_Ur="U"){
            body.push(['No Of Records:', '', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', Data.length-1],
            );
          }
          else{body.push(['No Of Records:', '', '','', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '',Data.length-1],
          );}

      }


        else if (this.Reportlevel == '3') {
          body.push(['S.no', this.villagename,'Village Revenue Code',
          'Total Female Surveyed',
          'Divorced',
          'Widow',
          'Single Woman',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Divorced',
          'Widow',
          'Single Woman',
          'Total',
          'Illiterate',
          'Functional Literacy',
          'Till 5th std.',
          'Till 8th std.',
          'Till 10th std.',
          'Till 12th std.',
          'Diploma-Vocational',
          'Diploma-Professional',
          'Graduation',
          'Post Graduation',
          'PhD',
          'Total',
          'Contract Worker',
          'Outsourcing Worker',
          'Private Sector',
          'Unauthorized Sector',
          'Government Employee',
          'Reserve Worker',
          'Badili Worker',
          'Clap Mitras',
          'Total',
          'Less than Rs.5000',
          'Rs.5000- Rs.10000',
          'Rs.10001-Rs.15000',
          'Rs.15001-Rs.20000',
          'Rs.20001-Rs.25000',
          'Rs.25001-Rs.30000',
          'Above Rs.30000',
          'Total',
          'Health insurance',
          'Life insurance',
          'Accidental insurance',
          'Employees State Insurance Corporation (ESIC)',
          'Govt Insurance',
          'Any Other',
          'Total',
          'Children',
          'Adults',
          'Senior Citizens',
          'Total',
          'Kutcha House',
          'Semi-Pucca House',
          'Pucca House',
          'Total',],);     
          //body
          for (let i = 0; i < Data.length; i++) {
            var dataRow = [];
            dataRow =  [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME, Data[i].WARD_OR_VILLAGE_NAME !="Total" ?  Data[i].WARD_OR_VILLAGE_ID:"",
              Data[i].TOTAL_FEMALE,
             Data[i].TOTAL_DIVORCED,
             Data[i].ASTOTAL_WIDOW,
             Data[i].TOTAL_SINGLE_WOMEN,
             Data[i].OC_TOTAL_DIVORCED,
             Data[i].OC_TOTAL_WIDOW,
             Data[i].OC_TOTAL_SINGLE_WOMEN,
             Data[i].OC_TOTAL,
             Data[i].BC_TOTAL_DIVORCED,
             Data[i].BC_TOTAL_WIDOW,
             Data[i].BC_TOTAL_SINGLE_WOMEN,
             Data[i].BC_TOTAL,
             Data[i].SC_TOTAL_DIVORCED,
             Data[i].SC_TOTAL_WIDOW,
             Data[i].SC_TOTAL_SINGLE_WOMEN,
             Data[i].SC_TOTAL,
             Data[i].ST_TOTAL_DIVORCED,
             Data[i].ST_TOTAL_WIDOW,
             Data[i].ST_TOTAL_SINGLE_WOMEN,
             Data[i].ST_TOTAL,
             Data[i].EDU_ILLITERATE,
             Data[i].EDU_FUN_LITERACY,
             Data[i].EDU_5TH,
             Data[i].EDU_8TH,
             Data[i].EDU_10TH,
             Data[i].EDU_12TH,
             Data[i].EDU_DIP_VOC,
             Data[i].EDU_DIP_PROFF,
             Data[i].EDU_GRADUATION,
             Data[i].EDU_PG,
             Data[i].EDU_PHD,
             Data[i].EDU_TOTAL,
             Data[i].EMP_CONTRACT,
             Data[i].EMP_OUTSOURCING,
             Data[i].EMP_PRIVATE,
             Data[i].EMP_UNAUTHORIZED,
             Data[i].EMP_GOVT,
             Data[i].EMP_RESERVE,
             Data[i].EMP_BADILI,
             Data[i].EMP_CLAP,
             Data[i].EMP_TOTAL,
             Data[i].INC_LESS_THAN_5000,
             Data[i].INC_5000_10000,
             Data[i].INC_10001_15000,
             Data[i].INC_15001_20000,
             Data[i].INC_20001_25000,
             Data[i].INC_25001_30000,
             Data[i].INC_30000,
             Data[i].INC_TOTAL,
             Data[i].HEALTH_INSURANCE,
             Data[i].LIFE_INSURANCE,
             Data[i].ACCIDENTAL_INSURANCE,
             Data[i].ESIC_INSURANCE,
             Data[i].GOVT_INSURANCE,
             Data[i].ANYOTHER_INSURANCE,
             Data[i].INSURANCE_TOTAL,
             Data[i].pending,
             Data[i].pending,
             Data[i].pending,
             Data[i].pending,
             Data[i].KUTCHA_HOUSE,
             Data[i].SEMI_PUCCA_HOUSE,
             Data[i].PUCCA_HOUSE,
             Data[i].HOUSE_TOTAL,
             ];
            body.push(dataRow);
          }
          body.push(['No Of Records:', '', '','', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '', '', '', '', '', '', '','', '', '', '',Data.length-1],
          );
        }
        

        if (this.Reportlevel == '0') {
          this.mid.JSONToCSVConvertor(
            body,
            'District Level' + '_' + this.Ur + '_' + 'Female Headed Report Data',
            true
          );
        }
        if (this.Reportlevel == '1') {
          this.mid.JSONToCSVConvertor(
            body,
            this.District + '_' + this.Ur + '_' + 'Female Headed Report Data',
            true
          );
        }
        if (this.Reportlevel == '2') {
          this.mid.JSONToCSVConvertor(
            body,
            this.Mandal + '_' + this.Ur + '_' + 'Female Headed Report Data',
            true
          );
        }
        if (this.Reportlevel == '3') {
          this.mid.JSONToCSVConvertor(
            body,
            this.Gp + '_' + this.Ur + '_' + 'Female Headed Report Data',
            true
          );
        }

      }
    }
  }

}
