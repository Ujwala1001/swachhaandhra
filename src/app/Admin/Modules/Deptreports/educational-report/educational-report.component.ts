import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-educational-report',
  templateUrl: './educational-report.component.html',
  styleUrls: ['./educational-report.component.css']
})
export class EducationalReportComponent {
 

    constructor(
  
      private router: Router,
      private alt: SweetalertService,
      private mid: MidlayerService,
      private apipost: ClientcallService,
      private spinner: NgxSpinnerService,
  
  
    ) {
    
    }
    roleid:any='';roledistid:any='';
    ngOnInit(): void {
      this.roleid=sessionStorage.getItem("userrole");
      debugger;
      if (
      (sessionStorage.getItem('username') == '')) {
  
       this.router.navigate(['/NewHome']); return;
      }
     
      if (this.roleid=='1004' || this.roleid=='1003')
      { 
        this.Swatch_Ur=sessionStorage.getItem('userurtype');
        this.roledistid= sessionStorage.getItem('userdistcode');
        this.GetSurveyReportdata();
      }
      else   if (this.roleid=='1001' || this.roleid=='1002'|| this.roleid=='1006')
      {
        this.roledistid='0';
        this.Swatch_Ur="U";
        this.ischecked=true;
        this.GetSurveyReportdata();
      }
     
    }
  
    URchange(URID:any)
    {
     this.Swatch_Ur=URID;
     if(this.Swatch_Ur!='')
     {
     this.GetSurveyReportdata();
     }
    }
    distarray: any[] = [];Swatch_DISTRICT:any='0';  dataRow:any []=[];
    Swatch_Ur:any="";
    ischecked:boolean=false;
    ischecked1:boolean=false;
    VillageSurveyReportdata: any[] = [];MandalSurveyReportdata: any[] = [];
    GpSurveyReportdata: any[] = [];  Reportlevel:any=0;
    SurveyReportdata: any[] = [];typeid:any='';
    async  GetSurveyReportdata(): Promise<void> 
    {
      sessionStorage.setItem('dist', '');
      sessionStorage.setItem('distname', '');
      sessionStorage.setItem('mandal', '');
      sessionStorage.setItem('mandalname', '');
      this.Reportlevel=0;
      debugger;
        const req = {
          type: "119",
          input01:this.Swatch_Ur.toString(),
          input02:this.roledistid.toString(),
        }
        this.SurveyReportdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
       
      this.SurveyReportdata=rsdata.Details; 
      this.sumtotalrows(rsdata1.Details,"DISTRICT_NAME");
      this.SurveyReportdata.push(this.details);
      }
      details:any[]=[];
      async  sumtotalrows(details:any,column:any): Promise<void> 
      {
        const scores =details; 
     
        const sumScores = (arr:any) => {
          return arr.reduce((acc:any, val:any) => {
            acc[column] = 'Total';
             Object.keys(val).forEach(key => {
              /*   if((key!== 'DISTRICT_NAME1')){ */
                   acc[key] += val[key];
               /*  }; */
             });
           /*   if((acc['DISTRICT_NAME'] !== 'all')){ */
                acc[column] = 'Total';
             /* }; */
             return acc;
          });
         
       };
       
       debugger;
       let det=sumScores(details);
       if(details.length ==1)
       {
        det[column]='Total';
       }
      for(var k in det) {
        if(k != "0")
        {
          delete scores[k];
        }
        
     } 
     this.details=det;
      }

      District:any='';Mandal:any='';Gp:any='';
      async  GetMandalSurveyReportdata(Distrctcode:any,Distname:any): Promise<void> 
    {
      this.District=Distname;
      sessionStorage.setItem('dist', Distrctcode);
      sessionStorage.setItem('distname', Distname);
      this.Reportlevel++;
      let rolemandalcode:any='';
      if (this.roleid=='1004' || this.roleid=='1003')
    {
      if(this.Swatch_Ur.toString() =="U")
      {
        rolemandalcode=sessionStorage.getItem('userulbcode');
      }
      else  if(this.Swatch_Ur.toString() =="R")
      {
        if(this.roleid=='1003')
      {
        rolemandalcode="0";
      }
      else{
        rolemandalcode=sessionStorage.getItem('usermandalcode');
      }
       
      }
     
    }
    else{
      rolemandalcode='0';
    }
      debugger;
        const req = {
          type: this.Swatch_Ur.toString() =="U" ?"123":"120",
          input01:Distrctcode.toString(),
          input02:this.Swatch_Ur.toString(),
          input03:rolemandalcode.toString(),
        }
        this.MandalSurveyReportdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
        if(rsdata.code)
        {
          this.MandalSurveyReportdata=rsdata.Details; 
          this.sumtotalrows(rsdata1.Details,"MANDAL_NAME");
          this.MandalSurveyReportdata.push(this.details);
        }
        else{
          this.alt.toasterror('No data found')
        }
       
      }
  
      async  GetGpSurveyReportdata(mandalname:any,Mandalcode:any): Promise<void> 
    {
      this.District=sessionStorage.getItem('distname');
      this.Mandal=mandalname;
      sessionStorage.setItem('mandal', Mandalcode);
      sessionStorage.setItem('mandalname', mandalname);
      this.Reportlevel++;
      let rolegpcode:any='';
      if (this.roleid=='1004' || this.roleid=='1003')
      {
        if(this.Swatch_Ur.toString() =="U")
        {
          rolegpcode='0';
        }
        else  if(this.Swatch_Ur.toString() =="R")
        {
         
          rolegpcode="0";
          
        }
       
      }
      else{
        rolegpcode='0';
      }
      debugger;
      const req = {
        type: this.Swatch_Ur.toString() =="U" ?"124":"121",
        input01:sessionStorage.getItem('dist'),
        input02:Mandalcode.toString(),
        input03:this.Swatch_Ur.toString(),
        input04:rolegpcode.toString(),
      }
        this.GpSurveyReportdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
        this.GpSurveyReportdata=rsdata.Details; 
        this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
        this.GpSurveyReportdata.push(this.details);
      }
  
      async  GetVillageSurveyReportdata(Gpname:any,gpcode:any): Promise<void> 
    {
      this.Gp=Gpname;
      this.Reportlevel++;
      debugger;
        const req = {
          type: "122",
          input01:sessionStorage.getItem('dist'),
          input02:sessionStorage.getItem('mandal'),
          input03:gpcode.toString(),
          input04:this.Swatch_Ur.toString(),
        }
        this.VillageSurveyReportdata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
        let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
        this.VillageSurveyReportdata=rsdata.Details; 
        this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
        this.VillageSurveyReportdata.push(this.details);
      }
    
      Back()
      {
        this.Reportlevel--;
      }
      Data1:any[]=[]; Ur:any=''
      mandalname='';
      villagename='';






    addModaldisplay:any='none';
    
    modalclose()
    {
      this.addModaldisplay = 'none';
      this.modaldata=[];
      this.educationdata=[];
      this.SANITATION_WORK_NAME='';
this.SURVEY_ID='';
          this.SANITATION_WORK_EDUCATIONAL_STATUS='';
    }


    
    
modaldata: any[] = [];
    async  GetModaldata(villagecode:any , urbancode:any): Promise<void> 
    {
     this.view=false;
      debugger;
        const req = {
          type: urbancode.toString() =="U" ?"100":"105",
        
          Input01: villagecode.toString()
          
         
        }
        this.modaldata=[];   
        let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
        // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
        let responce = await this.apipost.clinetposturlauth(req, urlservice);
        let rsdata = JSON.parse(this.mid.deccall(responce.data));
       
        if(rsdata.code)
        {
          this.modaldata=rsdata.Details; 
          this.addModaldisplay='block';
        }
     
        else{
        
          this.addModaldisplay='none';
          this.alt.toasterror('No data found')
        }
        
        // this.modaldata=rsdata.Details; 
        // this.addModaldisplay='block';
    
      // if(this.modaldata.length > 0){
      //   this.addModaldisplay='block';
      // }
      // else{
      //   this.addModaldisplay='hide';
      // }

       
      }

      // hasTableData(): boolean {
      //   return this.modaldata.length > 0;
      // }

      view:boolean=false;
      educationdata: any[] = [];SANITATION_WORK_NAME:any='';SANITATION_WORK_EDUCATIONAL_STATUS:any='';SURVEY_ID:any='';
      async  Vieweducation(surveyid:any,surveyid1:any ): Promise<void> 
      {
       
       // this.addModaldisplay='block';
        debugger;
   
         this.view=false;
      
      
          const req = {
            
            type: this.Swatch_Ur.toString() =="U" ?"1244":"1222",
            Input06: surveyid.toString()
  
           
          }
          this.SANITATION_WORK_NAME='';
this.SURVEY_ID='';
          this.SANITATION_WORK_EDUCATIONAL_STATUS='';
          this.educationdata=[];   
          let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
          // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
          let responce = await this.apipost.clinetposturlauth(req, urlservice);
          let rsdata = JSON.parse(this.mid.deccall(responce.data));
          //console.log(rsdata.Details);
          this.SURVEY_ID=rsdata.Details[0].SURVEY_ID;;
          this.SANITATION_WORK_NAME=rsdata.Details[0].SANITATION_WORK_NAME;this.SANITATION_WORK_EDUCATIONAL_STATUS=rsdata.Details[0].SANITATION_WORK_EDUCATIONAL_STATUS;
          this.educationdata=rsdata.Details; 
          if(rsdata.code)
          {
            this.modaldata=rsdata.Details; 
            this.addModaldisplay='block';
            this.view=true;
          }
          else{

           this.addModaldisplay='none';
            this.alt.toasterror('No data found')
          }
          
          
          
        }
  
        exportSlipXl() {
   
          if (this.Swatch_Ur == 'U') {
            this.Ur = 'Urban';
            this.mandalname='Urban Local Body';
            this.villagename='Ward';
          }
          else if (this.Swatch_Ur == 'R') {
            this.Ur = 'Rural';
            this.mandalname='Mandal';
            this.villagename='Gram panchayat';
          }
           
            if(this.Reportlevel =='0')
            {
              this.Data1= this.SurveyReportdata;
            }
            if(this.Reportlevel =='1')
            {
              this.Data1= this.MandalSurveyReportdata;
            }
            if(this.Reportlevel =='2')
            {
              this.Data1= this.GpSurveyReportdata;
            }
            if(this.Reportlevel =='3')
            {
              this.Data1= this.VillageSurveyReportdata;
            }
            const Data=this.Data1;
        if(this.SurveyReportdata.length > 0)
        {
            const checkdata = Math.max(Data.length);
            if(checkdata>0){
              debugger;
              //firstreport
        
              var body = [];
              var dataRow :any []=[];
              //Headers; 
              body.push(['','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','SAFAIMITRA SURVEY - EDUCATIONAL DATA REPORTS','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',],
              );
              body.push(dataRow);
              if(this.Reportlevel =='1')
              {
                body.push(['','','District :',this.District,'','','','','','','','','','','',,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''],
                );
              }
              else if(this.Reportlevel =='2')
              {
                body.push(['','','District :',this.District,'','','Mandal :',this.Mandal,'','','','','','',,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''],
                );
              }
              else if(this.Reportlevel =='3')
              {
                body.push(['','','District :',this.District,'','','Mandal :',this.Mandal,'','','Grampanchayat/Ward :',this.Gp,'','','',,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''],
                );
              }
              body.push(['','','','','','','','',,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','Core Sanitation Workers Educational Status','','','','','','','','Core Sanitation Workers Family Educational Status','','','','','','','',,'','','','','','','','','','','','','','','','','','','','','','','','','','','',,'','',''],
              );
            
              body.push(['','','','', '','Illiterate','','','','Functional Literacy','','','','Till 5th std.','','','','Till 8th std.','','','','Till 10th std.','','','','Till 12th std.','','','','Diploma-Vocational','','','','Diploma-Professional','','','','Graduation','','','','Post Graduation','','','','PhD','','',
              '','Illiterate','','','','Functional Literacy','','','','Till 5th std.','','','','Till 8th std.','','','','Till 10th std.','','','','Till 12th std.','','','','Diploma-Vocational','','','','Diploma-Professional','','','','Graduation','','','','Post Graduation','','','','PhD','',''],
             );
              if(this.Reportlevel =='0')
              {
              body.push(['S.no','District',
              'Total Suveyed',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              'Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              'Total',
              'Male',
              'Female',
              'Transgender',
              'Total',
              'Male',
              'Female',
              'Transgender',
              'Total',
              'Male',
              'Female',
              'Transgender',
              'Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              'Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',
              'Male',
              'Female',
              'Transgender',
              ' Total',],);
              //body
              for (let i = 0; i < Data.length; i++) {
                var dataRow = [];
                dataRow = [Data[i].DISTRICT_NAME !="Total" ? i+1:"",Data[i].DISTRICT_NAME,
                Data[i].TOTAL_SURVEYED,
                Data[i].MALE_NOT_LITERATE,
                Data[i].FEMALE_NOT_LITERATE,
                Data[i].TRANSGENDER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_NOT_LITERATE,
                Data[i].MALE_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FUNCTIONAL_LITERACY,
                Data[i].MALE_UPTO_5TH_STD,
                Data[i].FEMALE_UPTO_5TH_STD,
                Data[i].TRANSGENDER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_UPTO_5TH_STD,
                Data[i].MALE_UPTO_8TH_STD,
                Data[i].FEMALE_UPTO_8TH_STD,
                Data[i].TRANSGENDER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_UPTO_8TH_STD,
                Data[i].MALE_UPTO_10TH_STD,
                Data[i].FEMALE_UPTO_10TH_STD,
                Data[i].TRANSGENDER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_UPTO_10TH_STD,
                Data[i].MALE_UPTO_12TH_STD,
                Data[i].FEMALE_UPTO_12TH_STD,
                Data[i].TRANSGENDER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_UPTO_12TH_STD,
                Data[i].MALE_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_DIPLOMA_PROFESSIONAL ,
                Data[i].TOTAL_GENDER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_GRADUATION,
                Data[i].FEMALE_GRADUATION,
                Data[i].TRANSGENDER_GRADUATION,
                Data[i].TOTAL_GENDER_GRADUATION,
                Data[i].MALE_POST_GRADUATION,
                Data[i].FEMALE_POST_GRADUATION,
                Data[i].TRANSGENDER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_POST_GRADUATION,
                Data[i].MALE_PHD,
                Data[i].FEMALE_PHD,
                Data[i].TOTAL_GENDER_PHD,
                Data[i].TRANSGENDER_PHD,
                Data[i].MALE_FAMILY_MEMBER_NOT_LITERATE ,
                Data[i].FEMALE_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TRANSGENDER_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_FAMILY_NOT_LITERATE,
                Data[i].MALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].MALE_FAMILY_MEMBER_UPTO_5TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_5TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_8TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_8TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_10TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_12TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_FAMILY_MEMBER_GRADUATION,
                Data[i].FEMALE_FAMILY_MEMBER_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_POST_GRADUATION ,
                Data[i].FEMALE_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_POST_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_PHD ,
                Data[i].FEMALE_FAMILY_MEMBER_PHD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_PHD,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_PHD,
              
              ];
                body.push(dataRow);
              }
              body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])

            }


            else  if(this.Reportlevel =='1')
            {
              if(this.Swatch_Ur=='U')
              {
                body.push(['S.no',this.mandalname,
                'Urban Local Body(ULB) Code' ,
                'Total Surveyed',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',],);
              }

              else
              {
                body.push(['S.no',this.mandalname,
                
                'Total Surveyed',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',],);
              }
           
            //body
            for (let i = 0; i < Data.length; i++) {
              var dataRow = [];
              if(this.Swatch_Ur=='U'){ dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"",   Data[i].MANDAL_NAME, Data[i].MANDAL_NAME !="Total" ?  Data[i].MANDAL_ID:"",
              Data[i].TOTAL_SURVEYED,
              Data[i].MALE_NOT_LITERATE,
                Data[i].FEMALE_NOT_LITERATE,
                Data[i].TRANSGENDER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_NOT_LITERATE,
                Data[i].MALE_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FUNCTIONAL_LITERACY,
                Data[i].MALE_UPTO_5TH_STD,
                Data[i].FEMALE_UPTO_5TH_STD,
                Data[i].TRANSGENDER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_UPTO_5TH_STD,
                Data[i].MALE_UPTO_8TH_STD,
                Data[i].FEMALE_UPTO_8TH_STD,
                Data[i].TRANSGENDER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_UPTO_8TH_STD,
                Data[i].MALE_UPTO_10TH_STD,
                Data[i].FEMALE_UPTO_10TH_STD,
                Data[i].TRANSGENDER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_UPTO_10TH_STD,
                Data[i].MALE_UPTO_12TH_STD,
                Data[i].FEMALE_UPTO_12TH_STD,
                Data[i].TRANSGENDER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_UPTO_12TH_STD,
                Data[i].MALE_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_DIPLOMA_PROFESSIONAL ,
                Data[i].TOTAL_GENDER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_GRADUATION,
                Data[i].FEMALE_GRADUATION,
                Data[i].TRANSGENDER_GRADUATION,
                Data[i].TOTAL_GENDER_GRADUATION,
                Data[i].MALE_POST_GRADUATION,
                Data[i].FEMALE_POST_GRADUATION,
                Data[i].TRANSGENDER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_POST_GRADUATION,
                Data[i].MALE_PHD,
                Data[i].FEMALE_PHD,
                Data[i].TOTAL_GENDER_PHD,
                Data[i].TRANSGENDER_PHD,
                Data[i].MALE_FAMILY_MEMBER_NOT_LITERATE ,
                Data[i].FEMALE_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TRANSGENDER_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_FAMILY_NOT_LITERATE,
                Data[i].MALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].MALE_FAMILY_MEMBER_UPTO_5TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_5TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_8TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_8TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_10TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_12TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_FAMILY_MEMBER_GRADUATION,
                Data[i].FEMALE_FAMILY_MEMBER_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_POST_GRADUATION ,
                Data[i].FEMALE_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_POST_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_PHD ,
                Data[i].FEMALE_FAMILY_MEMBER_PHD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_PHD,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_PHD,
            
            ];}
              else{ dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"", Data[i].MANDAL_NAME,
              Data[i].TOTAL_SURVEYED,
              Data[i].MALE_NOT_LITERATE,
                Data[i].FEMALE_NOT_LITERATE,
                Data[i].TRANSGENDER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_NOT_LITERATE,
                Data[i].MALE_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FUNCTIONAL_LITERACY,
                Data[i].MALE_UPTO_5TH_STD,
                Data[i].FEMALE_UPTO_5TH_STD,
                Data[i].TRANSGENDER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_UPTO_5TH_STD,
                Data[i].MALE_UPTO_8TH_STD,
                Data[i].FEMALE_UPTO_8TH_STD,
                Data[i].TRANSGENDER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_UPTO_8TH_STD,
                Data[i].MALE_UPTO_10TH_STD,
                Data[i].FEMALE_UPTO_10TH_STD,
                Data[i].TRANSGENDER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_UPTO_10TH_STD,
                Data[i].MALE_UPTO_12TH_STD,
                Data[i].FEMALE_UPTO_12TH_STD,
                Data[i].TRANSGENDER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_UPTO_12TH_STD,
                Data[i].MALE_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_DIPLOMA_PROFESSIONAL ,
                Data[i].TOTAL_GENDER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_GRADUATION,
                Data[i].FEMALE_GRADUATION,
                Data[i].TRANSGENDER_GRADUATION,
                Data[i].TOTAL_GENDER_GRADUATION,
                Data[i].MALE_POST_GRADUATION,
                Data[i].FEMALE_POST_GRADUATION,
                Data[i].TRANSGENDER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_POST_GRADUATION,
                Data[i].MALE_PHD,
                Data[i].FEMALE_PHD,
                Data[i].TOTAL_GENDER_PHD,
                Data[i].TRANSGENDER_PHD,
                Data[i].MALE_FAMILY_MEMBER_NOT_LITERATE ,
                Data[i].FEMALE_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TRANSGENDER_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_FAMILY_NOT_LITERATE,
                Data[i].MALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].MALE_FAMILY_MEMBER_UPTO_5TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_5TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_8TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_8TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_10TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_12TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_FAMILY_MEMBER_GRADUATION,
                Data[i].FEMALE_FAMILY_MEMBER_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_POST_GRADUATION ,
                Data[i].FEMALE_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_POST_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_PHD ,
                Data[i].FEMALE_FAMILY_MEMBER_PHD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_PHD,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_PHD,
            
            ];}
             
              body.push(dataRow);
            }
            if(this.Swatch_Ur=='U')
            {
              body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])
            }
            else
            {
              body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])
            }
          }

          else  if(this.Reportlevel =='2')
            {
              if(this.Swatch_Ur=='U')
              {
                body.push(['S.no',this.villagename,
                'Total Surveyed',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',],);
               
              }
              else
              {                          
                body.push(['S.no',this.villagename,
                'GP(LGD)Code',
                'Total Surveyed',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                'Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',
                'Male',
                'Female',
                'Transgender',
                ' Total',],);
              }
          
            //body
            for (let i = 0; i < Data.length; i++) {
              var dataRow = [];
              if(this.Mandal !='Penamaluru' && this.Swatch_Ur =='U')
              {
                dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"", Data[i].WARD_OR_VILLAGE_NAME, 
                Data[i].TOTAL_SURVEYED,
                Data[i].MALE_NOT_LITERATE,
                  Data[i].FEMALE_NOT_LITERATE,
                  Data[i].TRANSGENDER_NOT_LITERATE,
                  Data[i].TOTAL_GENDER_NOT_LITERATE,
                  Data[i].MALE_FUNCTIONAL_LITERACY,
                  Data[i].FEMALE_FUNCTIONAL_LITERACY,
                  Data[i].TOTAL_GENDER_FUNCTIONAL_LITERACY,
                  Data[i].TRANSGENDER_FUNCTIONAL_LITERACY,
                  Data[i].MALE_UPTO_5TH_STD,
                  Data[i].FEMALE_UPTO_5TH_STD,
                  Data[i].TRANSGENDER_UPTO_5TH_STD,
                  Data[i].TOTAL_GENDER_UPTO_5TH_STD,
                  Data[i].MALE_UPTO_8TH_STD,
                  Data[i].FEMALE_UPTO_8TH_STD,
                  Data[i].TRANSGENDER_UPTO_8TH_STD,
                  Data[i].TOTAL_GENDER_UPTO_8TH_STD,
                  Data[i].MALE_UPTO_10TH_STD,
                  Data[i].FEMALE_UPTO_10TH_STD,
                  Data[i].TRANSGENDER_UPTO_10TH_STD,
                  Data[i].TOTAL_GENDER_UPTO_10TH_STD,
                  Data[i].MALE_UPTO_12TH_STD,
                  Data[i].FEMALE_UPTO_12TH_STD,
                  Data[i].TRANSGENDER_UPTO_12TH_STD,
                  Data[i].TOTAL_GENDER_UPTO_12TH_STD,
                  Data[i].MALE_DIPLOMA_VOCATIONAL,
                  Data[i].FEMALE_DIPLOMA_VOCATIONAL,
                  Data[i].TRANSGENDER_DIPLOMA_VOCATIONAL,
                  Data[i].TOTAL_GENDER_DIPLOMA_VOCATIONAL,
                  Data[i].MALE_DIPLOMA_PROFESSIONAL,
                  Data[i].FEMALE_DIPLOMA_PROFESSIONAL,
                  Data[i].TRANSGENDER_DIPLOMA_PROFESSIONAL ,
                  Data[i].TOTAL_GENDER_DIPLOMA_PROFESSIONAL,
                  Data[i].MALE_GRADUATION,
                  Data[i].FEMALE_GRADUATION,
                  Data[i].TRANSGENDER_GRADUATION,
                  Data[i].TOTAL_GENDER_GRADUATION,
                  Data[i].MALE_POST_GRADUATION,
                  Data[i].FEMALE_POST_GRADUATION,
                  Data[i].TRANSGENDER_POST_GRADUATION,
                  Data[i].TOTAL_GENDER_POST_GRADUATION,
                  Data[i].MALE_PHD,
                  Data[i].FEMALE_PHD,
                  Data[i].TOTAL_GENDER_PHD,
                  Data[i].TRANSGENDER_PHD,
                  Data[i].MALE_FAMILY_MEMBER_NOT_LITERATE ,
                  Data[i].FEMALE_FAMILY_MEMBER_NOT_LITERATE,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_NOT_LITERATE,
                  Data[i].TOTAL_GENDER_FAMILY_NOT_LITERATE,
                  Data[i].MALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                  Data[i].FEMALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                  Data[i].TOTAL_GENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                  Data[i].MALE_FAMILY_MEMBER_UPTO_5TH_STD ,
                  Data[i].FEMALE_FAMILY_MEMBER_UPTO_5TH_STD,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_5TH_STD,
                  Data[i].TOTAL_GENDER_FAMILY_UPTO_5TH_STD,
                  Data[i].MALE_FAMILY_MEMBER_UPTO_8TH_STD ,
                  Data[i].FEMALE_FAMILY_MEMBER_UPTO_8TH_STD,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_8TH_STD,
                  Data[i].TOTAL_GENDER_FAMILY_UPTO_8TH_STD,
                  Data[i].MALE_FAMILY_MEMBER_UPTO_10TH_STD,
                  Data[i].FEMALE_FAMILY_MEMBER_UPTO_10TH_STD,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_10TH_STD,
                  Data[i].TOTAL_GENDER_FAMILY_UPTO_10TH_STD,
                  Data[i].MALE_FAMILY_MEMBER_UPTO_12TH_STD,
                  Data[i].FEMALE_FAMILY_MEMBER_UPTO_12TH_STD,
                  Data[i].TOTAL_GENDER_FAMILY_UPTO_12TH_STD,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_12TH_STD,
                  Data[i].MALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                  Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                  Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                  Data[i].MALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                  Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                  Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                  Data[i].MALE_FAMILY_MEMBER_GRADUATION,
                  Data[i].FEMALE_FAMILY_MEMBER_GRADUATION,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_GRADUATION,
                  Data[i].TOTAL_GENDER_FAMILY_GRADUATION,
                  Data[i].MALE_FAMILY_MEMBER_POST_GRADUATION ,
                  Data[i].FEMALE_FAMILY_MEMBER_POST_GRADUATION,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_POST_GRADUATION,
                  Data[i].TOTAL_GENDER_FAMILY_POST_GRADUATION,
                  Data[i].MALE_FAMILY_MEMBER_PHD ,
                  Data[i].FEMALE_FAMILY_MEMBER_PHD,
                  Data[i].TRANSGENDER_FAMILY_MEMBER_PHD,
                  Data[i].TOTAL_GENDER_FAMILY_MEMBER_PHD,
              ];
          }
          else{
            dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"", Data[i].WARD_OR_VILLAGE_NAME, Data[i].WARD_OR_VILLAGE_NAME !="Total" ?  Data[i].WARD_OR_VILLAGE_ID:"", 
               Data[i].TOTAL_SURVEYED,
               Data[i].MALE_NOT_LITERATE,
                Data[i].FEMALE_NOT_LITERATE,
                Data[i].TRANSGENDER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_NOT_LITERATE,
                Data[i].MALE_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FUNCTIONAL_LITERACY,
                Data[i].MALE_UPTO_5TH_STD,
                Data[i].FEMALE_UPTO_5TH_STD,
                Data[i].TRANSGENDER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_UPTO_5TH_STD,
                Data[i].MALE_UPTO_8TH_STD,
                Data[i].FEMALE_UPTO_8TH_STD,
                Data[i].TRANSGENDER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_UPTO_8TH_STD,
                Data[i].MALE_UPTO_10TH_STD,
                Data[i].FEMALE_UPTO_10TH_STD,
                Data[i].TRANSGENDER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_UPTO_10TH_STD,
                Data[i].MALE_UPTO_12TH_STD,
                Data[i].FEMALE_UPTO_12TH_STD,
                Data[i].TRANSGENDER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_UPTO_12TH_STD,
                Data[i].MALE_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_DIPLOMA_PROFESSIONAL ,
                Data[i].TOTAL_GENDER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_GRADUATION,
                Data[i].FEMALE_GRADUATION,
                Data[i].TRANSGENDER_GRADUATION,
                Data[i].TOTAL_GENDER_GRADUATION,
                Data[i].MALE_POST_GRADUATION,
                Data[i].FEMALE_POST_GRADUATION,
                Data[i].TRANSGENDER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_POST_GRADUATION,
                Data[i].MALE_PHD,
                Data[i].FEMALE_PHD,
                Data[i].TOTAL_GENDER_PHD,
                Data[i].TRANSGENDER_PHD,
                Data[i].MALE_FAMILY_MEMBER_NOT_LITERATE ,
                Data[i].FEMALE_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TRANSGENDER_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_FAMILY_NOT_LITERATE,
                Data[i].MALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].MALE_FAMILY_MEMBER_UPTO_5TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_5TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_8TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_8TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_10TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_12TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_FAMILY_MEMBER_GRADUATION,
                Data[i].FEMALE_FAMILY_MEMBER_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_POST_GRADUATION ,
                Data[i].FEMALE_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_POST_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_PHD ,
                Data[i].FEMALE_FAMILY_MEMBER_PHD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_PHD,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_PHD,
          ];
        }
              body.push(dataRow);
            }
            if(this.Swatch_Ur=='U')
            {
              body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])
            }
            else
            {
              body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])
            }
          }


          else  if(this.Reportlevel =='3')
            {
            body.push(['S.no','Village',
            'Village Revenue Code',
            'Total Surveyed',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            'Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            'Total',
            'Male',
            'Female',
            'Transgender',
            'Total',
            'Male',
            'Female',
            'Transgender',
            'Total',
            'Male',
            'Female',
            'Transgender',
            'Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            'Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',
            'Male',
            'Female',
            'Transgender',
            ' Total',],);
            //body
            for (let i = 0; i < Data.length; i++) {
              var dataRow = [];
              dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME,  Data[i].WARD_OR_VILLAGE_NAME !="Total" ?  Data[i].WARD_OR_VILLAGE_ID:"", 
              Data[i].TOTAL_SURVEYED,
               Data[i].MALE_NOT_LITERATE,
                Data[i].FEMALE_NOT_LITERATE,
                Data[i].TRANSGENDER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_NOT_LITERATE,
                Data[i].MALE_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FUNCTIONAL_LITERACY,
                Data[i].MALE_UPTO_5TH_STD,
                Data[i].FEMALE_UPTO_5TH_STD,
                Data[i].TRANSGENDER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_UPTO_5TH_STD,
                Data[i].MALE_UPTO_8TH_STD,
                Data[i].FEMALE_UPTO_8TH_STD,
                Data[i].TRANSGENDER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_UPTO_8TH_STD,
                Data[i].MALE_UPTO_10TH_STD,
                Data[i].FEMALE_UPTO_10TH_STD,
                Data[i].TRANSGENDER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_UPTO_10TH_STD,
                Data[i].MALE_UPTO_12TH_STD,
                Data[i].FEMALE_UPTO_12TH_STD,
                Data[i].TRANSGENDER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_UPTO_12TH_STD,
                Data[i].MALE_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_DIPLOMA_PROFESSIONAL ,
                Data[i].TOTAL_GENDER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_GRADUATION,
                Data[i].FEMALE_GRADUATION,
                Data[i].TRANSGENDER_GRADUATION,
                Data[i].TOTAL_GENDER_GRADUATION,
                Data[i].MALE_POST_GRADUATION,
                Data[i].FEMALE_POST_GRADUATION,
                Data[i].TRANSGENDER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_POST_GRADUATION,
                Data[i].MALE_PHD,
                Data[i].FEMALE_PHD,
                Data[i].TOTAL_GENDER_PHD,
                Data[i].TRANSGENDER_PHD,
                Data[i].MALE_FAMILY_MEMBER_NOT_LITERATE ,
                Data[i].FEMALE_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TRANSGENDER_FAMILY_MEMBER_NOT_LITERATE,
                Data[i].TOTAL_GENDER_FAMILY_NOT_LITERATE,
                Data[i].MALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].FEMALE_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TRANSGENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_FUNCTIONAL_LITERACY,
                Data[i].MALE_FAMILY_MEMBER_UPTO_5TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_5TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_5TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_8TH_STD ,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_8TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_8TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_10TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_10TH_STD,
                Data[i].MALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].FEMALE_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].TOTAL_GENDER_FAMILY_UPTO_12TH_STD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_UPTO_12TH_STD,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_VOCATIONAL,
                Data[i].MALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].FEMALE_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TRANSGENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_DIPLOMA_PROFESSIONAL,
                Data[i].MALE_FAMILY_MEMBER_GRADUATION,
                Data[i].FEMALE_FAMILY_MEMBER_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_POST_GRADUATION ,
                Data[i].FEMALE_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TRANSGENDER_FAMILY_MEMBER_POST_GRADUATION,
                Data[i].TOTAL_GENDER_FAMILY_POST_GRADUATION,
                Data[i].MALE_FAMILY_MEMBER_PHD ,
                Data[i].FEMALE_FAMILY_MEMBER_PHD,
                Data[i].TRANSGENDER_FAMILY_MEMBER_PHD,
                Data[i].TOTAL_GENDER_FAMILY_MEMBER_PHD,
            
            ];
              body.push(dataRow);
            }
            body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1]);

          }
              
        
              if(this.Reportlevel =='0')
              {
                this.mid.JSONToCSVConvertor(
                  body,
                  'District Level'+'_'+this.Ur+'_'+'Educational Report Data',
                  true
                 );
              }
              if(this.Reportlevel =='1')
              {
                this.mid.JSONToCSVConvertor(
                  body,
                 this.District +'_'+this.Ur+'_'+ 'Educational Report Data',
                  true
                 );
              }
              if(this.Reportlevel =='2')
              {
                this.mid.JSONToCSVConvertor(
                  body,
                  this.Mandal +'_'+this.Ur+'_'+'Educational Report Data',
                  true
                 );
              }
              if(this.Reportlevel =='3')
              {
                this.mid.JSONToCSVConvertor(
                  body,
                  this.Gp +'_'+this.Ur+'_'+'Educational Report Data',
                  true
                 );
              }
        
           
            }
          }
          }
      




















  }
