import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-villagedeletion',
  templateUrl: './villagedeletion.component.html',
  styleUrls: ['./villagedeletion.component.css']
})

export class VillagedeletionComponent   {

  constructor(

    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,


  ) {
  
  }
  roleid:any='';roledistid:any='';
  ngOnInit(): void {
    debugger;
    this.roleid=sessionStorage.getItem("userrole");
    if (
    (sessionStorage.getItem('username') == '')) {

     this.router.navigate(['/NewHome']); return;
    }
    
    if (this.roleid=='1004' || this.roleid=='1003')
    { 
      this.Swatch_Ur=sessionStorage.getItem('userurtype');
      this.roledistid= sessionStorage.getItem('userdistcode');
      this.GetDistrictSurveyReportdata();
    }
    else   if (this.roleid=='1001' || this.roleid=='1002'|| this.roleid=='1006')
    {
      this.roledistid='0';
      this.Swatch_Ur="R";
      this.ischecked=true;
      this.GetDistrictSurveyReportdata();
    }
    
  }

  URchange(URID:any)
  {
   this.Swatch_Ur=URID;
   if(this.Swatch_Ur!='')
   {
    this.GetDistrictSurveyReportdata();
   }
  }
  distarray: any[] = [];Swatch_DISTRICT:any='0';  dataRow:any []=[];
  Swatch_Ur:any="";
  ischecked:boolean=false;
  ischecked1:boolean=false;
  Reportlevel:any=0;
  SurveyReportdata: any[] = [];typeid:any='';
  VillageSurveyReportdata: any[] = [];MandalSurveyReportdata: any[] = [];
  GpSurveyReportdata: any[] = [];
  async  GetDistrictSurveyReportdata(): Promise<void> 
  {
    sessionStorage.setItem('dist', '');
    sessionStorage.setItem('distname', '');
    sessionStorage.setItem('mandal', '');
    sessionStorage.setItem('mandalname', '');
    this.Reportlevel=0;
    debugger;
      const req = {
        type: "155",
        input01:this.Swatch_Ur.toString(),
        input02:this.roledistid.toString(),
      }
      this.SurveyReportdata=[];   
      this.details=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      this.SurveyReportdata=rsdata1.Details; 
      /*   this.sumtotalrows(rsdata.Details,"DISTRICT_NAME");
        this.SurveyReportdata.push(this.details); */
    }
    details:any[]=[];
    async  sumtotalrows(details:any,column:any): Promise<void> 
    {
      const scores =details; 
   
      const sumScores = (arr:any) => {
        return arr.reduce((acc:any, val:any) => {
          acc[column] = 'Total';
           Object.keys(val).forEach(key => {
            /*   if((key!== 'DISTRICT_NAME1')){ */
                 acc[key] += val[key];
             /*  }; */
           });
         /*   if((acc['DISTRICT_NAME'] !== 'all')){ */
              acc[column] = 'Total';
           /* }; */
           return acc;
        });
       
     };
     
     debugger;
     let det=sumScores(details);
     if(details.length ==1)
     {
      det[column]='Total';
     }
    for(var k in det) {
      if(k != "0")
      {
        delete scores[k];
      }
      
   } 
   this.details=det;
    }
    District:any='';Mandal:any='';Gp:any='';
    async  GetMandalSurveyReportdata(Distrctcode:any,Distname:any): Promise<void> 
  {
    this.District=Distname;
    sessionStorage.setItem('dist', Distrctcode);
    sessionStorage.setItem('distname', Distname);
    this.Reportlevel++;
    let rolemandalcode:any='';
    if (this.roleid=='1004' || this.roleid=='1003')
    {
      if(this.Swatch_Ur.toString() =="U")
      {
        rolemandalcode=sessionStorage.getItem('userulbcode');
      }
      else  if(this.Swatch_Ur.toString() =="R")
      {

        if (this.roleid=='1004')
        {
        rolemandalcode=sessionStorage.getItem('usermandalcode');
        }
        else{
          rolemandalcode='0';
        }
       
      }
     
    }
    else{
      rolemandalcode='0';
    }
    debugger;
      const req = {
        type: this.Swatch_Ur.toString() =="U" ?"156":"158",
        input01:Distrctcode.toString(),
        input02:rolemandalcode.toString(),
      }
     /*  alert(sessionStorage.getItem('usermandalcode')); */
      this.MandalSurveyReportdata=[]; 
      this.details=[];  
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      if(rsdata.code)
        {
          this.MandalSurveyReportdata=rsdata.Details; 
       /*    this.sumtotalrows(rsdata1.Details,"MANDAL_NAME");
          this.MandalSurveyReportdata.push(this.details); */
        }
        else{
          this.alt.toasterror('No data found')
        }
        this.roleid=sessionStorage.getItem("userrole");
      
    }

    async  GetGpSurveyReportdata(mandalname:any,Mandalcode:any): Promise<void> 
  {
    this.District=sessionStorage.getItem('distname');
    this.Mandal=mandalname;
    sessionStorage.setItem('mandal', Mandalcode);
    sessionStorage.setItem('mandalname', mandalname);
    this.Reportlevel++;
    debugger;
    let rolegpcode:any='';
    if (this.roleid=='1004' || this.roleid=='1003')
    {
      if(this.Swatch_Ur.toString() =="U")
      {
        rolegpcode='0';
      }
      else  if(this.Swatch_Ur.toString() =="R")
      {
        rolegpcode=sessionStorage.getItem('usergpcode');
      }
     
    }
    else{
      rolegpcode='0';
    }
    const req = {
      type: this.Swatch_Ur.toString() =="U" ?"157":"159",
      input01:sessionStorage.getItem('dist'),
      input02:Mandalcode.toString(),
      input03:this.Swatch_Ur.toString(),
      input04:rolegpcode.toString(),
    }
    this.checkedrecords=[];this.finalcheckrecords=[];
      this.GpSurveyReportdata=[];  
      this.details=[]; 
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.GpSurveyReportdata=rsdata.Details; 
      this.GpSurveyReportdata.forEach(function(e:any){
        if (typeof e === "object" ){
          e["SURVEY_CheckId"] = false
        }
      });
     /*  this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
      this.GpSurveyReportdata.push(this.details); */
    }
    gpcode:any='';
    async  GetVillageSurveyReportdata(Gpname:any,gpcode:any): Promise<void> 
  {
    this.gpcode=gpcode;
    this.Gp=Gpname;
    this.Reportlevel++;
    debugger;
      const req = {
        type: "104",
        input02:sessionStorage.getItem('dist'),
        input03:sessionStorage.getItem('mandal'),
        input04:gpcode.toString(),
        input01:this.Swatch_Ur.toString(),
      }
      this.checkedrecords=[];this.finalcheckrecords=[];
      this.VillageSurveyReportdata=[];  
      this.details=[]; 
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.VillageSurveyReportdata=rsdata.Details; 
      this.VillageSurveyReportdata.forEach(function(e:any){
        if (typeof e === "object" ){
          e["SURVEY_CheckId"] = false
        }
      });
 /*      this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
      this.VillageSurveyReportdata.push(this.details); */
    }

   
  
    checkedrecords:any[]=[];finalcheckrecords:any=[];
    RecordcheckGp(event:any,row: any) {
     debugger;
     if(event.target.checked== true)
     {
     let item ={ "SURVEY_ID":row.WARD_OR_VILLAGE_ID};
  
     this.checkedrecords.push(item);
     var result = this.checkedrecords.map(function(val) {
          return val.SURVEY_ID;
        }).join(',');
  
     this.finalcheckrecords =result;
    }
     else if(event.target.checked== false)
     {
      debugger;
      this.checkedrecords = this.checkedrecords.filter(recolist => recolist.SURVEY_ID !== row.WARD_OR_VILLAGE_ID);
  
       var result = this.checkedrecords.map(function(val) {
          return val.SURVEY_ID;
       }).join(',');
      this.finalcheckrecords = result;
     }
     debugger;
     alert(this.finalcheckrecords);
    }

    Recordcheckvillage(event:any,row: any) {
      debugger;
      this.finalcheckrecords=[];
      this.checkedrecords=[];
      if(event.target.checked== true)
      {
      let item ={ "SURVEY_ID":row.ID};
   
      this.checkedrecords.push(item);
      var result = this.checkedrecords.map(function(val) {
           return val.SURVEY_ID;
         }).join(',');
   
      this.finalcheckrecords =result;
     }
      else if(event.target.checked== false)
      {
       debugger;
       this.checkedrecords = this.checkedrecords.filter(recolist => recolist.SURVEY_ID !== row.ID);
   
        var result = this.checkedrecords.map(function(val) {
           return val.SURVEY_ID;
        }).join(',');
       this.finalcheckrecords = result;
      }
      debugger;
    
     }
     

     async  deletiondata(): Promise<void> 
     {
       debugger;
         const req = {
           type: "202",
           input01:this.finalcheckrecords
         }
         let urlservice = "api/swachaandhra/SwachhAndhra_Masters";
         let responce = await this.apipost.clinetposturlauth(req, urlservice);
         let rsdata = JSON.parse(this.mid.deccall(responce.data)); 
       
         debugger;
        this.Reportlevel--;
           this.alt.success('Deleted Successfully' );
           this.GetVillageSurveyReportdata(this.Gp,this.gpcode);
        
       }
  
    Back()
    {
      this.checkedrecords=[];this.finalcheckrecords=[];
      if(this.Swatch_Ur == "U" && this.Reportlevel == 4)
      {
        this.Reportlevel =3;
      }
      this.Reportlevel--;
    }

  Data1:any[]=[]; Ur:any=''
  mandalname='';
      villagename='';

 mandalgpcode:any='';
  async  GetSurveyReportdata(villagewardname:any,villagewrdid:any,type:any): Promise<void> 
  {
    debugger;
   
  
  
  
      const req = {
        type: type,
        input01:'',
        input03:villagewrdid.toString()
      };
     this.SurveyReportdata=[];   
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      if(rsdata.code)
      {
        this.Reportlevel=4;
        console.log(rsdata.Details);
        this.SurveyReportdata=rsdata.Details; 
      }
      else{
        this.alt.toasterror('No data found')
      }
     
    
  }


  generalinfoReportdata: any[] = [];
  async  Getgeneralinformationreportdata(Surveyid:any): Promise<void> 
  {
  debugger;
  
      const req = {
        type: '106',
        input01:Surveyid.toString()
      };
this.generalinfoReportdata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      debugger;
      this.generalinfoReportdata=rsdata.Details  ;
    
  }

familyinfodata:any []=[];
  async  GetFamilymembersReportdata(Surveyid:any): Promise<void> 
  {
   
 
    
      const req = {
        type: '107',
        input02: Surveyid.toString()
      };
this.familyinfodata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      debugger;
      this.familyinfodata=rsdata.Details;
    
  }


  Jobroledata:any []=[];
  async  GetjobroleReportdata(Surveyid:any): Promise<void> 
  {
    debugger;
  
      const req = {
        type: '108',
        input08:Surveyid.toString()
      };
this.Jobroledata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.Jobroledata=rsdata.Details;
    
  }
  SocioReportdata:any[]=[];
  async  GetSocioReportdata(Surveyid:any): Promise<void> 
  {
    
   
    debugger;
      const req = {
        type: '109',
        input06:Surveyid.toString()
      };
this.SocioReportdata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.SocioReportdata=rsdata.Details;
    
  }

  Insurencedata:any []=[];
  async  GetInsurenceReportdata(Surveyid:any): Promise<void> 
  {
    debugger;
   
      const req = {
        type: '110',
        input05:Surveyid.toString()
      };
this.Insurencedata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.Insurencedata=rsdata.Details;
    
  }


  feedbackdata:any []=[];
  async  Getfeedbackdata(Surveyid:any): Promise<void> 
  {
    debugger;
  
      const req = {
        type: '111',
        input10:Surveyid.toString()
      };
this.feedbackdata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.feedbackdata=rsdata.Details;
    
  }

  

  async ViewReports(Surveyid: string): Promise<void> {
    debugger;
    if((this.SurveyReportdata.length>0 ))
    {
    this.addModaldisplay='block'
   this.Getgeneralinformationreportdata(Surveyid);
   this.GetFamilymembersReportdata(Surveyid);
 this.GetSocioReportdata(Surveyid);
  this.GetInsurenceReportdata(Surveyid); 
  this.GetjobroleReportdata(Surveyid);
  //this.Getfeedbackdata(Surveyid);
    }
  }
  exportSlipXl() {
    
    if (this.Swatch_Ur == 'U') {
      this.Ur = 'Urban';
      this.mandalname='Urban Local Body';
      this.villagename='Ward';
    }
    else if (this.Swatch_Ur == 'R') {
      this.Ur = 'Rural';
      this.mandalname='Mandal';
      this.villagename='Gram panchayat';
    }
    
    if(this.Reportlevel =='0')
    {
      this.Data1= this.SurveyReportdata;
    }
    if(this.Reportlevel =='1')
    {
      this.Data1= this.MandalSurveyReportdata;
    }
    if(this.Reportlevel =='2')
    {
      this.Data1= this.GpSurveyReportdata;
    }
    if(this.Reportlevel =='3')
    {
      this.Data1= this.VillageSurveyReportdata;
    }
    const Data=this.Data1;
  if(this.SurveyReportdata.length > 0)
  {
    const checkdata = Math.max(Data.length);
    if(checkdata>0){
      debugger;
      //firstreport
  
      var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','SAFAIMITRA SURVEY - APPROVE/REJECT COUNT REPORT','','',''],
      );
      body.push(dataRow);
      if(this.Reportlevel =='1')
      {
        body.push(['','District :',this.District,'','','',''],
        );
      }
      else if(this.Reportlevel =='2')
      {
        body.push(['','District :',this.District,'Mandal :',this.Mandal,'','','' ],
        );
      }
      else if(this.Reportlevel =='3')
      {
        body.push(['', 'District :',this.District, 'Mandal :',this.Mandal, 'Grampanchayat/Ward :',this.Gp,'',''],
        );
      }
     
    
      if(this.Reportlevel =='0')
      {   
        if (this.Swatch_Ur == 'R') {
          body.push(['S.No',
          'District',
          'Total Surveyed',
          'EOPRRD to be Approved',
          'EOPRRD Rejected',
          'DPO to be Approved',
          'Total Approved'],);
        }
        else  {
          body.push(['S.No',
          'District',
          'Total Surveyed',
          'Nodal Officer to be Approved',
          'Nodal Officer Rejected',
          'Municipal Commissioner to be Approved',
          'Total Approved'],);
        }
     
      //body
      for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        dataRow = [Data[i].DISTRICT_NAME !="Total" ? i+1:"",Data[i].DISTRICT_NAME,
        Data[i].SUBMITTED_COUNT,
Data[i].NODAL_PENDING_COUNT,
Data[i].NODAL_REJECT_COUNT,
Data[i].MC_PENDING_COUNT,
Data[i].FINAL_APPROVAL_COMPLETED
      ];
        body.push(dataRow);
      }
      if (this.Swatch_Ur == 'R') {
      body.push(['No Of Records:','','','','','',(Data.length-1)],
      );
      }else{
        body.push(['No Of Records:','','','','','',(Data.length-1)], );
      }
    }

    else  if(this.Reportlevel =='1')
    {   
        if (this.Swatch_Ur == 'R') {
          body.push(['S.No','Mandal', 'Total Surveyed',
          'EOPRRD to be Approved',
          'EOPRRD Rejected',
          'DPO to be Approved',
          'Total Approved'],);
        }
        else  {
          body.push(['S.No','Urban Local Body','Urban Local Body Code',
          'Total Surveyed',
          'EOPRRD to be Approved',
          'EOPRRD Rejected',
          'DPO to be Approved',
          'Total Approved'],);
        }
     
      //body
      for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        if(this.Swatch_Ur == 'R'){
        dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"",Data[i].MANDAL_NAME,
        Data[i].SUBMITTED_COUNT,
Data[i].NODAL_PENDING_COUNT,
Data[i].NODAL_REJECT_COUNT,
Data[i].MC_PENDING_COUNT,
Data[i].FINAL_APPROVAL_COMPLETED
      ];
    }
      else{
        dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"",Data[i].MANDAL_NAME,Data[i].MANDAL_NAME !="Total" ? Data[i].MANDAL_ID:"",
        Data[i].SUBMITTED_COUNT,
Data[i].NODAL_PENDING_COUNT,
Data[i].NODAL_REJECT_COUNT,
Data[i].MC_PENDING_COUNT,
Data[i].FINAL_APPROVAL_COMPLETED
      ];
    }
        body.push(dataRow);
      }
      if(this.Swatch_Ur == 'R'){
      body.push(['No Of Records:','','','','','',(Data.length-1)],
      );}
      else{
        body.push(['No Of Records:','','','','','','',(Data.length-1)],
        );
      }
    }


    else  if(this.Reportlevel =='2')
    {   
        if (this.Swatch_Ur == 'R') {
          body.push(['S.No','Gram Panchayat','GP(LGD)Code', 'Total Surveyed',
          'EOPRRD to be Approved',
          'EOPRRD Rejected',
          'DPO to be Approved',
          'Total Approved'],);
        }
        else  {
          body.push(['S.No','Ward', 'Total Surveyed',
          'EOPRRD to be Approved',
          'EOPRRD Rejected',
          'DPO to be Approved',
          'Total Approved'],);
        }
     
      //body
      for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        if(this.Swatch_Ur == 'R'){
          dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME,Data[i].WARD_OR_VILLAGE_NAME !="Total" ? Data[i].WARD_OR_VILLAGE_ID:"",
          Data[i].SUBMITTED_COUNT,
          Data[i].NODAL_PENDING_COUNT,
          Data[i].NODAL_REJECT_COUNT,
          Data[i].MC_PENDING_COUNT,
          Data[i].FINAL_APPROVAL_COMPLETED
      ];
    }
      else{
        
        dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME,Data[i].SUBMITTED_COUNT,
        Data[i].NODAL_PENDING_COUNT,
        Data[i].NODAL_REJECT_COUNT,
        Data[i].MC_PENDING_COUNT,
        Data[i].FINAL_APPROVAL_COMPLETED
      ];
    }
        body.push(dataRow);
      
      }
      if(this.Swatch_Ur == 'R'){
        body.push(['No Of Records:','','','','','','',(Data.length-1)],
        );}
        else{
          body.push(['No Of Records:','','','','','',(Data.length-1)],
          );
        }
    }



  else  if(this.Reportlevel =='3')
  {  
    if(this.Swatch_Ur == 'R'){ 
  body.push(['S.No','Village',
  'Village Revenue Code ', 'Total Surveyed',
  'EOPRRD to be Approved',
  'EOPRRD Rejected',
  'DPO to be Approved',
  'Total Approved'],);
    }
  //body
  for (let i = 0; i < Data.length; i++) {
    var dataRow = [];
    dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME,  
    Data[i].WARD_OR_VILLAGE_ID,Data[i].SUBMITTED_COUNT,
    Data[i].NODAL_PENDING_COUNT,
    Data[i].NODAL_REJECT_COUNT,
    Data[i].MC_PENDING_COUNT,
    Data[i].FINAL_APPROVAL_COMPLETED
  ];
}
    body.push(dataRow);
    if(this.Swatch_Ur == 'R'){
      body.push(['No Of Records:','','','',,'','','',(Data.length-1)],
      );}
      else{
        body.push(['No Of Records:','','','','',(Data.length-1)],
        );
      }
 
  }
      //footer
    
  
      if(this.Reportlevel =='0')
      {
        this.mid.JSONToCSVConvertor(
          body,
          'District Level'+'_'+this.Ur+'_'+'Surveyor Login Registration Count Report Data',
          true
          );
      }
      if(this.Reportlevel =='1')
      {
        this.mid.JSONToCSVConvertor(
          body,
          this.District +'_'+this.Ur+'_'+ 'Surveyor Login Registration Count Report Data',
          true
          );
      }
      if(this.Reportlevel =='2')
      {
        this.mid.JSONToCSVConvertor(
          body,
          this.Mandal +'_'+this.Ur+'_'+'Surveyor Login Registration Count Report Data',
          true
          );
      }
      if(this.Reportlevel =='3')
      {
        this.mid.JSONToCSVConvertor(
          body,
          this.Gp +'_'+this.Ur+'_'+'Surveyor Login Registration Count Report Data',
          true
          );
      }
    
    }
  }
  }
  addModaldisplay:any='none';
  modalclose()
  {
    this.addModaldisplay = 'none';
  }

  img:any
  async ViewCatimg(imageurl: string): Promise<void> {
    debugger;
    this.Imagedisplay='block'
   if(this.mid.Deploystage =="UAT")
   {
    this.img = "https://apsanitationworkerssurvey.ap.gov.in//SWUAT//SwachhAndhraSurveyImages/"+imageurl;
   }
   else  if(this.mid.Deploystage =="PROD")
   {
    this.img = "https://apsanitationworkerssurvey.ap.gov.in//SWAPI//SwachhAndhraSurveyImages/"+imageurl;
   }
  
  
  }
  Imagedisplay='none';
  imgmodalclose()
  {
    this.Imagedisplay='none';
  }
 
}
