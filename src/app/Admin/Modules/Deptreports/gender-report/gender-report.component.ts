import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-gender-report',
  templateUrl: './gender-report.component.html',
  styleUrls: ['./gender-report.component.css']
})
export class GenderReportComponent  {

  constructor(

    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,


  ) {
  
  }
  roleid:any='';roledistid:any='';
  ngOnInit(): void {
    debugger;
    this.roleid=sessionStorage.getItem("userrole");
    if (
    (sessionStorage.getItem('username') == '')) {

     this.router.navigate(['/NewHome']); return;
    }
    
    if (this.roleid=='1004' || this.roleid=='1003')
    { 
      this.Swatch_Ur=sessionStorage.getItem('userurtype');
      this.roledistid= sessionStorage.getItem('userdistcode');
      this.GetDistrictSurveyReportdata();
    }
    else   if (this.roleid=='1001' || this.roleid=='1002'|| this.roleid=='1006')
    {
      this.roledistid='0';
      this.Swatch_Ur="U";
      this.ischecked=true;
      this.GetDistrictSurveyReportdata();
    }
    
  }

  URchange(URID:any)
  {
   this.Swatch_Ur=URID;
   if(this.Swatch_Ur!='')
   {
    this.GetDistrictSurveyReportdata();
   }
  }
  distarray: any[] = [];Swatch_DISTRICT:any='0';  dataRow:any []=[];
  Swatch_Ur:any="";
  ischecked:boolean=false;
  ischecked1:boolean=false;
  Reportlevel:any=0;
  SurveyReportdata: any[] = [];typeid:any='';
  VillageSurveyReportdata: any[] = [];MandalSurveyReportdata: any[] = [];
  GpSurveyReportdata: any[] = [];
  async  GetDistrictSurveyReportdata(): Promise<void> 
  {
    sessionStorage.setItem('dist', '');
    sessionStorage.setItem('distname', '');
    sessionStorage.setItem('mandal', '');
    sessionStorage.setItem('mandalname', '');
    this.Reportlevel=0;
    debugger;
      const req = {
        type: "101",
        input01:this.Swatch_Ur.toString(),
        input02:this.roledistid.toString(),
      }
      this.SurveyReportdata=[];   
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      this.SurveyReportdata=rsdata.Details; 
      this.sumtotalrows(rsdata1.Details,"DISTRICT_NAME");
      this.SurveyReportdata.push(this.details);
    }
    details:any[]=[];
    async  sumtotalrows(details:any,column:any): Promise<void> 
    {
      const scores =details; 
   
      const sumScores = (arr:any) => {
        return arr.reduce((acc:any, val:any) => {
          acc[column] = 'Total';
           Object.keys(val).forEach(key => {
            /*   if((key!== 'DISTRICT_NAME1')){ */
                 acc[key] += val[key];
             /*  }; */
           });
         /*   if((acc['DISTRICT_NAME'] !== 'all')){ */
              acc[column] = 'Total';
           /* }; */
           return acc;
        });
       
     };
     
     debugger;
     let det=sumScores(details);
     if(details.length ==1)
     {
      det[column]='Total';
     }
    for(var k in det) {
      if(k != "0")
      {
        delete scores[k];
      }
      
   } 
   this.details=det;
    }
    District:any='';Mandal:any='';Gp:any='';
    async  GetMandalSurveyReportdata(Distrctcode:any,Distname:any): Promise<void> 
  {
    this.District=Distname;
    sessionStorage.setItem('dist', Distrctcode);
    sessionStorage.setItem('distname', Distname);
    this.Reportlevel++;
    let rolemandalcode:any='';
    if (this.roleid=='1004' || this.roleid=='1003')
    {
      if(this.Swatch_Ur.toString() =="U")
      {
        rolemandalcode=sessionStorage.getItem('userulbcode');
      }
      else  if(this.Swatch_Ur.toString() =="R")
      {
        if(this.roleid=='1003')
      {
        rolemandalcode="0";
      }
      else{
        rolemandalcode=sessionStorage.getItem('usermandalcode');
      }
       
      }
     
    }
    else{
      rolemandalcode='0';
    }
    debugger;
      const req = {
        type: this.Swatch_Ur.toString() =="U" ?"105":"102",
        input01:Distrctcode.toString(), 
        input02:this.Swatch_Ur.toString(),
        input03:rolemandalcode.toString(),
      }
     /*  alert(sessionStorage.getItem('usermandalcode')); */
      this.MandalSurveyReportdata=[];   
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      if(rsdata.code)
        {
          this.MandalSurveyReportdata=rsdata.Details;
          this.sumtotalrows(rsdata1.Details,"MANDAL_NAME");
          this.MandalSurveyReportdata.push(this.details); 
        }
        else{
          this.alt.toasterror('No data found')
        }
        this.roleid=sessionStorage.getItem("userrole");
      
    }

    async  GetGpSurveyReportdata(mandalname:any,Mandalcode:any): Promise<void> 
  {
    this.District=sessionStorage.getItem('distname');
    this.Mandal=mandalname;
    sessionStorage.setItem('mandal', Mandalcode);
    sessionStorage.setItem('mandalname', mandalname);
    this.Reportlevel++;
    debugger;
    let rolegpcode:any='';
    if (this.roleid=='1004' || this.roleid=='1003')
    {
      if(this.Swatch_Ur.toString() =="U")
      {
        rolegpcode='0';
      }
      else  if(this.Swatch_Ur.toString() =="R")
      {
       
        rolegpcode="0";
        
      }
     
    }
    else{
      rolegpcode='0';
    }
    const req = {
      type: this.Swatch_Ur.toString() =="U" ?"106":"103",
      input01:sessionStorage.getItem('dist'),
      input02:Mandalcode.toString(),
      input03:this.Swatch_Ur.toString(),
      input04:rolegpcode.toString(),
    }
      this.GpSurveyReportdata=[];   
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.GpSurveyReportdata=rsdata.Details; 
      this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
      this.GpSurveyReportdata.push(this.details);
    }

    async  GetVillageSurveyReportdata(Gpname:any,gpcode:any): Promise<void> 
  {
    this.Gp=Gpname;
    this.Reportlevel++;
    debugger;
      const req = {
        type: "104",
        input01:sessionStorage.getItem('dist'),
        input02:sessionStorage.getItem('mandal'),
        input03:gpcode.toString(),
        input04:this.Swatch_Ur.toString(),
      }
      this.VillageSurveyReportdata=[];   
      let urlservice = "api/swachaandhra/SwachhAndhra_DeptReports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      let rsdata1 = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.VillageSurveyReportdata=rsdata.Details; 
      this.sumtotalrows(rsdata1.Details,"WARD_OR_VILLAGE_NAME");
      this.VillageSurveyReportdata.push(this.details);
    }
  
    Back()
    {
      this.Reportlevel--;
    }

  Data1:any[]=[]; Ur:any=''
  mandalname='';
      villagename='';
  exportSlipXl() {
   
    if (this.Swatch_Ur == 'U') {
      this.Ur = 'Urban';
      this.mandalname='Urban Local Body';
      this.villagename='Ward';
    }
    else if (this.Swatch_Ur == 'R') {
      this.Ur = 'Rural';
      this.mandalname='Mandal';
      this.villagename='Gram panchayat';
    }
   
    if(this.Reportlevel =='0')
    {
      this.Data1= this.SurveyReportdata;
    }
    if(this.Reportlevel =='1')
    {
      this.Data1= this.MandalSurveyReportdata;
    }
    if(this.Reportlevel =='2')
    {
      this.Data1= this.GpSurveyReportdata;
    }
    if(this.Reportlevel =='3')
    {
      this.Data1= this.VillageSurveyReportdata;
    }
    const Data=this.Data1;
if(this.SurveyReportdata.length > 0)
{
    const checkdata = Math.max(Data.length);
    if(checkdata>0){
      debugger;
      //firstreport

      var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','','','','','','','','','','SAFAIMITRA SURVEY - GENDER DATA REPORTS','','','','','','','','','','','','','',''],
      );
      body.push(dataRow);
      if(this.Reportlevel =='1')
      {
        body.push(['','','District :',this.District,'','','','','','','','','','','','','','','','','','','','','','','',''],
        );
      }
      else if(this.Reportlevel =='2')
      {
        body.push(['','','District :',this.District,'','','Mandal :',this.Mandal,'','','','','','','','','','','','','','','','','','','','',''],
        );
      }
      else if(this.Reportlevel =='3')
      {
        body.push(['','','District :',this.District,'','','Mandal :',this.Mandal,'','','Grampanchayat/Ward :',this.Gp,'','','','','','','','','','','','','','','','',''],
        );
      }
      body.push(['','','','','','','','','','','','','','','','','','','','','','Age Group ','','','','','',''],
      );

      body.push(['','','','','','Gender','','','','Married','','','','Family Members','','','','Children','','','','Adults','','','','Senior Citizen','',''],
      );
      if(this.Reportlevel =='0')
      {   
      body.push(['S.no','District','Total Surveyed',
      'Male','Female','Transgender','Total ','Male','Female','Transgender ','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total '],);
      //body
      for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        dataRow = [Data[i].DISTRICT_NAME !="Total" ? i+1:"",Data[i].DISTRICT_NAME,
       Data[i].TOTAL_SURVEYED,Data[i].GENDER_MALE,Data[i].GENDER_FEMALE,Data[i].GENDER_TRANSGENDER,
        Data[i].TOTAL_GENDER,Data[i].MARRIED_MALE,Data[i].MARRIED_FEMALE,Data[i].MARRIED_TRANSGENDER,Data[i].TOTAL_MARRIED,
        Data[i].FAM_MEM_MALE,Data[i].FAM_MEM_FEMALE,Data[i].FAM_MEM_TRANSGENDER,Data[i].TOTAL_FAMILY_GENDER,Data[i].FAM_MEM_CHILDREN_MALE,
        Data[i].FAM_MEM_CHILDREN_FEMALE,Data[i].FAM_MEM_CHILDREN_TRANSGENDER,Data[i].TOTAL_CHILDREN_GENDER,Data[i].FAM_MEM_ADULTS_MALE,Data[i].FAM_MEM_ADULTS_FEMALE,Data[i].FAM_MEM_ADULTS_TRANSGENDER,Data[i].TOTAL_ADULTS_GENDER,Data[i].FAM_MEM_SENIOR_CITIZEN_MALE,Data[i].FAM_MEM_SENIOR_CITIZEN_FEMALE,Data[i].FAM_MEM_SENIOR_CITIZEN_TRANSGENDER,Data[i].TOTAL_SENIOR_CITIZEN_GENDER,
      ];
        body.push(dataRow);
      }
       body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])

    }
    else  if(this.Reportlevel =='1')
    {   
      if(this.Swatch_Ur =='U')
      {
        body.push(['S.no',this.mandalname,'Urban Local Body (ULB)Code',
      'Total Surveyed',
        'Male','Female','Transgender','Total ','Male','Female','Transgender ','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total '],);
       }
       else {
        {
          body.push(['S.no',this.mandalname,
        'Total Surveyed',
          'Male','Female','Transgender','Total ','Male','Female','Transgender ','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total '],);
         }
       }
     //body
    for (let i = 0; i < Data.length; i++) {
      var dataRow = [];
      if(this.Swatch_Ur =='U')
      {
        dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"",Data[i].MANDAL_NAME,  Data[i].MANDAL_NAME !="Total" ?  Data[i].MANDAL_ID:"",   
      Data[i].TOTAL_SURVEYED,Data[i].GENDER_MALE,Data[i].GENDER_FEMALE,Data[i].GENDER_TRANSGENDER,
        Data[i].TOTAL_GENDER,Data[i].MARRIED_MALE,Data[i].MARRIED_FEMALE,Data[i].MARRIED_TRANSGENDER,Data[i].TOTAL_MARRIED,
        Data[i].FAM_MEM_MALE,Data[i].FAM_MEM_FEMALE,Data[i].FAM_MEM_TRANSGENDER,Data[i].TOTAL_FAMILY_GENDER,Data[i].FAM_MEM_CHILDREN_MALE,
        Data[i].FAM_MEM_CHILDREN_FEMALE,Data[i].FAM_MEM_CHILDREN_TRANSGENDER,Data[i].TOTAL_CHILDREN_GENDER,Data[i].FAM_MEM_ADULTS_MALE,Data[i].FAM_MEM_ADULTS_FEMALE,Data[i].FAM_MEM_ADULTS_TRANSGENDER,Data[i].TOTAL_ADULTS_GENDER,Data[i].FAM_MEM_SENIOR_CITIZEN_MALE,Data[i].FAM_MEM_SENIOR_CITIZEN_FEMALE,Data[i].FAM_MEM_SENIOR_CITIZEN_TRANSGENDER,Data[i].TOTAL_SENIOR_CITIZEN_GENDER,
      ];
      }
      else{
        dataRow = [Data[i].MANDAL_NAME !="Total" ? i+1:"",Data[i].MANDAL_NAME, 
      Data[i].TOTAL_SURVEYED,Data[i].GENDER_MALE,Data[i].GENDER_FEMALE,Data[i].GENDER_TRANSGENDER,
        Data[i].TOTAL_GENDER,Data[i].MARRIED_MALE,Data[i].MARRIED_FEMALE,Data[i].MARRIED_TRANSGENDER,Data[i].TOTAL_MARRIED,
        Data[i].FAM_MEM_MALE,Data[i].FAM_MEM_FEMALE,Data[i].FAM_MEM_TRANSGENDER,Data[i].TOTAL_FAMILY_GENDER,Data[i].FAM_MEM_CHILDREN_MALE,
        Data[i].FAM_MEM_CHILDREN_FEMALE,Data[i].FAM_MEM_CHILDREN_TRANSGENDER,Data[i].TOTAL_CHILDREN_GENDER,Data[i].FAM_MEM_ADULTS_MALE,Data[i].FAM_MEM_ADULTS_FEMALE,Data[i].FAM_MEM_ADULTS_TRANSGENDER,Data[i].TOTAL_ADULTS_GENDER,Data[i].FAM_MEM_SENIOR_CITIZEN_MALE,Data[i].FAM_MEM_SENIOR_CITIZEN_FEMALE,Data[i].FAM_MEM_SENIOR_CITIZEN_TRANSGENDER,Data[i].TOTAL_SENIOR_CITIZEN_GENDER,
      ];
      }
      body.push(dataRow);
    }
    if(this.Swatch_Ur =='U')
    {  body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])
  }
    else{  body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])
  }
  }
  else  if(this.Reportlevel =='2')
    {  
      if(this.Swatch_Ur =='U') { body.push(['S.no',this.villagename,
      'Total Surveyed',
        'Male','Female','Transgender','Total ','Male','Female','Transgender ','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total '],);}
   
        else{
          body.push(['S.no',this.villagename,'GP(LGD)Code','Total Surveyed',
            'Male','Female','Transgender','Total ','Male','Female','Transgender ','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total '],);
        }
      //body
    for (let i = 0; i < Data.length; i++) {
      var dataRow = [];
      if(this.Mandal !='Penamaluru' && this.Swatch_Ur =='U')
      {
        dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"","Ward No"+Data[i].WARD_OR_VILLAGE_NAME,
        Data[i].TOTAL_SURVEYED,Data[i].GENDER_MALE,Data[i].GENDER_FEMALE,Data[i].GENDER_TRANSGENDER,
        Data[i].TOTAL_GENDER,Data[i].MARRIED_MALE,Data[i].MARRIED_FEMALE,Data[i].MARRIED_TRANSGENDER,Data[i].TOTAL_MARRIED,
        Data[i].FAM_MEM_MALE,Data[i].FAM_MEM_FEMALE,Data[i].FAM_MEM_TRANSGENDER,Data[i].TOTAL_FAMILY_GENDER,Data[i].FAM_MEM_CHILDREN_MALE,
        Data[i].FAM_MEM_CHILDREN_FEMALE,Data[i].FAM_MEM_CHILDREN_TRANSGENDER,Data[i].TOTAL_CHILDREN_GENDER,Data[i].FAM_MEM_ADULTS_MALE,Data[i].FAM_MEM_ADULTS_FEMALE,Data[i].FAM_MEM_ADULTS_TRANSGENDER,Data[i].TOTAL_ADULTS_GENDER,Data[i].FAM_MEM_SENIOR_CITIZEN_MALE,Data[i].FAM_MEM_SENIOR_CITIZEN_FEMALE,Data[i].FAM_MEM_SENIOR_CITIZEN_TRANSGENDER,Data[i].TOTAL_SENIOR_CITIZEN_GENDER,
      ];
      }
      
      else{
      dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME, Data[i].WARD_OR_VILLAGE_NAME !="Total" ?  Data[i].WARD_OR_VILLAGE_ID:"", 
      Data[i].TOTAL_SURVEYED,Data[i].GENDER_MALE,Data[i].GENDER_FEMALE,Data[i].GENDER_TRANSGENDER,
      Data[i].TOTAL_GENDER,Data[i].MARRIED_MALE,Data[i].MARRIED_FEMALE,Data[i].MARRIED_TRANSGENDER,Data[i].TOTAL_MARRIED,
      Data[i].FAM_MEM_MALE,Data[i].FAM_MEM_FEMALE,Data[i].FAM_MEM_TRANSGENDER,Data[i].TOTAL_FAMILY_GENDER,Data[i].FAM_MEM_CHILDREN_MALE,
      Data[i].FAM_MEM_CHILDREN_FEMALE,Data[i].FAM_MEM_CHILDREN_TRANSGENDER,Data[i].TOTAL_CHILDREN_GENDER,Data[i].FAM_MEM_ADULTS_MALE,Data[i].FAM_MEM_ADULTS_FEMALE,Data[i].FAM_MEM_ADULTS_TRANSGENDER,Data[i].TOTAL_ADULTS_GENDER,Data[i].FAM_MEM_SENIOR_CITIZEN_MALE,Data[i].FAM_MEM_SENIOR_CITIZEN_FEMALE,Data[i].FAM_MEM_SENIOR_CITIZEN_TRANSGENDER,Data[i].TOTAL_SENIOR_CITIZEN_GENDER,
    ];
  }
      body.push(dataRow);
    }
    if(this.Swatch_Ur =='U')
    {  body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])
  }
    else{  body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1])
  }
  }


  else  if(this.Reportlevel =='3')
  {   
  body.push(['S.no','Village','Village Revenue Code',
  'Total Surveyed',
      'Male','Female','Transgender','Total ','Male','Female','Transgender ','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total ','Male','Female','Transgender','Total '],);
      //body
  for (let i = 0; i < Data.length; i++) {
    var dataRow = [];
    dataRow = [Data[i].WARD_OR_VILLAGE_NAME !="Total" ? i+1:"",Data[i].WARD_OR_VILLAGE_NAME,  Data[i].WARD_OR_VILLAGE_NAME !="Total" ?  Data[i].WARD_OR_VILLAGE_ID:"", 
    Data[i].TOTAL_SURVEYED,Data[i].GENDER_MALE,Data[i].GENDER_FEMALE,Data[i].GENDER_TRANSGENDER,
    Data[i].TOTAL_GENDER,Data[i].MARRIED_MALE,Data[i].MARRIED_FEMALE,Data[i].MARRIED_TRANSGENDER,Data[i].TOTAL_MARRIED,
    Data[i].FAM_MEM_MALE,Data[i].FAM_MEM_FEMALE,Data[i].FAM_MEM_TRANSGENDER,Data[i].TOTAL_FAMILY_GENDER,Data[i].FAM_MEM_CHILDREN_MALE,
    Data[i].FAM_MEM_CHILDREN_FEMALE,Data[i].FAM_MEM_CHILDREN_TRANSGENDER,Data[i].TOTAL_CHILDREN_GENDER,Data[i].FAM_MEM_ADULTS_MALE,Data[i].FAM_MEM_ADULTS_FEMALE,Data[i].FAM_MEM_ADULTS_TRANSGENDER,Data[i].TOTAL_ADULTS_GENDER,Data[i].FAM_MEM_SENIOR_CITIZEN_MALE,Data[i].FAM_MEM_SENIOR_CITIZEN_FEMALE,Data[i].FAM_MEM_SENIOR_CITIZEN_TRANSGENDER,Data[i].TOTAL_SENIOR_CITIZEN_GENDER,
  ];
    body.push(dataRow);
  }
        //footer
  body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','',Data.length-1],
  );

}

     

      if(this.Reportlevel =='0')
      {
        this.mid.JSONToCSVConvertor(
          body,
          'District Level'+'_'+this.Ur+'_'+'Gender Report Data',
          true
         );
      }
      if(this.Reportlevel =='1')
      {
        this.mid.JSONToCSVConvertor(
          body,
         this.District +'_'+this.Ur+'_'+ 'Gender Report Data',
          true
         );
      }
      if(this.Reportlevel =='2')
      {
        this.mid.JSONToCSVConvertor(
          body,
          this.Mandal +'_'+this.Ur+'_'+'Gender Report Data',
          true
         );
      }
      if(this.Reportlevel =='3')
      {
        this.mid.JSONToCSVConvertor(
          body,
          this.Gp +'_'+this.Ur+'_'+'Gender Report Data',
          true
         );
      }
   
    }
  }
  }
 
}
