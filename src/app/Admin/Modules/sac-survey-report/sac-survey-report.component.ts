import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

import { ClientcallService } from 'src/app/service/clientcall.service';

import { SweetalertService } from 'src/app/thirdparty/sweetalert.service';
import { MidlayerService } from 'src/app/thirdparty/midlayer.service';
import Swal from 'sweetalert2';
import { ConsoleLogger } from '@microsoft/signalr/dist/esm/Utils';
import { Router } from '@angular/router';
 
import { DatePipe } from '@angular/common';
import { InputvalidaService } from 'src/app/thirdparty/inputvalida.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-sac-survey-report',
  templateUrl: './sac-survey-report.component.html',
  styleUrls: ['./sac-survey-report.component.css']
})
export class SacSurveyReportComponent {

  constructor(

    private router: Router,
    private alt: SweetalertService,
    private mid: MidlayerService,
    private apipost: ClientcallService,
    private spinner: NgxSpinnerService,


  ) {
  
  }
  roleid:any='';
  ngOnInit(): void {
    this.roleid=sessionStorage.getItem("userrole");
    if ((sessionStorage.getItem('username') == '') 
    ) {

      this.router.navigate(['/NewHome']); return;
    }
    if(this.roleid=='1005' )
    {this.Swatch_DISTRICT= sessionStorage.getItem('userdistcode');
    this.Swatch_MMC_CODE=sessionStorage.getItem('usermandalcode');
    this.Swatch_Ur=sessionStorage.getItem('userurtype');
    this.Swatch_GP_CODE=sessionStorage.getItem('usergpcode');
    this.Swatch_VW_CODE=sessionStorage.getItem('uservillagecode');

 
    
this.GetSurveyReportdata();
    }
    else if (this.roleid=='1004' || this.roleid=='1003')
    {
      
debugger;
      this.Swatch_DISTRICT= sessionStorage.getItem('userdistcode');

    this.Swatch_Ur=sessionStorage.getItem('userurtype');
    
    if(this.Swatch_Ur=='R')
    {
      this.Levelthree='Gram Panchayat';
      if(this.roleid=='1003')
      {
        this.Leveltwo='Mandal';
        this.mandalload();
      }
else
{
  this.Levelthree='Gram Panchayat';
//this.Swatch_GP_CODE=sessionStorage.getItem('usergpcode');
//this.villageload();
this.Swatch_MMC_CODE=sessionStorage.getItem('usermandalcode');
this.gpload();
}

    }
    else if(this.Swatch_Ur=='U')
    {
      this.Leveltwo='Urban Local Body';
      this.Levelthree='Wards';
      this.Swatch_MMC_CODE=sessionStorage.getItem('userulbcode');
      this.gpload();
     
    }
    }
    else if (this.roleid=='1001' || this.roleid=='1002' || this.roleid=='1006'){
      this.districtload();
    }
  
  }
  Leveltwo:any='Urban Local Body';
Levelthree:any='Wards';
  URchange(URID:any)
  {
   this.Swatch_Ur=URID;
   if(this.Swatch_Ur!='')
   {
    if(this.Swatch_Ur=='R')
    {
this.Leveltwo='Mandal';
this.Levelthree='Gram Panchayat';
    }
    else 
    {
      this.Leveltwo='Urban Local Body';
      this.Levelthree='Wards';
    }
    this.mandalload();
   }
  }
  distarray: any[] = [];Swatch_DISTRICT:any='0';  dataRow:any []=[];
  Swatch_Ur:any="";
  ischecked:boolean=false;
  ischecked1:boolean=false;
  async districtload(): Promise<void> {
    
    try {
      const req = {
        type: '101'
      };
      this.SurveyReportdata=[];  
    this.Swatch_DISTRICT='0';
    this.Swatch_MMC_CODE = '0';
    this.Swatch_VW_CODE = '0';
    this.Swatch_GP_CODE = '0';
    this.Swatch_Ur='U';
    this.ischecked=true;
      this.distarray = []; this.spinner.show();
      this.mandalarry = [];
      this.villagearry = [];
      this.gparry=[];
   /*    this.Masters.value.STORE_MMC_CODE = '0';
      this.Masters.value.STORE_VW_CODE = '0'; */
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      this.spinner.hide();


      this.distarray = rsdata.Details;
      let Data =this.distarray;
     debugger;
     for(var i in Data) {    

      var item = Data[i];   
  
      this.dataRow.push({ 
          "firstName" :Data[i].DISTRICT_ID,
          "lastName"  : Data[i].DISTRICT_NAME
      });
  }
     
      let obj =[
        { value: 100, category: "Total" },
        { value: 50, category: "Male" },
        { value: 40, category: "Female" },
        { value: 10, category: "Transgender" },
      ];
      console.log(obj);
      console.log(this.dataRow);
      this.spinner.hide();

    } catch (error) {

    }
  }
  mandalarry: any[] = []; Swatch_MMC_CODE:any='0';Swatch_VW_CODE:any='0';
   async mandalload(): Promise<void> {
    try {
      let typeid:any='';
     if(this.Swatch_Ur=='U') 
     {
      this.ischecked=true;
      this.typeid='115';
     }
     else
     {
      this.ischecked=false;
      this.typeid='102';
     }
     
   
      if(this.Swatch_Ur!='')
      {

      
      if(this.Swatch_DISTRICT !='0')
      {
      const req = {
        type: this.typeid.toString(),
        input01:this.Swatch_Ur.toString(),
        input02:this.Swatch_DISTRICT.toString()
      };
      this.SurveyReportdata=[];  
        this.Swatch_MMC_CODE = '0';
        this.Swatch_VW_CODE = '0';
        this.Swatch_GP_CODE = '0';
      this.mandalarry = []; this.spinner.show();
      this.villagearry = [];
this.gparry=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      this.spinner.hide();

     
      if(rsdata.code)
      {
        this.mandalarry = rsdata.Details;
      }
      else{
        this.alt.toasterror('No data found')
      }
      this.spinner.hide();
      }
     }
     else{
      this.alt.toastwarning('Please select Urban/Rural')
     }

    } catch (error) {

    }
  } 
  villagearry: any[] = [];  gparry: any[] = [];Swatch_GP_CODE:any='0';isgetdata:boolean=false;
  async gpload(): Promise<void> {
    try {
      let typeid:any='';
      if(this.Swatch_Ur=='U') 
      {
      
       this.typeid='116';
      }
      else
      {
     
       this.typeid='103';
      }
      if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0'))
      {
      const req = {
        type: this.typeid.toString(),
        input01:this.Swatch_Ur.toString(),
        input02:this.Swatch_DISTRICT.toString(),
        input03:this.Swatch_MMC_CODE.toString()
      };

      this.SurveyReportdata=[];  
        this.Swatch_GP_CODE = '0';
        this.Swatch_VW_CODE = '0';
      this.gparry = []; this.spinner.show();
this.villagearry=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));

      this.spinner.hide();

      this.gparry = rsdata.Details;

    }

    } catch (error) {

    }
  } 

 async villageload(): Promise<void> {
    try {
      if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0') && (this.Swatch_Ur =='R'))
      {
      const req = {
        type: '104',
        input01:this.Swatch_Ur.toString(),
        input02:this.Swatch_DISTRICT.toString(),
        input03:this.Swatch_MMC_CODE.toString(),
        input04:this.Swatch_GP_CODE.toString()
      };

      this.SurveyReportdata=[];  
        this.Swatch_VW_CODE = '0';
      
      this.villagearry = []; this.spinner.show();

      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));

      this.spinner.hide();

      this.villagearry = rsdata.Details;
    }

    } catch (error) {

    }
  } 
  SurveyReportdata: any[] = [];typeid:any='';mandalgpcode:any='';
  async  GetSurveyReportdata(): Promise<void> 
  {
    debugger;
    this.isgetdata=true;
   
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0')&& (this.Swatch_Ur !=''))
    {
      
      if((this.Swatch_Ur =='U'))
      { this.typeid='100';
          this.Swatch_VW_CODE=this.Swatch_GP_CODE ;
          this.mandalgpcode =this.Swatch_MMC_CODE;
      }
      else if((this.Swatch_Ur =='R'))
      {
        this.typeid='105';
        this.mandalgpcode =this.Swatch_GP_CODE;
      
      }
      this.isgetdata=false;
      const req = {
        type: this.typeid,
        input01:this.Swatch_VW_CODE == 'All' ? '' : this.Swatch_VW_CODE.toString(),
        input02:this.mandalgpcode 
      };
     this.SurveyReportdata=[];   
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      if(rsdata.code)
      {
        console.log(rsdata.Details);
        this.SurveyReportdata=rsdata.Details; 
      }
      else{
        this.alt.toasterror('No data found')
      }
     
    }
  }


  generalinfoReportdata: any[] = [];
  async  Getgeneralinformationreportdata(Surveyid:any): Promise<void> 
  {
  debugger;
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
     
      const req = {
        type: '106',
        input01:Surveyid.toString()
      };
this.generalinfoReportdata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      debugger;
      this.generalinfoReportdata=rsdata.Details  ;
    }
  }

familyinfodata:any []=[];
  async  GetFamilymembersReportdata(Surveyid:any): Promise<void> 
  {
   
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
    
      const req = {
        type: '107',
        input02: Surveyid.toString()
      };
this.familyinfodata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      debugger;
      this.familyinfodata=rsdata.Details;
    }
  }


  Jobroledata:any []=[];
  async  GetjobroleReportdata(Surveyid:any): Promise<void> 
  {
    debugger;
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
     
      const req = {
        type: '108',
        input08:Surveyid.toString()
      };
this.Jobroledata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.Jobroledata=rsdata.Details;
    }
  }
  SocioReportdata:any[]=[];
  async  GetSocioReportdata(Surveyid:any): Promise<void> 
  {
    
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
    debugger;
      const req = {
        type: '109',
        input06:Surveyid.toString()
      };
this.SocioReportdata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.SocioReportdata=rsdata.Details;
    }
  }

  Insurencedata:any []=[];
  async  GetInsurenceReportdata(Surveyid:any): Promise<void> 
  {
    debugger;
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
     
      const req = {
        type: '110',
        input05:Surveyid.toString()
      };
this.Insurencedata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.Insurencedata=rsdata.Details;
    }
  }


  feedbackdata:any []=[];
  async  Getfeedbackdata(Surveyid:any): Promise<void> 
  {
    debugger;
    if((this.Swatch_DISTRICT !='0')&& (this.Swatch_MMC_CODE !='0') && (this.Swatch_GP_CODE !='0'))
    {
     
      const req = {
        type: '111',
        input10:Surveyid.toString()
      };
this.feedbackdata=[];
      let urlservice = "api/swachaandhra/SwachhAndhra_Reports";
      // let rsdata = await this.apipost.clinetposturlauth_withoutenc(req, this.urlservice);
      let responce = await this.apipost.clinetposturlauth(req, urlservice);
      let rsdata = JSON.parse(this.mid.deccall(responce.data));
      console.log(rsdata.Details);
      this.feedbackdata=rsdata.Details;
    }
  }

  

  async ViewReports(Surveyid: string): Promise<void> {
    debugger;
    if((this.SurveyReportdata.length>0 ))
    {
    this.addModaldisplay='block'
    this.Getgeneralinformationreportdata(Surveyid);
   this.GetFamilymembersReportdata(Surveyid);
   this.GetSocioReportdata(Surveyid); 
   this.GetInsurenceReportdata(Surveyid);
   this.GetjobroleReportdata(Surveyid);


  //this.Getfeedbackdata(Surveyid);
    }
  }
  exportSlipXl() {
    const Data:any= this.SurveyReportdata;
if(this.SurveyReportdata.length > 0)
{
    const checkdata = Math.max(Data.length);
    if(checkdata>0){
      debugger;
      //firstreport

      var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','Survey Report Data','','','',''],
      );
      body.push(dataRow);
      if(this.Swatch_VW_CODE =='All')
      {
if(this.Swatch_Ur=="U")
{
  body.push(['S.no',
  'Ward No.',
  'Survey ID',
  'Core Sanitation Worker Name'],);

  for (let i = 0; i < Data.length; i++) {
    var dataRow = [];
    dataRow = [i+1,Data[i].WARD_OR_VILLAGE_NAME,
    Data[i].SURVEY_ID,
    Data[i].NAME];
    body.push(dataRow);
  }
  body.push(['No Of Records:','','',Data.length],
  );
}
else 
{
  body.push(['S.no','Village Name',
  'Survey ID',
  'Core Sanitation Worker Name'],);

  for (let i = 0; i < Data.length; i++) {
    var dataRow = [];
    dataRow = [i+1,Data[i].WARD_OR_VILLAGE_NAME,
    Data[i].SURVEY_ID,
    Data[i].NAME];
    body.push(dataRow);
  }
  body.push(['No Of Records:','','',Data.length],
  );
}
      }
      else{
      body.push(['S.no','Survey ID',
      'Surveyer Name'],);
       //body
       for (let i = 0; i < Data.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data[i].SURVEY_ID,
        Data[i].NAME];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','',Data.length],
      );
      }
     


      const Data1:any= this.generalinfoReportdata;
      const checkdata1 = Math.max(Data1.length);
      if(checkdata1>0){
      
      //var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','General Information','','','','',''],
      );
      body.push(dataRow);
      body.push(['S.No',
'Survey ID',
'Do you consent for Aadhaar-based authentication',
'Aadhaar no',
'Name',
'Surname',
"Father's name",
'Gender',
'Date of birth',
'Age',
'Marital status',
'Caste',
'Mobile no',
'Alternate mobile no',
'Survey location name'],);
      //body
      for (let i = 0; i < Data1.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data1[i].SURVEY_ID,
        Data1[i].UID_ANY,
        Data1[i].MASKED_UID_NO,
        Data1[i].NAME,
        Data1[i].SURNAME,
        Data1[i].FATHER_NAME,
        Data1[i].GENDER,
        Data1[i].DATE_OF_BIRTH,
        Data1[i].AGE,
        Data1[i].MARITAL_STATUS,
        Data1[i].CASTE,
        Data1[i].MOBILE,
        Data1[i].ALTER_MOBILE,
        Data1[i].SURVEY_LOCATION_NAME];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','','','','','','','','','','','',Data1.length],
      );
    }

    const Data2:any= this.familyinfodata;
    const checkdata2 = Math.max(Data2.length);
    if(checkdata2>0){
     // var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','','','','','Family Details','','','','','','','',''],
      );
      body.push(dataRow);
      body.push(['S.No',
      'Survey ID',
      'Have family',
      'Family members count',
      'Family member names',
      'Relation',
      'Date of birth',
      'Age',
      'Gender',
      'Marital status',
      'Vulnerable category ',
      'Educational qualification',
      'Address',
      'Are you at residence during survey',
      "Previous generation's of family in same occupation",
      "Previous generation's count",
      "Previous generation's relationship with worker",
      "Previous generation's relationship with worker other"],);
      //body
      for (let i = 0; i < Data2.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data2[i].SURVEY_ID,
        Data2[i].HAVE_FAMILY,
        Data2[i].FAMILY_MEMBERS_COUNT,
        Data2[i].FAMILY_MEMBERS_NAME ,
        Data2[i].RELATION,
        Data2[i].DATE_OF_BIRTH,
        Data2[i].AGE,
        Data2[i].GENDER ,
        Data2[i].MARITAL_STATUS,
        Data2[i].CATEGORY_NAME,
        Data2[i].EDUCATIONAL_STATUS_NAME,
        Data2[i].ADDRESS,
        Data2[i].AT_RESIDENCE_ON_SURVEY,
        Data2[i].PREVIOUS_GENERATIONS_INSAME_OCCUPATIONS ,
        Data2[i].MANY_GENERATIONS_COUNT,
        Data2[i].PREVIOUS_GENERATIONS_NAME,
        Data2[i].PREVIOUS_GENERATIONS_OTHER_TEXT];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','','','','','','','','','','','','','','',Data2.length],
      );
    }

    const Data5:any= this.Jobroledata;
    const checkdata5 = Math.max(Data5.length);
    if(checkdata5>0){
      //var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','Job Role Information','','','',''],
      );
      body.push(dataRow);
      body.push([ 'S.No',
      'Survey ID',
      'Are you provided with ID card',
      'ID card image',
      'Are you provided with payslips',
      'Pay slips image',
      'Satisfied with payslips',
      'Do you have ID card in the current location',
      'ID card type',
      'ID Card Text',
      'Have you been allowed into House for Work',
      'Are you being provided with basic needs',
      'Provided Basic Needs Text',
      'Has water been provided to you during work at house',
      'Has water been provided to you in the same tumbler used by the house residents',
      'Have you been treated courteously',
      'How often',
      'Is there any place to sit during work break',
      'Type of employment',
      'Name of the employer',
      'Employer Text',
      'Activities the worker is engaged with',
      'Other Activities Text',
      'Monthly working days',
      'Are you being paid full salary if work is not provided',
      'When are you getting salary paid',
      'Monthly income',
      'Covered under EPF',
      ],);
      //body
      for (let i = 0; i < Data5.length; i++) {
        var dataRow = [];
        let ETN:string ='';
        if(Data5[i].UNAUTHORIZED_TEXT != null || Data5[i].UNAUTHORIZED_TEXT != '' )
        {
               ETN = Data5[i].EMPLOYMENT_TYPE_NAME + '('+Data5[i].UNAUTHORIZED_TEXT +')';
        }
        else{
          ETN = Data5[i].EMPLOYMENT_TYPE_NAME;
        }
        dataRow = [i+1,Data5[i].SURVEY_ID,
        Data5[i].PROVIDED_WITH_IDCARD,
        Data5[i].PROVIDED_IDCARD_IMAGEPATH,
        Data5[i].PROVIDED_WITH_PAYSLIPS,
        Data5[i].PAYSLIPS_IMAGEGPATH,
        Data5[i].SATISFIED_WITH_PAYSLIPS_PROVIDED,
        Data5[i].IDCARD_IN_CURRENT_LOCATION,
        Data5[i].CARD_NAME,
        Data5[i].IDCARD_CURRENT_LOCATION_TEXT,
        Data5[i].HOUSE_FOR_WORK,
        Data5[i].PROVIDED_WITH_BASI_NEEDS,
        Data5[i].BASIC_NEEDS_OTHER_TEXT,
        Data5[i].WATER_PROVIDED_ANY,
        Data5[i].PROVIDED_IN_SEPARATE_TUMBLER_NAME,
        Data5[i].TREATED_COURTEOUSLY_ANY,
        Data5[i].BEING_TREATED,
        Data5[i].SIT_DURING_WORK_BREAK_ANY,
        ETN,
        Data5[i].EMPLOYER_NAME,
        Data5[i].EMPLOYER_TEXT,
        Data5[i].ACTIVITIES_NAME,
        Data5[i].ACTIVITIES_TEXT,
        Data5[i].WORKING_DAYS_IN_MONTH,
        Data5[i].BEING_PAID_FULL_SALARY,
        Data5[i].SALARY_DELAYED,
        Data5[i].MONTHLY_INCOME_AMOUNT,
        Data5[i].UNDER_EPF];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','',Data5.length],
      );
    }

const Data3:any= this.SocioReportdata;
const checkdata3 = Math.max(Data3.length);
if(checkdata3>0){
  //var body = [];
  var dataRow :any []=[];
  //Headers; 
  body.push(['','','','','Economic Information','','','',''],
  );
  body.push(dataRow);
  body.push(['S.No',
  'Survey Id',
  'How did you get into sanitation work',
  'Other sanitation',
  'Are you being paid monthly or on daily basis',
  'How are you paid for the work you do',
  'Bank Name',
  'Account No',
  'IFSC Code',
  'Do you currently live in own or rent house',
  'What type of house do you live in',
  'House location',
  'House Location Other',
  'Have you taken any loans',
  'Loan type',
  'Loan Type Other',
  'Amount of loan you have taken',
  'Rate of interest',
  'Rate of Interest Text',
  'Have you repaid the loan',
  'Repayment of loan other',
  'Repayment of loan other Text',
  'Are you engaged in any alternative livelihoods too',
  'What kind of livelihood are you engaged in',
  'Livelihood others',
  'Have you taken occupational safety training',
  'Safety Training Other',
  'No.of training hours',
  'Training certificate image'],);
 
  //body
  for (let i = 0; i < Data3.length; i++) {
    var dataRow = [];
    dataRow = [i+1,Data3[i].SURVEY_ID,
    Data3[i].SANITATION_WORK_NAME,
    Data3[i].sanitation_work_other,
    Data3[i].MONTHLY_OR_DAILY_BASIS,
    Data3[i].PAID_FOR_THE_WORK,
    Data3[i].BANK_NAME,
    Data3[i].ACCOUNT_NO,
    Data3[i].IFSC_CODE,
    Data3[i].OWN_OR_RENT,
    Data3[i].TYPE_OF_HOUSE_NAME,
    Data3[i].HOUSE_LOCATION,
    Data3[i].HOUSE_LOCATION_OTHERS,
    Data3[i].HAVE_ANY_LOANS,
    Data3[i].LOAN_TYPE,
    Data3[i].LOAN_TYPE_OTHER,
    Data3[i].LOAN_AMOUNT_TAKEN_NAME,
    Data3[i].RATE_OF_INTEREST,
    Data3[i].LOAN_INTREST_RATE_OTHERS,
    Data3[i].RE_PAY_LOAN,
    Data3[i].REASONS_NAME,
    Data3[i].LOAN_PAYMENT_DELAY_OTHER,
    Data3[i].ALTERNATE_LIVELIHOODS,
    Data3[i].LIVELIHOOD_NAME,
    Data3[i].LIVELIHOOD_OTHERS_TEXT,
    Data3[i].SAFETY_TRAINING_NAME,
    Data3[i].CAPACITY_BUILDING_NAME_OTHERS,
    Data3[i].TRAINING_HR_NAME,
    Data3[i].CERTIFICATE_IMAGE];
    body.push(dataRow);
  }
  //footer
  body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','','','','',Data3.length],
  );
}

    const Data4:any= this.Insurencedata;
    const checkdata4 = Math.max(Data4.length);
    if(checkdata4>0){
     // var body = [];
      var dataRow :any []=[];
      //Headers; 
      body.push(['','','','','','Insurance Details','','','',''],
      );
      body.push(dataRow);
      body.push(['S.No',
      'Survey ID',
      'Are you permanently disabled ',
      'Permanently disabled type',
      'Permanently disabled type text',
      'How did you become permanently disabled',
      'How did you become permanently disabled other',
      'Has there been a death during the sanitation work',
      'Where the sanitation death occured ',
      'During which sanitation work the death occured',
      'During which sanitation work the death occured other',
      'Have you previously suffered from any occupational related health complications',
      'Previous health issues',
      'Sub-Previous health issues',
      'Previous health issues Other',
      'Are you currently suffering from any occupational related health complications',
      'Current health issues',
      'Sub-current health issues',
      'Current health issues other',
      'Do you have any insurance ',
      'Insurance type',
      'Insurance type other',
      'Have you consulted doctor for health checkup',
      'Where was the health checkup done ',
      'When was the health checkup done other',
      'When was the health checkup done date'
    ],);
  
  
   
      //body
      for (let i = 0; i < Data4.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data4[i].SURVEY_ID,
        Data4[i].PERMANENT_DISABLED_ANY,
        Data4[i].PERMANENT_DISABLED_NAME,
        Data4[i].PERMANENT_DISABLED_OTHER_TEXT,
        Data4[i].PERMANENT_DISABLED_TEXT,
        Data4[i].PERMANENTLY_DISABLED_OTHER,
        Data4[i].DEATH_SANITATION_WORK_ANY,
        Data4[i].SANITATION_DEATH_NAME,
        Data4[i].DEATH_SANITATION_WORK_NAME,
        Data4[i].SANITATION_WORK_DEATH_OTHER,
        Data4[i].PREVIOUS_HEALTH_ISSUE_ANY,
        Data4[i].PREVIOUS_HEALTH_ISSUE_NAME,
        Data4[i].PREVIOUS_HEALTH_ISSUE_SUB_NAME,
        Data4[i].PREVIOUS_HEALTH_ISSUE_OTHER,
        Data4[i].CURRENT_HEALTH_ISSUE_ANY,
        Data4[i].CURRENT_HEALTH_ISSUE_NAME ,
        Data4[i].CURRENT_HEALTH_ISSUE_SUB_NAME,
        Data4[i].CURRENT_HEALTH_ISSUE_OTHER,
        Data4[i].HAVE_INSURANCE_ANY,
        Data4[i].INSURANCE_NAME,
        Data4[i].INSURANCE_OTHER,
        Data4[i].HEALTH_CHECKUP_ANY,
        Data4[i].HEALTH_CHECKUP_DONE,
        Data4[i].HEALTH_CHECKUP_OTHER_TEXT,
        Data4[i].HEALTH_CHECKUP_OTHER];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','','','','','','','','','','','','','','','','','','','','','','',Data4.length],
      );
    }


    const Data6:any= this.feedbackdata;
    const checkdata6 = Math.max(Data6.length);
    if(checkdata6>0){
    
      var dataRow :any []=[];
     
      body.push(['','','Feedback Details','',''],
      );
      body.push(dataRow);
      body.push(['S.no','Survey ID',
      'Add Any Questions In Survey',
      'Remove Any Questions In survey','Feedback and Suggestions'],);
   
      for (let i = 0; i < Data6.length; i++) {
        var dataRow = [];
        dataRow = [i+1,Data6[i].SURVEY_ID,
        Data6[i].ADD_ANY_QUESTIONS_INSURVEY,Data6[i].REMOVE_ANY_QUESTIONS_INSURVEY,Data6[i].FEEDBACK_AND_SUGGESTIONS];
        body.push(dataRow);
      }
      //footer
      body.push(['No Of Records:','','','',Data6.length],
      );
    }
    this.mid.JSONToCSVConvertor(
      body,
      'Survey Report Data',
      true
     );
    }
  }
  }
  addModaldisplay:any='none';
  modalclose()
  {
    this.addModaldisplay = 'none';
  }

  img:any
  async ViewCatimg(imageurl: string): Promise<void> {
    debugger;
    this.Imagedisplay='block'
   if(this.mid.Deploystage =="UAT")
   {
    this.img = "https://apsanitationworkerssurvey.ap.gov.in//SWUAT//SwachhAndhraSurveyImages/"+imageurl;
   }
   else  if(this.mid.Deploystage =="PROD")
   {
    this.img = "https://apsanitationworkerssurvey.ap.gov.in//SWAPI//SwachhAndhraSurveyImages/"+imageurl;
   }
  
  
  }
  Imagedisplay='none';
  imgmodalclose()
  {
    this.Imagedisplay='none';
  }
}
