import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DynamictableComponent } from './dynamictable/dynamictable.component';

const routes: Routes = [
  {
    path: 'dyntable',
    component: DynamictableComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRoutingModule { }
