import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TestRoutingModule } from './test-routing.module';
import { DynamictableComponent } from './dynamictable/dynamictable.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    DynamictableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    TestRoutingModule
  ]
})
export class TestModule { }
